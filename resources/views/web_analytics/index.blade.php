<div class="col-lg-6">
    <section class="card">
        <header class="card-header">
            Gantt Chart
        </header>
        <div class="card-block">
            <svg width="100%" height="300" id="plottable-gantt-chart"></svg>
        </div>
    </section>
</div>


<div class="col-lg-6">
    <section class="card">
        <header class="card-header">
            Bullet Graph
        </header>
        <div class="card-block">
            <svg width="100%" height="300" id="plottable-bullet-graph"></svg>
        </div>
    </section>
</div>