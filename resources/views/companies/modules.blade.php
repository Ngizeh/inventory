@extends('layouts.app')


@section('content')

    <p>Modules for Company</p>
    (List all the modules being used by a particular company)

    <table class="table">
        <thead>
        <tr>
            <td>
                Modules Name
            </td>
            <td>
                Status (Enabled: Only if the Company has subscribed to that module, Disabled: If the subscription to the module has expired)
            </td>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>
                POS
            </td>
            <td>
                <a>Enabled</a>
            </td>
        </tr>
        <tr>
            <td>
                HR
            </td>
            <td>
               <a>Disabled</a>
            </td>
        </tr>
        </tbody>
    </table>

@endsection