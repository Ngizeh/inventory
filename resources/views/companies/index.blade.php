@extends('layouts.app')


@section('content')

    <p>Registered Companies</p>
    (A listing of all Companies that have accounts with the UzaPoint application)

    <table class="table">
        <thead style="background-color: #00a1f3;color: #ffffff">
        <tr>
            <td>
                Company Name
            </td>
            <td>
                Owner
            </td>
            <td>
                Location
            </td>
            <td>
                Registration Date
            </td>
            <td>
                Type
            </td>
            <td>
                Activated Modules
            </td>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>
                Ujenzi Chemist
            </td>
            <td>
                John Doe
            </td>
            <td>
                South B
            </td>
            <td>
                28/09/2016
            </td>
            <td>
                Retailer
            </td>
            <td>
                <a href="{{ route('companies.modules') }}">Modules</a>
            </td>
        </tr>
        <tr>
            <td>
                Nike Electronics
            </td>
            <td>
                Peter Tosh
            </td>
            <td>
                South C
            </td>
            <td>
                28/09/2016
            </td>
            <td>
                Manufacturer
            </td>
            <td>
                <a href="{{ route('companies.modules') }}">Modules</a>
            </td>
        </tr>
        </tbody>
    </table>





@endsection