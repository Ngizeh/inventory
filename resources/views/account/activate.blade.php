@extends('layouts.app')

@section('content')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box" method="POST" style="opacity: 0.7; background-color:black; border: none; margin-left: 41%;" action="{{ route('account.activate') }}">
                    {{ csrf_field() }}
                    <div>
                        <img src="{{ asset('img/logo/logo.png') }}" alt="" height="60" width="200" style="padding-left: 15%">
                    </div>
                    <header class="sign-title" style="padding-top: 10%; color: #f5f6fa;">Activate Your Account</header>
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" name="email" class="form-control" placeholder="E-Mail" value="{{ old('email') }}" required autofocus/>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                        <input type="text" name="code" class="form-control" placeholder="Enter Your Activation Code" value="{{ old('code') }}" required autofocus/>
                        @if ($errors->has('code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('code') }}</strong>
                            </span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-rounded" style="background-color: #2befbe; border: none;">Activate</button>
                </form>
            </div>
        </div>
    </div><!--.page-center-->
    {{--<div class="container">--}}
@endsection

@section('footer')
    <footer style="position: absolute;bottom: 0; height: 60px;">
        <div class="container">
            <p class="text-muted" style="margin-left: 55%; color: #f5f6fa;">UzaPoint &copy; @include('partials.dates.year') </p>
        </div>
    </footer>
@endsection
