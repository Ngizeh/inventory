@extends('layouts.app')


@section('content')
    <table class="table table-bordered datatable display cell-border order-column stripe " id="inventory-table">
        <thead class="dt-head-center" style="background-color: #00a1f3;color: #ffff">
        <tr>
            <th>Product Name</th>
            <th>Product ID</th>
            <th>Quantity</th>
            <th>Purchase Price</th>
            <th>Selling Price</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
    </table>
@endsection

@push('scripts')
<script>
    $(function() {
        $('#inventory-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('inventory.data') !!}',
            columns: [
                { data: 'product_name', name: 'product_name' },
                { data: 'product_id', name: 'product_id' },
                { data: 'product_quantity', name: 'product_quantity' },
                { data: 'purchase_price', name: 'purchase_price' },
                { data: 'selling_price', name: 'selling_price' },
                { data: 'edit', name: 'edit', orderable: false, searchable: false},
                { data: 'delete', name: 'delete', orderable: false, searchable: false}


            ],

            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });


</script>
@endpush