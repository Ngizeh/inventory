@extends('layouts.app')

@section('content')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box" method="POST" action="{{ route('account.secure', [$email]) }}">
                    {{ csrf_field() }}
                    <div>
                        <img src="{{ asset('img/logo/logo.png') }}" alt="" height="60" width="200" style="padding-left: 15%">
                    </div>
                    <header class="sign-title" style="padding-top: 10%;">Please add a password to secure your account</header>

                    <p>
                        Your Login Email is: {{ $email }}
                    </p>

                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input type="password" id="password_confirm" name="password_confirmation" class="form-control" placeholder="Repeat password"/>

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-rounded" style="background-color: #2befbe; border: none;">Finish</button>
                </form>
            </div>
        </div>
    </div><!--.page-center-->
    {{--<div class="container">--}}
@endsection

@section('footer')
    <footer style="position: absolute;bottom: 0; height: 60px;">
        <div class="container">
            <p class="text-muted text-center">UzaPoint &copy; @include('partials.dates.year') </p>
        </div>
    </footer>
@endsection
