@extends('layouts.app')

@section('content')

    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box" method="POST" style="opacity: 0.7; background-color:black; border: none; margin-left: 41%;" action="{{ url('/register') }}">
                    {{ csrf_field() }}
                    {{--<div class="sign-avatar no-photo">&plus;</div>--}}
                    <div>
                        {{--<img src="{{ asset('img/logo/logo.png') }}" alt="" height="60" width="200" style="padding-left: 15%">--}}
                    </div>
                    <header class="sign-title" style="color: #f5f6fa;">Sign Up</header>
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name"/>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" id="email" name="email" class="form-control" placeholder="E-Mail"/>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                        <input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="Phone Number"/>
                        @if ($errors->has('phone_number'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">
                        <input type="text" id="business_name" name="business_name" class="form-control" placeholder="Business Name"/>
                        @if ($errors->has('business_name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('business_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                        <input type="text" id="location" name="location" class="form-control" placeholder="Business Location"/>
                        @if ($errors->has('location'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                            </span>
                        @endif
                    </div>
                    {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                        {{--<input type="password" id="password" name="password" class="form-control" placeholder="Password"/>--}}
                        {{--@if ($errors->has('password'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('password') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">--}}
                        {{--<input type="password" id="password_confirm" name="password_confirmation" class="form-control" placeholder="Repeat password"/>--}}

                        {{--@if ($errors->has('password_confirmation'))--}}
                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('password_confirmation') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    <button type="submit" class="btn btn-rounded sign-up" style="background-color: #2befbe; border: none;">Sign up</button>
                    <p class="sign-note" style="color: #f5f6fa;">Already have an account? <a href="{{ url('/login') }}">Sign in</a></p>
                    <!--<button type="button" class="close">
                        <span aria-hidden="true">&times;</span>
                    </button>-->
                </form>
            </div>
        </div>
    </div><!--.page-center-->

{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Register</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                            {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>--}}

                                {{--@if ($errors->has('name'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">--}}
                            {{--<label for="phone_number" class="col-md-4 control-label">Phone Number</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" required>--}}

                                {{--@if ($errors->has('phone_number'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('phone_number') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">--}}
                            {{--<label for="business_name" class="col-md-4 control-label">Business Name</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="business_name" type="text" class="form-control" name="business_name" value="{{ old('business_name') }}" required>--}}

                                {{--@if ($errors->has('business_name'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('business_name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">--}}
                            {{--<label for="location" class="col-md-4 control-label">Business Location</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}" required>--}}

                                {{--@if ($errors->has('location'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('location') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}



                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">--}}
                            {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}

                                {{--@if ($errors->has('password_confirmation'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password_confirmation') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Register--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
@endsection

@section('footer')
    <footer style="position: absolute;bottom: 0; height: 60px;">
        <div class="container">
            <p class="text-muted text-center" style="margin-left: 20%; color: #f5f6fa;">Inventory System &copy; @include('partials.dates.year') </p>
        </div>
    </footer>
@endsection
