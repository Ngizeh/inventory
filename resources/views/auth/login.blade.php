@extends('layouts.app')

@section('content')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box" style="opacity: 0.7; background-color:black; border: none; margin-left: 41%;" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <div>
                        {{--<img src="{{ asset('img/logo/logo.png') }}" alt="" height="60" width="200" style="padding-left: 15%">--}}
                    </div>
                    <header class="sign-title" style="padding-top: 10%; color: #f5f6fa;">Sign In</header>
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" name="email" class="form-control" placeholder="E-Mail" value="{{ old('email') }}" required autofocus/>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="checkbox float-left">
                            <input type="checkbox" name="remember" id="signed-in"/>
                            <label for="signed-in" style="color: #f5f6fa;">Keep me signed in</label>
                        </div>
                        <div class="float-right reset">
                            <a href="{{ url('/password/reset') }}">Reset Password</a>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-rounded" style="background-color: #2befbe; border: none;">Sign in</button>
                    <p class="sign-note" style="color: #f5f6fa;">New to Inventory System? <a href="{{ url('/register') }}">Register</a></p>
                </form>
            </div>
        </div>
    </div><!--.page-center-->
{{--<div class="container">--}}
@endsection

@section('footer')
    <footer style="position: absolute;bottom: 0; height: 60px;">
        <div class="container">
            <p class="text-muted" style="margin-left: 55%; color: #f5f6fa;">Inventory System &copy; @include('partials.dates.year') </p>
        </div>
    </footer>
@endsection
