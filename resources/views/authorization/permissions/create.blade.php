@extends('layouts.app')

@section('content')
    <h3>
        @include('authorization.partials.create_permissions_form')
    </h3>
@endsection