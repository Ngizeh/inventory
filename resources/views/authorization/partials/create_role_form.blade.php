<section class="box-typical box-panel">
    <header class="box-typical-header">
        <div class="tbl-row">
            <div class="tbl-cell tbl-cell-title">
                <h3>Add an Application Role</h3>
                (Add a global Role defined for the UzaPoint Application Users e.g. Retailer, distributor)
            </div>
        </div>
    </header>
    <div class="box-typical-body">
        <form id="module-form" method="post" action="{{ route('roles.store')}}"  class="form-wizard">

            {{ csrf_field()  }}
            <div>
                <h3>New Role</h3>
                <section>
                    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
                        <label for="name">Name*</label>
                        <input type="text" class="form-control" id="name" name="name"  placeholder="Enter a role name">
                        <small  class="form-text text-muted"></small>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group" {{ $errors->has('display_name') ? ' has-error' : '' }}>
                        <label for="description">Display Name*</label>
                        <input type="text" class="form-control" id="display_name" name="display_name" placeholder="Enter a name to be displayed for this role">
                        @if ($errors->has('display_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('display_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group" {{ $errors->has('description') ? ' has-error' : '' }}>
                        <label for="description">Description*</label>
                        <input type="text" class="form-control" id="description" name="description" placeholder="Enter a description for this role">
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-rounded btn-inline btn-info-outline">Save</button>
                </section>
            </div>
        </form>
    </div><!--.box-typical-body-->
</section>