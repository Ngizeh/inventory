<p>Hey {{ $name }}</p>

Thank you for activating your UzaPoint account

<h3>Kindly click this link to complete your registration</h3>

<a href="{{ route('account.complete', [$code, $email]) }}">Complete Your Registration</a>
