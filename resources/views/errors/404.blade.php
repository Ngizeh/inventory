@extends('layouts.app')

@section('content')

    <h1>Sorry Invalid Resource,, it does not exist in our application</h1>

@endsection

@section('footer')
    <footer style="position: absolute;bottom: 0; height: 60px;">
        <div class="container">
            <p class="text-muted text-center">UzaPoint &copy; @include('partials.dates.year') </p>
        </div>
    </footer>
@endsection
