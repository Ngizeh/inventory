@extends('layouts.app')

@section('content')

    <div class="caption" style="margin-top: 20%; margin-left: 18%;">

        <p style="color: navajowhite; font-size: 30px;">
            Better Stock Management
        </p>
        <p style="color:navajowhite; font-size: 30px;">
            Brighter Business
        </p>

        <p style="color:navajowhite; font-size: 18px">
            Allowing small inventory and larger inventory <br>
            to use a system for managing their stock pipeline
        </p>
    </div>
@endsection