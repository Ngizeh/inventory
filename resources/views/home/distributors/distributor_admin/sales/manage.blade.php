@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{--@if($agents->count())--}}
                <p>Sales</p>

                <table class="table">
                    <thead style="background-color: #00a1f3;color: #ffffff">
                    <tr>
                        <td>
                            <strong>Item Name</strong>
                        </td>
                        <td>
                            <strong>Selling Price</strong>
                        </td>
                        <td>
                            <strong>Quantity</strong>
                        </td>
                        <td>
                            <strong>Total selling</strong>
                        </td>

                    </tr>
                    </thead>

                    <tbody>
                    {{--@foreach($agents as $agent)--}}
                    <tr>
                        <td>
                            Item Name
                        </td>
                        <td>
                            Selling Price
                        </td>
                        <td>
                            Quantity
                        </td>
                        <td>
                            Total selling
                        </td>
                    </tr>
                    {{--@endforeach--}}
                    </tbody>
                </table>

                {{--@else--}}

                <h2>No sales Found</h2>

                {{--@endif--}}

            </div>
        </div>
    </div>
@endsection