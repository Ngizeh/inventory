@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{--@if($agents->count())--}}
                <p>Products</p>

                <table class="table">
                    <thead style="background-color: #00a1f3;color: #ffffff">
                    <tr>
                        <td>
                            <strong>Product Name</strong>
                        </td>
                        <td>
                            <strong>Product Number</strong>
                        </td>
                        <td>
                            <strong>Quantity</strong>
                        </td>
                        <td>
                            <strong>Description</strong>
                        </td>
                        <td>
                            <strong>Buying Price</strong>
                        </td>
                        <td>
                            <strong>Selling Price</strong>
                        </td>
                        <td>
                            <strong>Avatar</strong>
                        </td>
                    </tr>
                    </thead>

                    <tbody>
                    {{--@foreach($agents as $agent)--}}
                    <tr>
                        <td>
                            product name
                        </td>
                        <td>
                            product number
                        </td>
                        <td>
                            quantity
                        </td>
                        <td>
                            description
                        </td>
                        <td>
                           buying price
                        </td>
                        <td>
                            selling price
                        </td>
                        <td>
                            <strong>Avatar</strong>
                        </td>
                    </tr>
                    {{--@endforeach--}}
                    </tbody>
                </table>

                {{--@else--}}

                <h2>No products Found</h2>

                {{--@endif--}}

            </div>
        </div>
    </div>
@endsection