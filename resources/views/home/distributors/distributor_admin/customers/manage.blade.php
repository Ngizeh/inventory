@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{--@if($agents->count())--}}
                    <p>Customers</p>

                    <table class="table">
                        <thead style="background-color: #00a1f3;color: #ffffff">
                        <tr>
                            <td>
                                <strong>Customer Name</strong>
                            </td>
                            <td>
                                <strong>ID Number</strong>
                            </td>
                            <td>
                                <strong>Postal Address</strong>
                            </td>
                            <td>
                                <strong>Phone Number</strong>

                            </td>
                            <td>
                                <strong>Location</strong>
                            </td>
                            <td>
                                <strong>Email</strong>
                            </td>
                        </tr>
                        </thead>

                        <tbody>
                        {{--@foreach($agents as $agent)--}}
                            <tr>
                                <td>
                                   customer name
                                </td>
                                <td>
                                    id number
                                </td>
                                <td>
                                    postal_address
                                </td>
                                <td>
                                    phone_number
                                </td>
                                <td>
                                    location
                                </td>
                                <td>
                                    email@email.com
                                </td>
                            </tr>
                        {{--@endforeach--}}
                        </tbody>
                    </table>

                {{--@else--}}

                    <h2>No customers Found</h2>

                {{--@endif--}}

            </div>
        </div>
    </div>
@endsection