@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{--@if($agents->count())--}}
                <p>Invoicing</p>

                <table class="table">
                    <thead style="background-color: #00a1f3;color: #ffffff">
                    <tr>
                        <td>
                            <strong>Name</strong>
                        </td>
                        <td>
                            <strong>Quantity</strong>
                        </td>
                        <td>
                            <strong>Price</strong>
                        </td>
                    </tr>
                    </thead>

                    <tbody>
                    {{--@foreach($agents as $agent)--}}
                    <tr>
                        <td>
                            Name
                        </td>
                        <td>
                            Quantity
                        </td>
                        <td>
                            Price
                        </td>
                    </tr>
                    {{--@endforeach--}}
                    </tbody>
                </table>

                {{--@else--}}

                <h2>No Invoices Found</h2>

                {{--@endif--}}

            </div>
        </div>
    </div>
@endsection