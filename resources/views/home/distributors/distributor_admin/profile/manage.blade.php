@extends('layouts.app')

@section('content')
    <div class="profile-header-photo gradient" style="background-image: url(img/profile-bg.jpg)">
        <div class="profile-header-photo-in">
            <div class="tbl-cell">
                <div class="info-block">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-9 col-xl-offset-3 col-lg-8 col-lg-offset-4 col-md-offset-0">
                                <div class="tbl info-tbl">
                                    <div class="tbl-row">
                                        <div class="tbl-cell">
                                            <p class="title">Brach title</p>
                                            <p>We give you the best shave in town</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" class="change-cover">
            <i class="font-icon font-icon-picture-double"></i>
            Change cover
            <input type="file"/>
        </button>
    </div><!--.profile-header-photo-->

    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <aside class="profile-side">
                    <section class="box-typical profile-side-user">
                        <button type="button" class="avatar-preview avatar-preview-128">
                            <img src="img/avatar-1-256.png" alt=""/>
                            <span class="update">
									<i class="font-icon font-icon-picture-double"></i>
									Update photo
								</span>
                            <input type="file"/>
                        </button>
                        <button type="button" class="btn btn-rounded">Send a Message</button>
                        <div class="btn-group">
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-home"></span>Quant and Verbal</a>
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-cart"></span>Real Gmat Test</a>
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-speed"></span>Prep Official App</a>
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-users"></span>CATprer Test</a>
                                <a class="dropdown-item" href="#"><span class="font-icon font-icon-comments"></span>Third Party Test</a>
                            </div>
                        </div>
                    </section>


                    <section class="box-typical">
                        <header class="box-typical-header-sm bordered">Info</header>
                        <div class="box-typical-inner">
                            <p class="line-with-icon">
                                <i class="font-icon font-icon-pin-2"></i>
                                South B
                            </p>
                            <p class="line-with-icon">
                                <i class="font-icon font-icon-users-two"></i>
                                Men, Women, Kids
                            </p>
                            <p class="line-with-icon">
                                <i class="font-icon font-icon-case-3"></i>
                                Hair, Cutz
                            </p>
                            <p class="line-with-icon">
                                <i class="font-icon font-icon-learn"></i>
                                Dye, Mohock
                            </p>
                            <p class="line-with-icon">
                                <i class="font-icon font-icon-github"></i>
                                <a href="#">
                                    Monday to Friday

                                    8.00 am to 5.00 pm
                                </a>
                            </p>
                            <p class="line-with-icon">
                                <i class="font-icon font-icon-earth"></i>
                                <a href="#">www.valkinyozi.com</a>
                            </p>
                        </div>
                    </section>
                </aside><!--.profile-side-->
            </div>

            <div class="col-xl-9 col-lg-8">
                <section class="tabs-section">
                    <div class="tabs-section-nav tabs-section-nav-left">
                        <ul class="nav" role="tablist">

                            <li class="nav-item">
                                <a class="nav-link active" href="#tabs-2-tab-2" role="tab" data-toggle="tab">
                                    <span class="nav-link-in">Services</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tabs-2-tab-4" role="tab" data-toggle="tab">
                                    <span class="nav-link-in">Manage Services (Owner)</span>
                                </a>
                            </li>
                        </ul>
                    </div><!--.tabs-section-nav-->

                    <div class="tab-content no-styled profile-tabs">
                        <div role="tabpanel" class="tab-pane active" id="tabs-2-tab-2">
                            <section class="box-typical box-typical-padding">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>
                                            Activity Name
                                        </td>
                                        <td>
                                            Price (Kshs)
                                        </td>
                                        <td>
                                            <a href="">Edit (Owner)</a>
                                        </td>
                                        <td>
                                            <a href="">Delete (Owner)</a>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            Head (Adults)
                                        </td>
                                        <td>
                                            100
                                        </td>
                                        <td>
                                            <a href="">Edit (Owner)</a>
                                        </td>
                                        <td>
                                            <a href="">Delete (Owner)</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Head (Children)
                                        </td>
                                        <td>
                                            50
                                        </td>
                                        <td>
                                            <a href="">Edit (Owner)</a>
                                        </td>
                                        <td>
                                            <a href="">Delete (Owner)</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Beared
                                        </td>
                                        <td>
                                            30
                                        </td>
                                        <td>
                                            <a href="">Edit (Owner)</a>
                                        </td>
                                        <td>
                                            <a href="">Delete (Owner)</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </section>
                        </div><!--.tab-pane-->
                        <div role="tabpanel" class="tab-pane" id="tabs-2-tab-4">
                            <section class="box-typical profile-settings">
                                <section class="box-typical-section">
                                    <header class="box-typical-header-sm">Info</header>
                                    <div class="form-group row">
                                        <div class="col-xl-2">
                                            <label class="form-label">
                                                <i class="font-icon font-icon-pin-2"></i>
                                                City
                                            </label>
                                        </div>
                                        <div class="col-xl-4">
                                            <input class="form-control" type="text" value="New York"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xl-2">
                                            <label class="form-label">
                                                <i class="font-icon font-icon-users-two"></i>
                                                Group
                                            </label>
                                        </div>
                                        <div class="col-xl-4">
                                            <input class="form-control" type="text" value="Group1, Group2"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xl-2">
                                            <label class="form-label">
                                                <i class="font-icon font-icon-case-3"></i>
                                                Code
                                            </label>
                                        </div>
                                        <div class="col-xl-6">
                                            <input class="form-control" type="text" value="Symfony, PHP, JavaScript, Java, Android, SQL, OOP, OOD"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xl-2">
                                            <label class="form-label">
                                                <i class="font-icon font-icon-learn"></i>
                                                Edication
                                            </label>
                                        </div>
                                        <div class="col-xl-6">
                                            <input class="form-control" type="text" value="VSU, Compiter Science, Master"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xl-2">
                                            <label class="form-label">
                                                <i class="font-icon font-icon-github"></i>
                                                Operation Hours
                                            </label>
                                        </div>
                                        <div class="col-xl-4">
                                            <input class="form-control" type="text" value="Nickname"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xl-2">
                                            <label class="form-label">
                                                <i class="font-icon font-icon-earth"></i>
                                                Web
                                            </label>
                                        </div>
                                        <div class="col-xl-4">
                                            <input class="form-control" type="text" value="example.com"/>
                                        </div>
                                    </div>
                                </section>
                                <section class="box-typical-section profile-settings-btns">
                                    <button type="submit" class="btn btn-rounded">Save Changes</button>
                                    <button type="button" class="btn btn-rounded btn-grey">Cancel</button>
                                </section>
                            </section>
                        </div><!--.tab-pane-->
                    </div><!--.tab-content-->
                </section><!--.tabs-section-->
            </div>
        </div><!--.row-->
    </div><!--.container-fluid-->

@endsection