@extends('layouts.app')


@section('content')

    <section class="box-typical box-panel">
        <header class="box-typical-header">
            <div class="tbl-row">
                <div class="tbl-cell tbl-cell-title">
                    <h3>Add Customer More Details</h3>
                </div>
            </div>
        </header>
        <div class="box-typical-body">
            <form id="module-form" method="post" action="{{ route('store.customer_details', [$user_id])}}"  class="form-wizard" enctype="multipart/form-data">

                {{ csrf_field()  }}

                <button type="submit" class="btn btn-rounded btn-inline btn-info-outline">Save Customer More Details</button>
            </form>
        </div><!--.box-typical-body-->
    </section>

@endsection