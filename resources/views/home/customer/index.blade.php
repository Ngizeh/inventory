@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{--@if($agents->count())--}}
                <p>All customers</p>


                <table class="table">
                    <thead style="background-color: #00a1f3;color: #ffffff;">
                    <tr>
                        <td>
                            <strong>Distributor Name</strong>
                        </td>
                        <td>
                            <strong>ID Number</strong>
                        </td>
                        <td>
                            <strong>Postal Address</strong>
                        </td>
                        <td>
                            <strong>Phone Number</strong>
                        </td>
                        <td>
                            <strong>Location</strong>
                        </td>
                        <td>
                            <strong>Email</strong>
                        </td>
                        <td>
                            <strong>Edit</strong>
                        </td>
                    </tr>
                    </thead>

                    <tbody>
                    {{--@foreach($agents as $agent)--}}
                    <tr>
                        <td>
                            distributor name{{--{{ $agent->name }}--}}
                        </td>
                        <td>
                            id number{{--{{ $agent->id_number }}--}}
                        </td>
                        <td>
                            postal address{{--{{ $agent->postal_address }}--}}
                        </td>
                        <td>
                            phone number {{--{{ $agent->phone_number }}--}}
                        </td>
                        <td>
                            location {{--{{ $agent->location }}--}}
                        </td>
                        <td>
                            email {{--{{ $agent->email }}--}}
                        </td>
                        <td>
                            <a href="#">Edit</a>
                        </td>
                    </tr>
                    {{--@endforeach--}}
                    </tbody>
                </table>

                {{--@else--}}

                <h3>No Customers Found</h3>

                {{--@endif--}}

            </div>
        </div>
    </div>
@endsection