@extends('layouts.app')


@section('content')

    <section class="box-typical box-panel">
        <header class="box-typical-header">
            <div class="tbl-row">
                <div class="tbl-cell tbl-cell-title">
                    <h3>Add a Retailer to the Application</h3>
                </div>
            </div>
        </header>
        <div class="box-typical-body">
            <form id="module-form" method="post" action="{{ route('agents.store')}}"  class="form-wizard">

                {{ csrf_field()  }}
                <div>
                    <h3>New Retailer</h3>
                    <section>
                        <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name"  placeholder="Enter name">
                            <small  class="form-text text-muted"></small>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group" {{ $errors->has('id_number') ? ' has-error' : '' }}>
                            <label for="description">ID Number</label>
                            <input type="text" class="form-control" id="id_number" name="id_number" placeholder="Enter agents ID Number">
                            @if ($errors->has('id_number'))
                                <span class="help-block">
                                <strong>{{ $errors->first('id_number') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group" {{ $errors->has('phone_number') ? ' has-error' : '' }}>
                            <label for="phone_number">Phone Number</label>
                            <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Enter phone number">
                            @if ($errors->has('phone_number'))
                                <span class="help-block">
                                <strong>{{ $errors->first('phone_number') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group" {{ $errors->has('postal_address') ? ' has-error' : '' }}>
                            <label for="postal_address">Postal Address</label>
                            <input type="text" class="form-control" id="postal_address" name="postal_address" placeholder="Enter postal address">
                            @if ($errors->has('postal_address'))
                                <span class="help-block">
                                <strong>{{ $errors->first('postal_address') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group" {{ $errors->has('location') ? ' has-error' : '' }}>
                            <label for="location">Location</label>
                            <input type="text" class="form-control" id="location" name="location" placeholder="Enter Location">
                            @if ($errors->has('location'))
                                <span class="help-block">
                                <strong>{{ $errors->first('location') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group" {{ $errors->has('email') ? ' has-error' : '' }}>
                            <label for="location">Email</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group" {{ $errors->has('password') ? ' has-error' : '' }}>
                            <label for="location">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <input type="password" class="form-control" name="password_confirmation">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-rounded btn-inline btn-info-outline">Save</button>
                    </section>
                </div>
            </form>
        </div><!--.box-typical-body-->
    </section>


@endsection