@extends('layouts.app')


@section('content')


    <ol class="breadcrumb">
        <li><a href="{{ route('agents.create') }}">Create Agent</a></li>
    </ol>

    @if($agents->count())
    <p>Agents</p>
    (A listing of all agents that work for UzaPoint)

    <table class="table">
        <thead style="background-color: #00a1f3;color: #ffffff">
        <tr>
            <td>
                <strong>Agent Name</strong>
            </td>
            <td>
               <strong>ID Number</strong>
            </td>
            <td>
               <strong>Postal Address</strong>
            </td>
            <td>
               <strong>Phone Number</strong>

            </td>
            <td>
                <strong>Location</strong>
            </td>
            <td>
                <strong>Email</strong>
            </td>
        </tr>
        </thead>

        <tbody>
        @foreach($agents as $agent)
        <tr>
            <td>
                {{ $agent->name }}
            </td>
            <td>
                {{ $agent->id_number }}
            </td>
            <td>
                {{ $agent->postal_address }}
            </td>
            <td>
                {{ $agent->phone_number }}
            </td>
            <td>
                {{ $agent->location }}
            </td>
            <td>
                {{ $agent->email }}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    @else

        <h2>No Agents Found</h2>

    @endif

@endsection