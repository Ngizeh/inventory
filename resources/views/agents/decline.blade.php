@extends('layouts.app')


@section('content')

    <section class="box-typical box-panel">
        <header class="box-typical-header">
            <div class="tbl-row">
                <div class="tbl-cell tbl-cell-title">
                    <h3>Add a brief reason why the request was declined </h3>
                </div>
            </div>
        </header>
        <div class="box-typical-body">

            <form action="{{ route('decline.user', [$user_id, $agent_id]) }}" method="post">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('information') ? ' has-error' : '' }}">
                    <label for="information" class="control-label">Decline Reason</label>

                    <div>
                        <textarea id="information" class="form-control" name="information" rows="6">{{ old('information')}}</textarea>

                        @if ($errors->has('information'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('information') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <button class="btn btn-info" type="submit">Decline Request</button>
            </form>
        </div><!--.box-typical-body-->
    </section>


@endsection