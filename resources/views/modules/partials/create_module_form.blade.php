<section class="box-typical box-panel">
    <header class="box-typical-header">
        <div class="tbl-row">
            <div class="tbl-cell tbl-cell-title">
                <h3>Register a Module</h3>
                (Register a system module for use by the UzaPoint application)
            </div>
        </div>
    </header>
    <div class="box-typical-body">
        <form id="module-form" method="post" action="{{ route('modules.store') }}"  class="form-wizard">

            {{ csrf_field()  }}
            <div>
                <h3>Module</h3>
                <section>
                    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name"  placeholder="Enter a module name">
                        <small  class="form-text text-muted"></small>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group" {{ $errors->has('description') ? ' has-error' : '' }}>
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" name="description" placeholder="Enter a module description">
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group" {{ $errors->has('status') ? ' has-error' : '' }}>
                        <label for="description">Status</label>

                        <input type="radio" name="status" value="1"> Enabled

                        <br>

                        <input type="radio" name="status" value="0"> Disabled

                        @if ($errors->has('status'))
                            <span class="help-block">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif

                    </div>
                    <button type="submit" class="btn btn-rounded btn-inline btn-info-outline">Save</button>
                </section>
            </div>
        </form>
    </div><!--.box-typical-body-->
</section>