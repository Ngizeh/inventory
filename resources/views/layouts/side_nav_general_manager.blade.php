<li class="pink">
    <a href="{{ route('modules.index') }}">
        <i class="glyphicon glyphicon-th-list"></i>
        <span class="lbl">View Modules</span>
    </a>
</li>

<li class="brown">
    <a href="{{ route('roles.create')  }}">
        <i class="glyphicon glyphicon-registration-mark"></i>
        <span class="lbl">Roles</span>
    </a>
</li>
<li class="green">
    <a href="{{ route('permissions.create') }}">
        <i class="glyphicon glyphicon-eye-close"></i>
        <span class="lbl">Permissions</span>
    </a>
</li>

<li class="gold">
    <a href="{{ route('modules.create') }}">
        <i class="font-icon font-icon-speed"></i>
        <span class="lbl">Register Module</span>
    </a>
</li>

<li class="gold">
    <a href="{{ route('companies.index') }}">
        <i class="glyphicon glyphicon-tasks"></i>
        <span class="lbl">View Companies</span>
    </a>
</li>

<li class="gold">
    <a href="{{ route('agents.index') }}">
        <i class="glyphicon glyphicon-tasks"></i>
        <span class="lbl">View Agents</span>
    </a>
</li>
<li class="gold">
    <a href="#">
        <i class="glyphicon glyphicon-tasks"></i>
        <span class="lbl">Distributor Manager</span>
    </a>
</li>
<li class="green">
    <a href="#">
        <i class="glyphicon glyphicon-eye-close"></i>
        <span class="lbl">Retailer Management</span>
    </a>
</li>
<li class="green">
    <a href="#">
        <i class="glyphicon glyphicon-eye-close"></i>
        <span class="lbl">Payment Management</span>
    </a>
</li>
<li class="green">
    <a href="#">
        <i class="glyphicon glyphicon-eye-close"></i>
        <span class="lbl">Aggregators Management</span>
    </a>
</li>
<li class="green">
    <a href="#">
        <i class="glyphicon glyphicon-eye-close"></i>
        <span class="lbl">CRM</span>
    </a>
</li>
<li class="green">
    <a href="#">
        <i class="glyphicon glyphicon-eye-close"></i>
        <span class="lbl">Knowledge</span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('oauth.index') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">0auth Tokens and Clients</span>
    </a>
</li>