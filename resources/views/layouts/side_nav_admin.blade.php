<li class="pink">
    <a href="{{ route('modules.index') }}">
        <i class="glyphicon glyphicon-th-list"></i>
        <span class="lbl">View Modules</span>
    </a>
</li>

<li class="brown">
    <a href="{{ route('roles.create')  }}">
        <i class="glyphicon glyphicon-registration-mark"></i>
        <span class="lbl">Roles</span>
    </a>
</li>
<li class="green">
    <a href="{{ route('permissions.create') }}">
        <i class="glyphicon glyphicon-eye-close"></i>
        <span class="lbl">Permissions</span>
    </a>
</li>

<li class="brown">
    <a href="{{ route('modules.create') }}">
        <i class="font-icon font-icon-speed"></i>
        <span class="lbl">Register Module</span>
    </a>
</li>

<li class="gold">
    <a href="{{ route('companies.index') }}">
        <i class="glyphicon glyphicon-tasks"></i>
        <span class="lbl">View Companies</span>
    </a>
</li>

<li class="blue">
    <a href="{{ route('agents.index') }}">
        <i class="glyphicon glyphicon-th-large"></i>
        <span class="lbl">View Agents</span>
    </a>
</li>
<li class="red">
    <a href="#">
        <i class="glyphicon glyphicon-user"></i>
        <span class="lbl">User Management</span>
    </a>
</li>
<li class="grey">
    <a href="{{ route('distributors.index') }}">
        <i class="glyphicon glyphicon-duplicate"></i>
        <span class="lbl">Distributor Management</span>
    </a>
</li>
<li class="purple">
    <a href="{{ route('retailers.index') }}">
        <i class="glyphicon glyphicon-credit-card"></i>
        <span class="lbl">Retailer Management</span>
    </a>
</li>

<li class="gold">
    <a href="{{ route('resource.index') }}">
        <i class="glyphicon glyphicon-folder-open"></i>
        <span class="lbl">Resource Management</span>
    </a>
</li>
<li class="grey">
    <a href="#">
        <i class="glyphicon glyphicon-equalizer"></i>
        <span class="lbl">Accounting Management</span>
    </a>
</li>
<li class="red">
    <a href="#">
        <i class="glyphicon glyphicon-cog"></i>
        <span class="lbl">Support</span>
    </a>
</li>
<li class="green">
    <a href="{{ route('crm.index') }}">
        <i class="glyphicon glyphicon-list-alt"></i>
        <span class="lbl">C.R.M</span>
    </a>
</li>
<li class="blue">
    <a href="#">
        <i class="glyphicon glyphicon-education"></i>
        <span class="lbl">Knowledge</span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('web_analytics.index') }}">
        <i class="glyphicon glyphicon-globe"></i>
        <span class="lbl">Web Analytics</span>
    </a>
</li>
<li class="gold">
    <a href="#">
        <i class="glyphicon glyphicon-hdd"></i>
        <span class="lbl">System Performance Analytics </span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('oauth.index') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">0auth Tokens and Clients</span>
    </a>
</li>