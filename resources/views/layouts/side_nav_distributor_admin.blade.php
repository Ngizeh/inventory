<li class="gold">
    <a href="{{ route('distributorCustomers') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">Manage Customer</span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('distributorAdminProfile') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">Company Profile</span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('distributorBranchManagement') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">Branch Management</span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('distributorProducts') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">Product</span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('distributorPortal') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">Portal</span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('distributorSales') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">Sales</span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('distributorSku') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">S.K.U</span>
    </a>
</li>
<li class="gold">
    <a href="{{ route('distributorInvoicing') }}">
        <i class="glyphicon glyphicon-exclamation-sign"></i>
        <span class="lbl">Invoicing</span>
    </a>
</li>