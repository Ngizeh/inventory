<li class="pink">
    <a href="#">
        <i class="glyphicon glyphicon-th-list"></i>
        <span class="lbl">System Log</span>
    </a>
</li>

<li class="brown">
    <a href="#">
        <i class="glyphicon glyphicon-registration-mark"></i>
        <span class="lbl">Back Up</span>
    </a>
</li>
<li class="gold">
    <a href="#">
        <i class="glyphicon glyphicon-tasks"></i>
        <span class="lbl">File System</span>
    </a>
</li>
<li class="green">
    <a href="#">
        <i class="glyphicon glyphicon-eye-close"></i>
        <span class="lbl">Settings</span>
    </a>
</li>

<li class="gold">
    <a href="#">
        <i class="font-icon font-icon-speed"></i>
        <span class="lbl">Emergency Alerts</span>
    </a>
</li>


{{--<li class="gold">--}}
    {{--<a href="{{ route('agents.index') }}">--}}
        {{--<i class="glyphicon glyphicon-tasks"></i>--}}
        {{--<span class="lbl">View Agents</span>--}}
    {{--</a>--}}
{{--</li>--}}
{{--<li class="gold">--}}
    {{--<a href="{{ route('oauth.index') }}">--}}
        {{--<i class="glyphicon glyphicon-exclamation-sign"></i>--}}
        {{--<span class="lbl">0auth Tokens and Clients</span>--}}
    {{--</a>--}}
{{--</li>--}}