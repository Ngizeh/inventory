@if(Request::path() != 'pos')

    <div class="mobile-menu-left-overlay"></div>
    <nav class="side-menu ">
        <div class="side-menu-avatar ">
            <div class="avatar-preview avatar-preview-100">
                <img src="{{  asset('img/avatar-1-256.png') }}" alt="">
            </div>
        </div>
        <ul class="side-menu-list dark-theme dark-theme-ultramarine">

                @if(Auth::user()->hasRole('admin'))
                    @include('layouts.side_nav_admin')
                @endif

                    @if(Auth::user()->hasRole('general_manager'))
                        @include('layouts.side_nav_general_manager')
                    @endif

                @if(Auth::user()->hasRole('technical_support'))
                        @include('layouts.side_nav_technical_support')
                    @endif

                    @if(Auth::user()->hasRole('retailer_admin'))
                        @include('layouts.side_nav_retailer_admin')
                        @endif

                @if(Auth::user()->hasRole('retailer') || Auth::user()->hasRole('company'))


                        @include('layouts.side_nav_retailer')

                @endif

                    @if(Auth::user()->hasRole('distributor_admin'))
                        @include('layouts.side_nav_distributor_admin')
                    @endif

                    @if(Auth::user()->hasRole('distributor_manager'))
                        @include('layouts.side_nav_distributor_manager')
                    @endif

                    @if(Auth::user()->hasRole('distributor_sales_agent'))
                        @include('layouts.side_nav_distributor_sales_agent')
                    @endif


                @if(Auth::user()->hasRole('agent'))
                    @include('layouts.side_nav_agent')
                @endif

                    @if(Auth::user()->hasRole('sub_agent'))
                        @include('layouts.side_nav_sub_agent')
                    @endif
        </ul>
    </nav><!--.side-menu-->
@endif