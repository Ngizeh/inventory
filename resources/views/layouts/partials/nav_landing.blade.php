<nav class="navbar navbar-default navbar-static-top" style="background:transparent;background-image:none;border-color:transparent;box-shadow:none;">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            {{--<p class="navbar-text" style="color: #f5f6fa;">FEATURES</p>--}}

            {{--<p class="navbar-text" style="color: #f5f6fa;">CUSTOMERS</p>--}}

            {{--<p class="navbar-text" style="color: #f5f6fa;">PRICING</p>--}}



            {{--<!-- Branding Image -->--}}
            {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
            {{--{{ config('app.name', 'Laravel') }}--}}
            {{--</a>--}}
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">

                <li><a href="{{ url('/login') }}" style="color:black;">Login</a></li>
                <li><a href="{{ url('/register') }}" style="color: black">Register</a></li>
                {{--<li><a class="btn btn-sm" href="{{ url('/register') }}" style="background-color: #2befbe; border: none;">FREE TRIAL</a></li>--}}
            </ul>
        </div>
    </div>
</nav>