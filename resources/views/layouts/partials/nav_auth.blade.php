<nav class="navbar navbar-default navbar-static-top" style="background:transparent;background-image:none;border-color:transparent;box-shadow:none;">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <p class="navbar-text"><a href="{{ url('/') }}" style="color: black;">HOME </a></p>

            {{--<p class="navbar-text" style="color: #434858;">FEATURES</p>--}}

            {{--<p class="navbar-text" style="color: #434858;">CUSTOMERS</p>--}}

            {{--<p class="navbar-text" style="color: #434858;">PRICING</p>--}}


            {{--<!-- Branding Image -->--}}
            {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
            {{--{{ config('app.name', 'Laravel') }}--}}
            {{--</a>--}}
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    @if(Request::path() != 'login')
                        <li><a href="{{ url('/login') }}" style="color:black;">Login</a></li>
                    @endif

                    @if(Request::path() != 'register')
                    {{--<li><a class="btn btn-sm" href="{{ url('/register') }}" style="background-color: #2befbe; border: none;">Register</a></li>--}}
                            <li><a href="{{ url('/register') }}" style="color:black; border: none;">Register</a></li>

                        @endif
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>