<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/tether.min.js') }}"></script>
<script src="{{ asset('js/plugins.js') }}"></script>
<script src="{{ asset('js/app_theme.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap-table/bootstrap-table.js') }}"></script>
<script src="{{ asset('js/bootstrap-table/bootstrap-table-export.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-table/tableExport.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-table/bootstrap-table-init.js')}}"></script>