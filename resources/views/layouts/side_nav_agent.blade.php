<li class="green">
    <a href="{{ route('merchant.index') }}">
        <i class="glyphicon glyphicon-th-list"></i>
        <span class="lbl">Merchant</span>
    </a>
</li>

<li class="brown">
    <a href="{{ route('sub_agents.index') }}">
        <i class="glyphicon glyphicon-barcode"></i>
        <span class="lbl">Register sub-agent</span>
    </a>
</li>

<li class="red">
    <a href="#">
        <i class="glyphicon glyphicon-barcode"></i>
        <span class="lbl">SLA's</span>
    </a>
</li>
{{--<li class="red">--}}
    {{--<a href="{{ route('hr.index') }}">--}}
        {{--<i class="glyphicon glyphicon-user"></i>--}}
        {{--<span class="lbl">Human Resource (HR)</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="yellow">--}}
    {{--<a href="{{ route('accounting.index') }}">--}}
        {{--<i class="font-icon font-icon-home"></i>--}}
        {{--<span class="lbl">Accounting</span>--}}
    {{--</a>--}}
{{--</li>--}}
{{--<li class="red">--}}
    {{--<a href="{{ route('hr.index') }}">--}}
        {{--<i class="glyphicon glyphicon-user"></i>--}}
        {{--<span class="lbl">Human Resource (HR)</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="yellow">--}}
    {{--<a href="{{ route('accounting.index') }}">--}}
        {{--<i class="font-icon font-icon-home"></i>--}}
        {{--<span class="lbl">Accounting</span>--}}
    {{--</a>--}}
{{--</li>--}}

<li class="pink">
    <a href="{{ route('crm.index') }}">
        <i class="glyphicon glyphicon-th"></i>
        <span class="lbl">C.R.M</span>
    </a>
</li>

<li class="brown">
    <a href="#">
        <i class="font-icon font-icon-phone"></i>
        <span class="lbl">Emergency</span>
    </a>
</li>

{{--<li class="gold">--}}
    {{--<a href="{{ route('service.index') }}">--}}
        {{--<i class="glyphicon glyphicon-star"></i>--}}
        {{--<span class="lbl">Service</span>--}}
    {{--</a>--}}
{{--</li>--}}


{{--<li class="gold">--}}
    {{--<a href="{{ route('user-management.index') }}">--}}
        {{--<i class="glyphicon glyphicon-star"></i>--}}
        {{--<span class="lbl">User Management</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="gold">--}}
    {{--<a href="{{ route('members.index') }}">--}}
        {{--<i class="glyphicon glyphicon-star"></i>--}}
        {{--<span class="lbl">Members</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="violet">--}}
    {{--<a href="{{ route('community.index') }}">--}}
        {{--<i class="glyphicon glyphicon-globe"></i>--}}
        {{--<span class="lbl">Community</span>--}}
    {{--</a>--}}
{{--</li>--}}

<li class="black">
    <a href="{{ route('knowledge.index') }}">
        <i class="glyphicon glyphicon-education"></i>
        <span class="lbl">Knowledge</span>
    </a>
</li>

{{--<li class="black">--}}
    {{--<a href="{{ route('knowledge.index') }}">--}}
        {{--<i class="glyphicon glyphicon-education"></i>--}}
        {{--<span class="lbl">Business Intelligence</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="black">--}}
    {{--<a href="{{ route('knowledge.index') }}">--}}
        {{--<i class="glyphicon glyphicon-education"></i>--}}
        {{--<span class="lbl">Payments</span>--}}
    {{--</a>--}}
{{--</li>--}}
