<!DOCTYPE html>
<html lang="en" ng-app="POS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--<title>{{ config('app.name', 'UzaPoint') }}</title>--}}
    <title> Inventory System</title>

    @include('layouts.partials.top_nav_styles')

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    {{--<link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.6/jq-2.2.3/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.css"/>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.0.3/css/dataTables.checkboxes.css" rel="stylesheet" />

{{--<link href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet">--}}

    @stack('css-scripts')

<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>



<body class="{{Request::path() == '/' || Request::path() == 'register' || Request::path() == 'login' || Request::path() == 'pos/business' || Request::is('pos/business/*') || Request::path() == 'pos/sales' || Request::is('pos/sales/*') ? '' : 'with-side-menu'}}"



@if(Request::path() == '/' || Request::path() == 'login' || Request::path() == 'register' || Request::path() == 'accounts/activate')
style="background-image: url({{ asset('img/backgrounds/stock.jpg') }});"
@endif
>

@if(Request::path() == '/')
    @include('layouts.partials.nav_landing')
@endif

@if(Request::path() == 'login' || Request::path() == 'register' || Request::path() == 'accounts/activate' || Request::is('accounts/*'))
    @include('layouts.partials.nav_auth')
@endif


@if(\Auth::check())
    @include('layouts.partials.top_nav')
    @if(!Request::is('pos/*'))
     @include('layouts.partials.side_nav')
    @endif
@endif
@if(Request::path() != 'login' && Request::path() != 'register' && Request::path() != 'accounts/activate' && !Request::is('accounts/*'))
<div class="page-content">
@endif
    <div class="container-fluid">
        @if(Request::path() != '/')
            @include('flash.flash_message')
            @include('flash.flash_message_error')
        @endif
        @yield('content')
        @yield('footer')
    </div><!--.container-fluid-->
@if(Request::path() != 'login' && Request::path() != 'register' && Request::path() != 'accounts/activate' && !Request::is('accounts/*'))
</div><!--.page-content-->
@endif
    @include('layouts.partials.scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.6/jq-2.2.3/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.0.3/js/dataTables.checkboxes.min.js"></script>

    @stack('scripts')
    </body>
</html>
