@if(Auth::user()->hasRole('admin'))


@endif


<li class="green">
    <a href="{{ route('pos.index') }}">
        <i class="glyphicon glyphicon-th-list"></i>
        <span class="lbl">P.O.S</span>
    </a>
</li>

<li class="brown">
    <a href="{{ route('inventory.index') }}">
        <i class="glyphicon glyphicon-barcode"></i>
        <span class="lbl">Inventory</span>
    </a>
</li>

<li class="brown">
    <a href="{{ route('sales.index') }}">
        <i class="glyphicon glyphicon-barcode"></i>
        <span class="lbl">Sales</span>
    </a>
</li>

{{--<li class="red">--}}
    {{--<a href="{{ route('hr.index') }}">--}}
        {{--<i class="glyphicon glyphicon-user"></i>--}}
        {{--<span class="lbl">Human Resource (HR)</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="yellow">--}}
    {{--<a href="{{ route('accounting.index') }}">--}}
        {{--<i class="font-icon font-icon-home"></i>--}}
        {{--<span class="lbl">Accounting</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="pink">--}}
    {{--<a href="{{ route('crm.index') }}">--}}
        {{--<i class="glyphicon glyphicon-th"></i>--}}
        {{--<span class="lbl">C.R.M</span>--}}
    {{--</a>--}}
{{--</li>--}}

<li class="brown">
    <a href="{{ route('procurement.index') }}">
        <i class="glyphicon glyphicon-envelope"></i>
        <span class="lbl">Procurement</span>
    </a>
</li>

{{--<li class="gold">--}}
    {{--<a href="{{ route('service.index') }}">--}}
        {{--<i class="glyphicon glyphicon-star"></i>--}}
        {{--<span class="lbl">Service</span>--}}
    {{--</a>--}}
{{--</li>--}}


<li class="gold">
    <a href="{{ route('user-management.index') }}">
        <i class="glyphicon glyphicon-star"></i>
        <span class="lbl">User Management</span>
    </a>
</li>

{{--<li class="gold">--}}
    {{--<a href="{{ route('members.index') }}">--}}
        {{--<i class="glyphicon glyphicon-star"></i>--}}
        {{--<span class="lbl">Members</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="violet">--}}
    {{--<a href="{{ route('community.index') }}">--}}
        {{--<i class="glyphicon glyphicon-globe"></i>--}}
        {{--<span class="lbl">Community</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="black">--}}
    {{--<a href="{{ route('knowledge.index') }}">--}}
        {{--<i class="glyphicon glyphicon-education"></i>--}}
        {{--<span class="lbl">Knowledge</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="black">--}}
    {{--<a href="{{ route('knowledge.index') }}">--}}
        {{--<i class="glyphicon glyphicon-education"></i>--}}
        {{--<span class="lbl">Business Intelligence</span>--}}
    {{--</a>--}}
{{--</li>--}}

{{--<li class="black">--}}
    {{--<a href="{{ route('knowledge.index') }}">--}}
        {{--<i class="glyphicon glyphicon-education"></i>--}}
        {{--<span class="lbl">Payments</span>--}}
    {{--</a>--}}
{{--</li>--}}