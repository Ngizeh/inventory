(function(){
    var app = angular.module('POS', [ ], function($interpolateProvider){
//-- as the output conflicts with blade lets alter the defaults
        $interpolateProvider.startSymbol('[[').endSymbol(']]');
    });



    app.controller("SearchItemCtrl", [ '$scope', '$http', function($scope, $http) {
        $scope.items = [ ];
        $http.get('/api/item').success(function(data) {
            $scope.items = data;

            console.log($scope.items);
        });
        $scope.saletemp = [ ];
        $scope.newsaletemp = { };
        $http.get('/api/saletemp').success(function(data, status, headers, config) {
            $scope.saletemp = data;
        });
        $scope.addSaleTemp = function(item, newsaletemp) {
            $http.post('/api/saletemp', { item_id: item.id, cost_price: item.purchase_price, selling_price: item.selling_price }).
            success(function(data, status, headers, config) {
                $scope.saletemp.push(data);
                    $http.get('/api/saletemp').success(function(data) {
                    $scope.saletemp = data;
                    });
            });
        }
        $scope.updateSaleTemp = function(newsaletemp) {


            console.log(newsaletemp.quantity);

            $http.post('/api/saletemp/update/' + newsaletemp.id, { quantity: newsaletemp.quantity, total_cost: newsaletemp.inventory.purchase_price * newsaletemp.quantity,
                total_selling: newsaletemp.inventory.selling_price * newsaletemp.quantity }).
            success(function(data, status, headers, config) {
                
                });
        }
        $scope.removeSaleTemp = function(id) {
            $http.get('/api/saletemp/' + id).
            success(function(data, status, headers, config) {
                $http.get('/api/saletemp').success(function(data) {
                        $scope.saletemp = data;
                        });
                });
        }
        $scope.sum = function(list) {
            var total=0;
            angular.forEach(list , function(newsaletemp){
                total+= parseFloat(newsaletemp.inventory.selling_price * newsaletemp.quantity);
            });
            return total;
        }

    }]);
})();