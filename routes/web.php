<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['as' => 'root.index', 'uses' => '\UzaPoint\Modules\Skeleton\Controllers\IndexController@index']);

Auth::routes();

/**
 * The route that loads the Home Page of the Application
 */

Route::get('/home', ['as' => 'app.index', 'uses' => '\UzaPoint\Web\Pages\Controllers\HomeController@index']);

/**
 * Web Routes For Roles
 * ----------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'roles'], function () {

    /****
     * Stores new Role to the database
     ****
     ****/
    Route::post('/', ['as' => 'roles.store', 'uses' => '\UzaPoint\Web\Authorization\Controllers\RolesController@store']);

    /****
     * Get the Template for creating a new Role
     ****
     ****/
    Route::get('/create', ['as' => 'roles.create', 'uses' => '\UzaPoint\Web\Authorization\Controllers\RolesController@create']);

});

/**
 * Web Routes For Permissions
 * ----------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'permissions'], function () {

    /****
     * Stores new Permission to the database
     ****
     ****/
    Route::post('/', ['as' => 'permissions.store', 'uses' => '\UzaPoint\Web\Authorization\Controllers\PermissionsController@store']);

    /****
     * Get the Template for creating a new Permission
     ****
     ****/
    Route::get('/create', ['as' => 'permissions.create', 'uses' => '\UzaPoint\Web\Authorization\Controllers\PermissionsController@create']);


});

/**
 * Web Routes For Permissions
 * ----------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'inventories'], function () {


    /****
     * Get the Template for creating a new Permission
     ****
     ****/
    Route::get('/create', ['as' => 'inventories.create', 'uses' => 'InventoryController@create']);


});

/**
 * Web routes for Modules
 * ----------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'modules'], function () {


    /****
     * Get the Template for creating a new module
     ****
     ****/
    Route::get('/create', ['as' => 'modules.create', 'uses' => 'ModuleController@create']);


    /****
     * Get the Template for creating a new module
     ****
     ****/
    Route::get('/', ['as' => 'modules.index', 'uses' => 'ModuleController@index']);

    Route::get('datatables/', ['as' => 'modules.data', 'uses' => 'ModuleController@modulesData']);


    /**
     * Update a module
     */
    Route::post('/update/{module_id}', ['as' => 'modules.update', 'uses' => 'ModuleController@update']);


    /****
     * Get the Template for editing a new module
     ****
     ****/
    Route::get('/edit/{module_id}', ['as' => 'modules.edit', 'uses' => 'ModuleController@edit']);
    /**
     * Persist a new module to the database
     */
    Route::post('/', ['as' => 'modules.store', 'uses' => 'ModuleController@store']);

});

/**
 * Web routes for companies
 * ----------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'companies'], function () {


    /****
     * Get the Template for creating a new module
     ****
     ****/
    Route::get('/', ['as' => 'companies.index', 'uses' => 'CompanyController@index']);

    Route::get('/modules', ['as' => 'companies.modules', 'uses' => 'CompanyController@modules']);

});

/**
 * Accounts web routes
 * ----------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'guest', 'prefix' => 'accounts'], function () {

    /****
     * Get the Template for creating a new module
     ****
     ****/
    Route::get('/activate', ['as' => 'get.activate', 'uses' => 'AccountController@getActivate']);

    Route::post('/activate', ['as' => 'account.activate', 'uses' => 'AccountController@activate']);

    Route::get('/{code}/{email}', ['as' => 'account.complete', 'uses' => 'AccountController@complete']);

    Route::post('/{email}/secure', ['as' => 'account.secure', 'uses' => 'AccountController@secure']);

});

/**
 * Web routes for agents
 * ----------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'agents'], function () {


    /****
     * Get the Template for displaying agents
     ****
     ****/
    Route::get('/', ['as' => 'agents.index', 'uses' => 'AgentController@index']);


    /**
     * Get the template for adding a new agent
     */
    Route::get('/create', ['as' => 'agents.create', 'uses' => 'AgentController@create']);

     /**
      * Store a new agent to the database
      */
    Route::post('/', ['as' => 'agents.store', 'uses' => 'AgentController@store']);

    Route::get('/{user_id}/details', ['as' => 'agents.details', 'uses' => 'UserController@addMoreDetails']);


});

/**
 * Web routes for sub agents
 * ------------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'sub_agents'], function () {

    /**
     * Get the Template for displaying sub_agent
     */
    Route::get('/', ['as' => 'sub_agents.index', 'uses' => 'SubAgentController@index']);

    /**
     * Get the template for adding a new sub_agent
     */
    Route::get('/create', ['as' => 'sub_agents.create', 'uses' => 'SubAgentController@create']);
});

/**
 * Web routes for customers
 * ------------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'customer'], function () {

    /**
     * Get the Template for displaying customers
     */
    Route::get('/', ['as' => 'customer.index', 'uses' => 'CustomersController@index']);
});

/**
 * Web routes for distributors
 * ------------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'distributors'], function () {

    /**
     * Get the Template for displaying a distributors
     */
    Route::get('/', ['as' => 'distributors.index', 'uses' => 'DistributorController@index']);

    /**
     * Get the template for adding a new distributors
     */
    Route::get('/create', ['as' => 'distributors.create', 'uses' => 'DistributorController@create']);
});

/**
 * Web routes for retailers
 * ------------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'retailers'], function () {

    /**
     * Get the Template for displaying a retailer
     */
    Route::get('/', ['as' => 'retailers.index', 'uses' => 'RetailerController@index']);

    /**
     * Get the template for adding a new retailer
     */
    Route::get('/create', ['as' => 'retailers.create', 'uses' => 'RetailerController@create']);
});

/**
 * Web routes for resource
 * ------------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'resource'], function () {

    /**
     * Get the Template for displaying a resource
     */
    Route::get('/', ['as' => 'resource.index', 'uses' => 'ResourceController@index']);
});

/**
 * Web routes for resource
 * ------------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'web_analytics'], function () {

    /**
     * Get the Template for displaying a resource
     */
    Route::get('/', ['as' => 'web_analytics.index', 'uses' => 'WebAnalyticsController@index']);
});

/**
 * Web routes for users
 * ----------------------------------------------------------------------------------------------------------
 */

Route::group(['middleware' => 'auth', 'prefix' => 'users'], function () {

    /**
     * Get the Template for adding a users more details
     */
    Route::get('/{user_id}/details', ['as' => 'create.customer_details', 'uses' => 'UserController@addMoreDetails']);

    Route::post('/{user_id}/details', ['as' => 'store.customer_details', 'uses' => 'UserController@storeMoreDetails']);

    Route::post('/{user_id}/{agent_id}/activate', ['as' => 'activate.user', 'uses' => 'UserController@activateUser']);

    Route::get('/{user_id}/{agent_id}/decline', ['as' => 'get.decline', 'uses' => 'UserController@getDecline']);

    Route::post('/{user_id}/{agent_id}/decline', ['as' => 'decline.user', 'uses' => 'UserController@declineUserRequest']);

});

Route::group(['middleware' => 'auth', 'prefix' => 'oauth'], function () {


    /****
     * Get the Template for displaying the oauth framework
     ****
     ****/
    Route::get('/', ['as' => 'oauth.index', 'uses' => 'OauthController@index']);

});


Route::group(['middleware' => 'auth', 'prefix' => 'distributor'], function () {


    /****
     * Get the Template for displaying the oauth framework
     ****
     ****/
    Route::get('/customers/manage', ['as' => 'distributorCustomers', 'uses' => 'DistributorController@manageCustomers']);

    Route::get('/profile/manage', ['as' => 'distributorAdminProfile', 'uses' => 'DistributorController@manageProfile']);

    Route::get('/branch/manage', ['as' => 'distributorBranchManagement', 'uses' => 'DistributorController@manageBranch']);

    Route::get('/product/manage', ['as' => 'distributorProducts', 'uses' => 'DistributorController@manageProducts']);

    Route::get('/portal/manage', ['as' => 'distributorPortal', 'uses' => 'DistributorController@managePortal']);

    Route::get('/sales/manage', ['as' => 'distributorSales', 'uses' => 'DistributorController@manageSales']);

    Route::get('/sku/manage', ['as' => 'distributorSku', 'uses' => 'DistributorController@manageSku']);

    Route::get('/invoicing/manage', ['as' => 'distributorInvoicing', 'uses' => 'DistributorController@manageInvoicing']);


});

Route::post('pos/sales/{business_id?}', ['as' => 'pos.sales', 'uses' => 'SalesController@store']);

Route::post('pos/customers/', ['as' => 'pos.customers.store', 'uses' => 'CustomersController@store']);



Route::get('/datatables/get',     ['as'  => 'datatables',  'uses' => '\UzaPoint\Web\DataTables\Controllers\DatatablesController@getIndex']);

Route::get('/datatables',     ['as'  => 'datatables.data',  'uses' => '\UzaPoint\Web\DataTables\Controllers\DatatablesController@anyData']);

Route::post('/categories/{business_id}', ['as' => 'saveCategory', 'uses' => 'CategoryController@store']);
