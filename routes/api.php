<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {


    $api->group(['middleware' => []], function($api){

        $api->post('/auth/register', '\UzaPoint\Api\V1\Auth\RegistrationController@register');
    });

    $api->group(['middleware' => ['auth:api'], 'prefix' => 'users'], function($api){
        $api->post('/details', '\UzaPoint\Api\V1\User\UserController@addMoreDetails');
        $api->post('/activate', '\UzaPoint\Api\V1\User\UserController@activateUser');
        $api->post('/decline', '\UzaPoint\Api\V1\User\UserController@declineUserRequest');
        $api->post('/single', '\UzaPoint\Api\V1\User\UserController@get');
    });

    $api->group(['middleware' => ['auth:api'], 'prefix' => 'agents'], function($api){
        $api->post('/', '\UzaPoint\Api\V1\Agent\AgentController@store');
        $api->get('/{agent_id}/requests','\UzaPoint\Api\V1\Agent\AgentController@getRequests');
    });

    $api->group(['middleware' => [], 'prefix' => 'accounts'], function($api){
        $api->post('/activate', '\UzaPoint\Api\V1\Account\AccountController@activate');
    });

    $api->group(['middleware' => [], 'prefix' => ''], function($api){
        $api->get('/item','\UzaPoint\Api\V1\POS\PosController@index');
    });

    $api->group(['middleware' => [], 'prefix' => ''], function($api){
        $api->post('/saletemp','\UzaPoint\Api\V1\POS\PosController@store');
    });

    $api->group(['middleware' => [], 'prefix' => ''], function($api){
        $api->get('/saletemp','\UzaPoint\Api\V1\POS\PosController@getSaleTemp');
    });

    $api->group(['middleware' => [], 'prefix' => ''], function($api){
        $api->get('/saletemp/{id}','\UzaPoint\Api\V1\POS\PosController@destroy');
    });

    $api->group(['middleware' => [], 'prefix' => ''], function($api){
        $api->post('/saletemp/update/{id}','\UzaPoint\Api\V1\POS\PosController@update');
    });


//    $api->post('/user', function (Request $request) {
//
//        return $request->user()->with('roles')->get();
//
//    })->middleware('auth:api');
});



