<?php

use Illuminate\Database\Seeder;
use UzaPoint\InventoryCategory;
use Faker\Factory as Faker;


class InventoryCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InventoryCategory::truncate();

        $faker = Faker::create();

        foreach (range(1, 50) as $index){

            InventoryCategory::create([

                'category_name' => $faker->word,
                'category_description' => $faker->word,
            ]);
        }
    }
}
