<?php

use Illuminate\Database\Seeder;
use UzaPoint\Agent;
use UzaPoint\Repositories\UserRepository;
use UzaPoint\User;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Agent::truncate();

        Agent::create([

            'name' => 'Default UzaPoint Agent',
            'id_number' => '8975858',
            'postal_address' => 45,
            'phone_number'   => '712675070',
            'location'       => 'default',
            'email'          => 'agent@uza.com'
        ]);

        $userRepository = new UserRepository(new User());

        $userRepository->store('Default UzaPoint Agent', 'agent@uza.com', '123456');
    }
}
