<?php

use Illuminate\Database\Seeder;
use UzaPoint\UserBusiness;
use UzaPoint\Business;
use UzaPoint\User;
use Faker\Factory as Faker;


class UserBusinessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserBusiness::truncate();

        $faker = Faker::create();

        $user = User::findOrFail(2);

        $businessIds = Business::pluck('id')->toArray();

        foreach (range(1, 3) as $index){

            $user->businesses()->create([
                'business_id' => $faker->randomElement($businessIds)
            ]);
        }
    }
}
