<?php

use Illuminate\Database\Seeder;
use UzaPoint\Business;
use Faker\Factory as Faker;


class BusinessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Business::truncate();

        $faker = Faker::create();

        foreach (range(1, 30) as $index) {

            Business::create([
                'business_name' => $faker->word,
                'location' => $faker->word,
            ]);
        }
    }
}
