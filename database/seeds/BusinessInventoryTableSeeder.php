<?php

use Illuminate\Database\Seeder;
use UzaPoint\Business;
use UzaPoint\Inventory;
use UzaPoint\BusinessInventory;
use Faker\Factory as Faker;


class BusinessInventoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BusinessInventory::truncate();

        $faker = Faker::create();

        $businessIds = Business::pluck('id')->toArray();

        $inventoryIds = Inventory::pluck('id')->toArray();

        foreach (range(1, 200) as $index){

            BusinessInventory::create([
                'business_id' => $faker->randomElement($businessIds),
                'inventory_id' => $faker->randomElement($inventoryIds)
            ]);
        }
    }
}
