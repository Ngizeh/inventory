<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //disable foreign key checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(BusinessTableSeeder::class);
        $this->call(UserBusinessesTableSeeder::class);
        $this->call(InventoryCategoryTableSeeder::class);
        $this->call(BusinessInventoryCategoryTableSeeder::class);
        $this->call(InventoriesTableSeeder::class);
        $this->call(BusinessInventoryTableSeeder::class);
        $this->call(AgentsTableSeeder::class);
        $this->call(ModulesTableSeeder::class);


        //Supposed to only apply to a single connection and reset itself
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Model::reguard();
    }
}
