<?php

use Illuminate\Database\Seeder;
use UzaPoint\Inventory;
use UzaPoint\BusinessInventoryCategory;
use UzaPoint\UserBusiness;
use Faker\Factory as Faker;

class InventoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Inventory::truncate();

        $faker = Faker::create();

        $businessIds = UserBusiness::where('user_id', 2)->pluck('business_id')->toArray();

        $businessCategoryIds = BusinessInventoryCategory::whereIn('business_id', $businessIds)
                                ->pluck('inventory_category_id')->toArray();

        foreach (range(1, 200) as $index){

            $inventory = Inventory::create([
                'product_name' => $faker->word,
                'product_id'   => $faker->word,
                'product_quantity' => $faker->randomDigit,
                'purchase_price'   => $faker->randomDigit,
                'selling_price'   => $faker->randomDigit
            ]);

            for($i=0; $i<3; $i++){
                $inventory->category()->create([
                    'inventory_category_id' =>  $faker->randomElement($businessCategoryIds)
                ]);
            }
        }
    }
}
