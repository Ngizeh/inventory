<?php

use Illuminate\Database\Seeder;
use UzaPoint\BusinessInventoryCategory;
use UzaPoint\UserBusiness;
use UzaPoint\InventoryCategory;
use Faker\Factory as Faker;

class BusinessInventoryCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BusinessInventoryCategory::truncate();

        $faker = Faker::create();

        $businessIds = UserBusiness::where('user_id', 2)->pluck('business_id')->toArray();

        $inventoryCategoryIds = InventoryCategory::pluck('id')->toArray();

        foreach (range(1, 200) as $index){

            BusinessInventoryCategory::create([
                'business_id' => $faker->randomElement($businessIds),
                'inventory_category_id' => $faker->randomElement($inventoryCategoryIds)
            ]);
        }
    }
}
