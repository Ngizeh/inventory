<?php

use Illuminate\Database\Seeder;
use UzaPoint\Web\Authorization\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'The super admin of the app termed as Clem'
        ]);

        Role::create([
            'name' => 'retailer',
            'display_name' => 'Retailer',
            'description' => 'The Retailer user of the app termed as Clem'
        ]);

        Role::create([
            'name' => 'company',
            'display_name' => 'Company',
            'description' => 'The Company Role'
        ]);

        Role::create([
            'name' => 'agent',
            'display_name' => 'Agent',
            'description' => 'The Agent Role'
        ]);

        Role::create([
            'name' => 'technical',
            'display_name' => 'Technical Support',
            'description' => 'The Technical Support Role'
        ]);

        Role::create([
            'name' => 'manager',
            'display_name' => 'General Manager',
            'description' => 'The General Manager admin'
        ]);

        Role::create([
            'name' => 'distributor_admin',
            'display_name' => 'Distributor Admin',
            'description' => 'The distributors admin'
        ]);

        Role::create([
            'name' => 'distributor_manager',
            'display_name' => 'Distributor Manager',
            'description' => 'The distributors manager'
        ]);

        Role::create([
            'name' => 'distributor_sales_agent',
            'display_name' => 'Distributor Sales Agent',
            'description' => 'The distributors sales agent'
        ]);

        Role::create([
            'name' => 'sub_agent',
            'display_name' => 'Distributor Sub Agent',
            'description' => 'The distributors sub agent'
        ]);
    }
}
