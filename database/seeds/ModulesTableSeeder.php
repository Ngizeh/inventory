<?php

use Illuminate\Database\Seeder;
use UzaPoint\UzapointModule;

class ModulesTableSeeder extends Seeder
{

    protected $modules = [

        [
            'name' => 'Skeleton',
            'description' => 'The Skeleton Module',
            'status'      => 1,
            'user_id'     => 1
        ],
        [
            'name'        => 'POS',
            'description' => 'The POS module',
            'status'      => 1,
            'user_id'     => 1
        ]

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UzapointModule::truncate();

        foreach ($this->modules as $module){

            UzapointModule::create([

                'name'       => $module['name'],
                'description' => $module['description'],
                'status'      => $module['status'],
                'user_id'     => $module['user_id']
            ]);
        }
    }
}
