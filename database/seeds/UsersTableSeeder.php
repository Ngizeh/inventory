<?php

use Illuminate\Database\Seeder;
use UzaPoint\User;
use UzaPoint\Web\Authorization\Repositories\RolesRepository;
use UzaPoint\Web\Authorization\Models\Role;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        DB::table('role_user')->truncate();

        $user_one = User::create([
            'name' =>  'admin',
            'email' => 'uza@clem.com',
            'password' => bcrypt('123456'),
        ]);

        $this->addRole($user_one, new RolesRepository(new Role()), 'admin');

        $user_two = User::create([
            'name' =>  'retailer',
            'email' => 'uza@retailer.com',
            'password' => bcrypt('123456'),
        ]);

        $user_two->login()->create([
            'status' => 0
        ]);

        $this->addRole($user_two, new RolesRepository(new Role()), 'retailer');

        $user_two = User::create([
            'name' =>  'Technical Support',
            'email' => 'technical@uza.com',
            'password' => bcrypt('123456'),
        ]);


        $this->addRole($user_two, new RolesRepository(new Role()), 'technical');

        $user_three = User::create([
            'name' =>  'General Manager',
            'email' => 'manager@uza.com',
            'password' => bcrypt('123456'),
        ]);

        $this->addRole($user_three, new RolesRepository(new Role()), 'manager');

        $user_four = User::create([
            'name' =>  'Distributor Admin',
            'email' => 'distributor_admin@uza.com',
            'password' => bcrypt('123456'),
        ]);

        $this->addRole($user_four, new RolesRepository(new Role()), 'distributor_admin');

        $user_five = User::create([
            'name' =>  'Distributor Manager',
            'email' => 'distributor_manager@uza.com',
            'password' => bcrypt('123456'),
        ]);

        $this->addRole($user_five, new RolesRepository(new Role()), 'distributor_manager');

        $user_six = User::create([
            'name' =>  'Distributor Sales Agent',
            'email' => 'distributor_sales_agent@uza.com',
            'password' => bcrypt('123456'),
        ]);

        $this->addRole($user_six, new RolesRepository(new Role()), 'distributor_sales_agent');

        $user_seven = User::create([
            'name' =>  'Sub Agent',
            'email' => 'sub_agent@uza.com',
            'password' => bcrypt('123456'),
        ]);

        $this->addRole($user_seven, new RolesRepository(new Role()), 'sub_agent');
    }


    /**
     * Add a role for the user
     * @param $user
     * @param RolesRepository $rolesRepository
     */
    public function addRole($user, RolesRepository $rolesRepository, $name){

        $admin = $rolesRepository->getRoleByName($name);

        $user->attachRole($admin);
    }
}
