<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserModulesTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_modules', function (Blueprint $table) {
            $table->increments('id');

            /**
             * The user id key for the users table
             */
            $table->integer('user_id')->index()->unsigned();

            /**
             * The module id key for the modules table
             */
            $table->integer('module_id')->index()->unsigned();

            /**
             * The status of a modules
             *
             * enabled 1
             * disabled 0
             */
            $table->integer('status')->unsigned();

            /**
             * The relationship with the users table
             */
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            /**
             * The relationship with the modules table
             */
            $table->foreign('module_id')
                ->references('id')
                ->on('uzapoint_modules')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_modules');
    }
}
