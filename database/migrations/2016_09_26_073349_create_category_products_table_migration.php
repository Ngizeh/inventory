<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryProductsTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('inventory_id')->index()->unsigned();

            $table->integer('inventory_category_id')->index()->unsigned();

            $table->foreign('inventory_id')
                  ->references('id')
                  ->on('inventories')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('inventory_category_id')
                  ->references('id')
                  ->on('inventory_categories')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_products');
    }
}
