<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessInventoryCategoriesTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_inventory_categories', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('business_id')->index()->unsigned();

            $table->integer('inventory_category_id')->index()->unsigned();

            $table->foreign('business_id')
                  ->references('id')
                  ->on('businesses')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('inventory_category_id')
                  ->references('id')
                  ->on('inventory_categories')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_inventory_categories');
    }
}
