<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUzapointModulesTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uzapoint_modules', function (Blueprint $table) {
            $table->increments('id');
            /**
             * The name of the module
             */
            $table->string('name', 100);

            /**
             * The Description of the modules
             */
            $table->text('description', 4000);

            /**
             * Either disabled or enabled
             *
             * disabled 0
             * enabled 1
             */
            $table->integer('status')->unsigned();

            /**
             * Index Key and Foreign Key for the Users Table
             */
            $table->integer('user_id')->index()->unsigned();

            /**
             * The relationship with the users table
             */
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uzapoint_modules');
    }
}
