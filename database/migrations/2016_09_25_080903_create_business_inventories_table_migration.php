<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessInventoriesTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_inventories', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('inventory_id')->unsigned()->index();

            $table->integer('business_id')->unsigned()->index();

            $table->foreign('inventory_id')
                  ->references('id')
                  ->on('inventories')
                   ->onDelete('cascade')
                   ->onUpdate('cascade');

            $table->foreign('business_id')
                  ->references('id')
                  ->on('businesses')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_inventories');
    }
}
