<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessSalesTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_sales', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('business_id')->index()->unsigned();

            $table->integer('sale_id')->index()->unsigned();


            $table->foreign('business_id')
                  ->references('id')
                  ->on('businesses')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('sale_id')
                  ->references('id')
                  ->on('sales')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_sales');
    }
}
