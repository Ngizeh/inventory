### UZAPOINT MOBILE API DEFINITION

## User Sign Up Process

## Companies and Businesses

Definition

Register a new user
    
      **HEADER** Content-Type: application/json          

     POST:/api/auth/register HTTP /1.1
     HOST:192.34.62.36
     Content-Type:application/json

            {
                "name":"Manji",
                "email":"manji@biscuits.com",
                "phone_number": "0789852047",
                "business_name":"Manji Biscuits",
                "location" : "High Rise"
            }
    
     RESPONSE 200  (OK)
     
            HTTP /1.1 200 OK
            HOST: 192.34.62.36
            Connection: close   

           {
               "meta": {
                 "status_code": "200",
                 "status": "OK",
                 "message": "Thanks for signing up, UzaPoint Agents will get back to you shortly"
               },
               "user": {
                 "user_id": 24,
                 "name": "Manji",
                 "email": "mekw0@biscuits.com"
               },
               "businesses": [
                 {
                   "business_name": "Manji Biscuits",
                   "location": "High Rise",
                   "user_id": 24
                 }
               ]
           }  
     
     RESPONSE ERROR 422 (Unprocessable Entity)
     
         HTTP /1.1 422 Unprocessable Entity
         HOST: 192.34.62.36
         Connection: close  
            
               {
                  "message": "422 Unprocessable Entity",
                  "errors": [
                    [
                      "The name field is required."
                    ],
                    [
                      "The email has already been taken."
                    ],
                    [
                      "The business name field is required."
                    ]
                  ],
                  "status_code": 422
               }  

User Account Activation Using Activation Code and Email
          
     **HEADER** Content-Type: application/json   
      
    POST:/api/accounts/activate HTTP /1.1
    HOST:192.34.62.36
    Content-Type:application/json 
          
           {
                "email" : "manji@biscuits.com",
                "code":"9WkhN"
           }  
    
    RESPONSE 200  (OK)
         
         HTTP /1.1 200 OK
         HOST: 192.34.62.36
         Connection: close   
    
           {
             "status_code": 200,
             "status": "OK",
             "message": "Account was activated successfully, registration link sent to user sign up email"
           }   
           
    RESPONSE ERROR 422 (Unprocessable Entity)
    
          HTTP /1.1 422 Unprocessable Entity
          HOST: 192.34.62.36
          Connection: close
           
           {
             "message": "422 Unprocessable Entity",
             "errors": [
               [
                 "The email field is required."
               ],
               [
                 "The code field is required."
               ]
             ],
             "status_code": 422
           }
           
    RESPONSE ERROR 404 (Not Found) 
          
          HTTP /1.1 422 Not Found
          HOST: 192.34.62.36
          Connection: close 
         
           {
              "message": "Activation Details Not Found, Try Again",
              "status_code": 404
           }
           
### LOGIN  
   
Definition
  
Login into the application as any user type 
      
     **HEADER** Content-Type: application/json   
      
    POST:/oauth/token HTTP /1.1
    HOST:192.34.62.36
    Content-Type:application/json 
    
        {
        	"grant_type" : "password",
        	"client_id": 1,
        	"client_secret" : "d6dETLqfyPaLZsVEtSYHFsoJHRjjcgnAtJNFsxpt",
        	"username": "agent@uza.com",
        	"password": "123456"
        }
     
    RESPONSE 200  (OK)
        
        HTTP /1.1 200 OK
        HOST: 192.34.62.36
        Connection: close
        
        {
          "token_type": "Bearer",
          "expires_in": 3155673600,
          "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjVmNmUxYTFhMTAyNDgzMDViM2NmMDlmMGZjODk3ZjdmODE0N2MxMjRmMjY2N2NhOTVjNjFiYjBhYTk0ODU2ZDA5NGFkODgxYzg4NmEyODg0In0.eyJhdWQiOiIyIiwianRpIjoiNWY2ZTFhMWExMDI0ODMwNWIzY2YwOWYwZmM4OTdmN2Y4MTQ3YzEyNGYyNjY3Y2E5NWM2MWJiMGFhOTQ4NTZkMDk0YWQ4ODFjODg2YTI4ODQiLCJpYXQiOjE0NzQyMDMwMTIsIm5iZiI6MTQ3NDIwMzAxMiwiZXhwIjo0NjI5ODc2NjEyLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.lyyUGF_1-mG5caI3WMKv6Go6ift2vpUu1jSrkS_kz3kE9e5nO1h4JA8uHjQbzvWLDgFCCTabHHDprzIG0JpTG9OOJq8vkoGuiU4XNtEL8_Y6Y42HD25q2VuQzm8X0gZKA9TbSwiL64k2c3SylR2qM8Gi9BhNtbMb9lFl_9clh2LqwgdaGLs6c2mEAaKunX3BLZH8333GIdG4RB2vUtBEC1yKZoiGKz3j3LZt9mP5me8nnfqvui4gD3g2raUSDu30BRYUtu7b_9QkiuPWNQi4r3w3eDBHM-oc376DP8M7gCis5W4NHv6VojG-HPzElpcimw-nUSvra-4wn2atorG9or2fsgFHCzGge28eD-0nJGGEikFi2Ja1RKDrzm87oVJ35vqIWDJjWpht8avORS-6wj2pqUVQJ62jWCTTyXZ36uJBp0SgkoTK5AaSJpvSb87CfI3S8QvvU2JFWH5zv-CJhnnw3DoBLsD3panr6siOuyXR6dgCog7x0Q8e1a35_N6Pao_VSVlt1iJ81LcD2t3tMGCRQOBxFpd4yFVU--MT8xeXTwKtvus_-Dj_HO82npEaAaGJcJ26W_aCWIKSgz2xsj4OLx0ZeXo9ORXQ1Io7hfUF5fgUm78hXnrk-aKDd1QZ4tz5iycEIqtfNYggfTooB4A5Oeg7SlIJ_46KDFIBEmE",
          "refresh_token": "QiZt+gcaW+fKsPVi1BzmrVha4xh2+NfxXGKQ8BZZuGX09M6E1py9LvPtlMzsGFmobhUhMVukQyoOUMLylRjLk4S8eqvm7ZLd09IyZDEYSzVB9RWoSJpMS7ZdnSn8ycBDkujsy9qj6iIjsfhNzywtWuWboO5wm09S3ebIXZpBD0hvDRSh0sI7/ILFqgyUZONmhxvNBChsVVL8+cuvMCqerEsa73+wKofnz4r/dAFnFre0eN1tfPxBtbisLwyUUF4YxPdvSTx8pFb4k7K1eRMvjb2GUoJEPhkpIu+I14nVr/b5Ix+/JC/hz7oMuErIfFYkPw+7/qLiBx5QMU+60FdSdWUcMemqtO9ajlMsj9i5HEKdSbETAlMurJHAygjnUN9gTwqoeAjTchMMFZeTtYB4YE+Lhjxe4IilAp+eqaXMcyMI2Vlr7/Yp89fj1pE2sXXAg3asMAHKjNXFmLH3pbeDOwIJZseK6BhSF4/rv1IAQYrIyQM5BQXXY4BZjYsc+xIFzDRQ8qdDg+Gh29zWd08CPJhmw2BEYsYVi8KCNUt3w+9cnO169U9IeziQyPxZzfhmPGa0zwLgKx/3K6Kn0Th8ZoBCByoOAyZEmjHWmNrMetf57v/mrgUHvzEs8TvM6IkRGzz/G/516EPdfD06XOlRgY7c/fBHu6EUgFM6cJ9iNDs="
        }  
          
    RESPONSE 401  (Unauthorized)  
        
        HTTP /1.1 401 Unauthorized
        HOST: 192.34.62.36
        Connection: close
        
        {
          "error": "invalid_credentials",
          "message": "The user credentials were incorrect."
        }
        
##  GET AUTHENTICATED USER DETAILS AND THEIR ROLE
      
Get the details of the authenticated user together with their roles
    
    **HEADER** Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjVmNmUxYTFhMTAyNDgzMDViM2NmMDlmMGZjODk3ZjdmODE0N2MxMjRmMjY2N2NhOTVjNjFiYjBhYTk0ODU2ZDA5NGFkODgxYzg4NmEyODg0In0.eyJhdWQiOiIyIiwianRpIjoiNWY2ZTFhMWExMDI0ODMwNWIzY2YwOWYwZmM4OTdmN2Y4MTQ3YzEyNGYyNjY3Y2E5NWM2MWJiMGFhOTQ4NTZkMDk0YWQ4ODFjODg2YTI4ODQiLCJpYXQiOjE0NzQyMDMwMTIsIm5iZiI6MTQ3NDIwMzAxMiwiZXhwIjo0NjI5ODc2NjEyLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.lyyUGF_1-mG5caI3WMKv6Go6ift2vpUu1jSrkS_kz3kE9e5nO1h4JA8uHjQbzvWLDgFCCTabHHDprzIG0JpTG9OOJq8vkoGuiU4XNtEL8_Y6Y42HD25q2VuQzm8X0gZKA9TbSwiL64k2c3SylR2qM8Gi9BhNtbMb9lFl_9clh2LqwgdaGLs6c2mEAaKunX3BLZH8333GIdG4RB2vUtBEC1yKZoiGKz3j3LZt9mP5me8nnfqvui4gD3g2raUSDu30BRYUtu7b_9QkiuPWNQi4r3w3eDBHM-oc376DP8M7gCis5W4NHv6VojG-HPzElpcimw-nUSvra-4wn2atorG9or2fsgFHCzGge28eD-0nJGGEikFi2Ja1RKDrzm87oVJ35vqIWDJjWpht8avORS-6wj2pqUVQJ62jWCTTyXZ36uJBp0SgkoTK5AaSJpvSb87CfI3S8QvvU2JFWH5zv-CJhnnw3DoBLsD3panr6siOuyXR6dgCog7x0Q8e1a35_N6Pao_VSVlt1iJ81LcD2t3tMGCRQOBxFpd4yFVU--MT8xeXTwKtvus_-Dj_HO82npEaAaGJcJ26W_aCWIKSgz2xsj4OLx0ZeXo9ORXQ1Io7hfUF5fgUm78hXnrk-aKDd1QZ4tz5iycEIqtfNYggfTooB4A5Oeg7SlIJ_46KDFIBEmE
    **HEADER** Content-Type: application/json     
      
    POST:/api/users/single HTTP /1.1
    HOST:192.34.62.36
    Content-Type:application/json 
     
    RESPONSE 200  (OK)
            
            HTTP /1.1 200 OK
            HOST: 192.34.62.36
            Connection: close
             
            {
              "meta": {
                "status_code": "200",
                "status": "OK"
              },
              "user": {
                "id": 3,
                "name": "Default UzaPoint Agent",
                "email": "agent@uza.com",
                "roles": [
                  {
                    "id": 4,
                    "name": "agent",
                    "description": "The Agent Role"
                  }
                ]
              }
            } 
     
### ADMIN REGISTER A NEW AGENT TO THE APPLICATION
     
Definition
     
     **HEADER** Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjVmNmUxYTFhMTAyNDgzMDViM2NmMDlmMGZjODk3ZjdmODE0N2MxMjRmMjY2N2NhOTVjNjFiYjBhYTk0ODU2ZDA5NGFkODgxYzg4NmEyODg0In0.eyJhdWQiOiIyIiwianRpIjoiNWY2ZTFhMWExMDI0ODMwNWIzY2YwOWYwZmM4OTdmN2Y4MTQ3YzEyNGYyNjY3Y2E5NWM2MWJiMGFhOTQ4NTZkMDk0YWQ4ODFjODg2YTI4ODQiLCJpYXQiOjE0NzQyMDMwMTIsIm5iZiI6MTQ3NDIwMzAxMiwiZXhwIjo0NjI5ODc2NjEyLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.lyyUGF_1-mG5caI3WMKv6Go6ift2vpUu1jSrkS_kz3kE9e5nO1h4JA8uHjQbzvWLDgFCCTabHHDprzIG0JpTG9OOJq8vkoGuiU4XNtEL8_Y6Y42HD25q2VuQzm8X0gZKA9TbSwiL64k2c3SylR2qM8Gi9BhNtbMb9lFl_9clh2LqwgdaGLs6c2mEAaKunX3BLZH8333GIdG4RB2vUtBEC1yKZoiGKz3j3LZt9mP5me8nnfqvui4gD3g2raUSDu30BRYUtu7b_9QkiuPWNQi4r3w3eDBHM-oc376DP8M7gCis5W4NHv6VojG-HPzElpcimw-nUSvra-4wn2atorG9or2fsgFHCzGge28eD-0nJGGEikFi2Ja1RKDrzm87oVJ35vqIWDJjWpht8avORS-6wj2pqUVQJ62jWCTTyXZ36uJBp0SgkoTK5AaSJpvSb87CfI3S8QvvU2JFWH5zv-CJhnnw3DoBLsD3panr6siOuyXR6dgCog7x0Q8e1a35_N6Pao_VSVlt1iJ81LcD2t3tMGCRQOBxFpd4yFVU--MT8xeXTwKtvus_-Dj_HO82npEaAaGJcJ26W_aCWIKSgz2xsj4OLx0ZeXo9ORXQ1Io7hfUF5fgUm78hXnrk-aKDd1QZ4tz5iycEIqtfNYggfTooB4A5Oeg7SlIJ_46KDFIBEmE
     **HEADER** Content-Type: application/json     
           
     POST:/api/agents HTTP /1.1
     HOST:192.34.62.36
     Content-Type:application/json 
     
        {
        	"name":"Agent One",
        	"id_number":"8523697",
        	"postal_address":"41",
        	"phone_number":"679375412",
        	"location":"South B",
        	"email":"agent34@one.com",
        	"password":"123456",
        	"password_confirmation":"123456"
        }
        
        RESPONSE 201  (Created)
        
        HTTP /1.1 201 Created
        HOST: 192.34.62.36
        Connection: close
        
        RESPONSE ERROR 422 (Unprocessable Entity)
    
        HTTP /1.1 422 Unprocessable Entity
        HOST: 192.34.62.36
        Connection: close
           
          {
            "message": "422 Unprocessable Entity",
            "errors": [
              [
                "The postal address field is required."
              ],
              [
                "The email has already been taken."
              ]
            ],
            "status_code": 422
          } 
              
### GET ALL THE REQUESTS FOR A PARTICULAR AGENT

The status key defines the pint where a request is

    e.g 0 for a request that was just initiated
        1 for a request that has been processed by an agent
        2 an activation code has been sent to the user
        3 for a declined request
               
Definition
      
    **HEADER** Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjVmNmUxYTFhMTAyNDgzMDViM2NmMDlmMGZjODk3ZjdmODE0N2MxMjRmMjY2N2NhOTVjNjFiYjBhYTk0ODU2ZDA5NGFkODgxYzg4NmEyODg0In0.eyJhdWQiOiIyIiwianRpIjoiNWY2ZTFhMWExMDI0ODMwNWIzY2YwOWYwZmM4OTdmN2Y4MTQ3YzEyNGYyNjY3Y2E5NWM2MWJiMGFhOTQ4NTZkMDk0YWQ4ODFjODg2YTI4ODQiLCJpYXQiOjE0NzQyMDMwMTIsIm5iZiI6MTQ3NDIwMzAxMiwiZXhwIjo0NjI5ODc2NjEyLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.lyyUGF_1-mG5caI3WMKv6Go6ift2vpUu1jSrkS_kz3kE9e5nO1h4JA8uHjQbzvWLDgFCCTabHHDprzIG0JpTG9OOJq8vkoGuiU4XNtEL8_Y6Y42HD25q2VuQzm8X0gZKA9TbSwiL64k2c3SylR2qM8Gi9BhNtbMb9lFl_9clh2LqwgdaGLs6c2mEAaKunX3BLZH8333GIdG4RB2vUtBEC1yKZoiGKz3j3LZt9mP5me8nnfqvui4gD3g2raUSDu30BRYUtu7b_9QkiuPWNQi4r3w3eDBHM-oc376DP8M7gCis5W4NHv6VojG-HPzElpcimw-nUSvra-4wn2atorG9or2fsgFHCzGge28eD-0nJGGEikFi2Ja1RKDrzm87oVJ35vqIWDJjWpht8avORS-6wj2pqUVQJ62jWCTTyXZ36uJBp0SgkoTK5AaSJpvSb87CfI3S8QvvU2JFWH5zv-CJhnnw3DoBLsD3panr6siOuyXR6dgCog7x0Q8e1a35_N6Pao_VSVlt1iJ81LcD2t3tMGCRQOBxFpd4yFVU--MT8xeXTwKtvus_-Dj_HO82npEaAaGJcJ26W_aCWIKSgz2xsj4OLx0ZeXo9ORXQ1Io7hfUF5fgUm78hXnrk-aKDd1QZ4tz5iycEIqtfNYggfTooB4A5Oeg7SlIJ_46KDFIBEmE
    **HEADER** Content-Type: application/json     
               
    GET:/api/agents/1/requests HTTP /1.1
    HOST:192.34.62.36
    Content-Type:application/json 
     
    RESPONSE 200  (OK)
                
          HTTP /1.1 200 OK
          HOST: 192.34.62.36
          Connection: close
          
          {
            "data": [
              [
                {
                  "id": 2,
                  "agent_id": 1,
                  "status": 2,
                  "user_id": 6,
                  "businesses": [
                    {
                      "business_name": "Manji Biscuits",
                      "location": "High Rise",
                      "user_id": 6
                    }
                  ]
                },
                {
                  "id": 3,
                  "agent_id": 1,
                  "status": 2,
                  "user_id": 7,
                  "businesses": [
                    {
                      "business_name": "Manji Biscuits",
                      "location": "High Rise",
                      "user_id": 7
                    }
                  ]
                },
                ...
              ]
            ],
            "meta": {
              "status_code": "200",
              "status": "OK"
            }
          }
                 

### AGENT PROCESS THE DOCUMENTS OF A USER WHO SENT A REQUEST
                 
Definition

    **HEADER** Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjVmNmUxYTFhMTAyNDgzMDViM2NmMDlmMGZjODk3ZjdmODE0N2MxMjRmMjY2N2NhOTVjNjFiYjBhYTk0ODU2ZDA5NGFkODgxYzg4NmEyODg0In0.eyJhdWQiOiIyIiwianRpIjoiNWY2ZTFhMWExMDI0ODMwNWIzY2YwOWYwZmM4OTdmN2Y4MTQ3YzEyNGYyNjY3Y2E5NWM2MWJiMGFhOTQ4NTZkMDk0YWQ4ODFjODg2YTI4ODQiLCJpYXQiOjE0NzQyMDMwMTIsIm5iZiI6MTQ3NDIwMzAxMiwiZXhwIjo0NjI5ODc2NjEyLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.lyyUGF_1-mG5caI3WMKv6Go6ift2vpUu1jSrkS_kz3kE9e5nO1h4JA8uHjQbzvWLDgFCCTabHHDprzIG0JpTG9OOJq8vkoGuiU4XNtEL8_Y6Y42HD25q2VuQzm8X0gZKA9TbSwiL64k2c3SylR2qM8Gi9BhNtbMb9lFl_9clh2LqwgdaGLs6c2mEAaKunX3BLZH8333GIdG4RB2vUtBEC1yKZoiGKz3j3LZt9mP5me8nnfqvui4gD3g2raUSDu30BRYUtu7b_9QkiuPWNQi4r3w3eDBHM-oc376DP8M7gCis5W4NHv6VojG-HPzElpcimw-nUSvra-4wn2atorG9or2fsgFHCzGge28eD-0nJGGEikFi2Ja1RKDrzm87oVJ35vqIWDJjWpht8avORS-6wj2pqUVQJ62jWCTTyXZ36uJBp0SgkoTK5AaSJpvSb87CfI3S8QvvU2JFWH5zv-CJhnnw3DoBLsD3panr6siOuyXR6dgCog7x0Q8e1a35_N6Pao_VSVlt1iJ81LcD2t3tMGCRQOBxFpd4yFVU--MT8xeXTwKtvus_-Dj_HO82npEaAaGJcJ26W_aCWIKSgz2xsj4OLx0ZeXo9ORXQ1Io7hfUF5fgUm78hXnrk-aKDd1QZ4tz5iycEIqtfNYggfTooB4A5Oeg7SlIJ_46KDFIBEmE
    **HEADER** Content-Type: application/json     
               
    POST:/api/user/details HTTP /1.1
    HOST:192.34.62.36
    Content-Type:application/json
    
        {
        	"document_one_title":"kra pin",
        	"document_one_name":"kra_pin.jpg",
        	"document_one_description":"simply kra pain",
        	"document_two_title":"company certifiacte",
        	"document_two_name":"certificate.jpg",
        	"document_two_description":"simply certificate"
        }
       
    Response 201 Created 
             
    HTTP /1.1 201 Created
    HOST: 192.34.62.36
    Connection: close
               
    RESPONSE ERROR 422 (Unprocessable Entity)
        
            HTTP /1.1 422 Unprocessable Entity
            HOST: 192.34.62.36
            Connection: close
            
            {
              "message": "422 Unprocessable Entity",
              "errors": [
                [
                  "The document one title field is required."
                ],
                [
                  "The document one description field is required."
                ],
                [
                  "The document two title field is required."
                ]
              ],
              "status_code": 422
            }
               
## AGENT SEND AN ACTIVATION CODE TO A USER WHO SENT A REQUEST                        
                 
    **HEADER** Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjVmNmUxYTFhMTAyNDgzMDViM2NmMDlmMGZjODk3ZjdmODE0N2MxMjRmMjY2N2NhOTVjNjFiYjBhYTk0ODU2ZDA5NGFkODgxYzg4NmEyODg0In0.eyJhdWQiOiIyIiwianRpIjoiNWY2ZTFhMWExMDI0ODMwNWIzY2YwOWYwZmM4OTdmN2Y4MTQ3YzEyNGYyNjY3Y2E5NWM2MWJiMGFhOTQ4NTZkMDk0YWQ4ODFjODg2YTI4ODQiLCJpYXQiOjE0NzQyMDMwMTIsIm5iZiI6MTQ3NDIwMzAxMiwiZXhwIjo0NjI5ODc2NjEyLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.lyyUGF_1-mG5caI3WMKv6Go6ift2vpUu1jSrkS_kz3kE9e5nO1h4JA8uHjQbzvWLDgFCCTabHHDprzIG0JpTG9OOJq8vkoGuiU4XNtEL8_Y6Y42HD25q2VuQzm8X0gZKA9TbSwiL64k2c3SylR2qM8Gi9BhNtbMb9lFl_9clh2LqwgdaGLs6c2mEAaKunX3BLZH8333GIdG4RB2vUtBEC1yKZoiGKz3j3LZt9mP5me8nnfqvui4gD3g2raUSDu30BRYUtu7b_9QkiuPWNQi4r3w3eDBHM-oc376DP8M7gCis5W4NHv6VojG-HPzElpcimw-nUSvra-4wn2atorG9or2fsgFHCzGge28eD-0nJGGEikFi2Ja1RKDrzm87oVJ35vqIWDJjWpht8avORS-6wj2pqUVQJ62jWCTTyXZ36uJBp0SgkoTK5AaSJpvSb87CfI3S8QvvU2JFWH5zv-CJhnnw3DoBLsD3panr6siOuyXR6dgCog7x0Q8e1a35_N6Pao_VSVlt1iJ81LcD2t3tMGCRQOBxFpd4yFVU--MT8xeXTwKtvus_-Dj_HO82npEaAaGJcJ26W_aCWIKSgz2xsj4OLx0ZeXo9ORXQ1Io7hfUF5fgUm78hXnrk-aKDd1QZ4tz5iycEIqtfNYggfTooB4A5Oeg7SlIJ_46KDFIBEmE
    **HEADER** Content-Type: application/json     
                   
    POST:/api/users/activate HTTP /1.1
    HOST:192.34.62.36
    Content-Type:application/json
    
    RESPONSE 200  (OK)
                
        HTTP /1.1 200 OK
        HOST: 192.34.62.36
        Connection: close
        
            {
              "meta": {
                "status_code": "200",
                "status": "Ok",
                "message": "Activation code was sent to the user signup email"
              },
              "activation": {
                "id": 12,
                "agent_id": 1,
                "status": 2,
                "user_id": 17
              }
            }
             
    RESPONSE ERROR 422 (Unprocessable Entity)
            
         HTTP /1.1 422 Unprocessable Entity
         HOST: 192.34.62.36
         Connection: close         
           {
             "message": "422 Unprocessable Entity",
             "errors": [
               [
                 "The user id field is required."
               ],
               [
                 "The agent id field is required."
               ]
             ],
             "status_code": 422
           }

### AGENT SEND A DECLINE MESSAGE TO USER WHO SENT A REQUEST
             
    **HEADER** Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjVmNmUxYTFhMTAyNDgzMDViM2NmMDlmMGZjODk3ZjdmODE0N2MxMjRmMjY2N2NhOTVjNjFiYjBhYTk0ODU2ZDA5NGFkODgxYzg4NmEyODg0In0.eyJhdWQiOiIyIiwianRpIjoiNWY2ZTFhMWExMDI0ODMwNWIzY2YwOWYwZmM4OTdmN2Y4MTQ3YzEyNGYyNjY3Y2E5NWM2MWJiMGFhOTQ4NTZkMDk0YWQ4ODFjODg2YTI4ODQiLCJpYXQiOjE0NzQyMDMwMTIsIm5iZiI6MTQ3NDIwMzAxMiwiZXhwIjo0NjI5ODc2NjEyLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.lyyUGF_1-mG5caI3WMKv6Go6ift2vpUu1jSrkS_kz3kE9e5nO1h4JA8uHjQbzvWLDgFCCTabHHDprzIG0JpTG9OOJq8vkoGuiU4XNtEL8_Y6Y42HD25q2VuQzm8X0gZKA9TbSwiL64k2c3SylR2qM8Gi9BhNtbMb9lFl_9clh2LqwgdaGLs6c2mEAaKunX3BLZH8333GIdG4RB2vUtBEC1yKZoiGKz3j3LZt9mP5me8nnfqvui4gD3g2raUSDu30BRYUtu7b_9QkiuPWNQi4r3w3eDBHM-oc376DP8M7gCis5W4NHv6VojG-HPzElpcimw-nUSvra-4wn2atorG9or2fsgFHCzGge28eD-0nJGGEikFi2Ja1RKDrzm87oVJ35vqIWDJjWpht8avORS-6wj2pqUVQJ62jWCTTyXZ36uJBp0SgkoTK5AaSJpvSb87CfI3S8QvvU2JFWH5zv-CJhnnw3DoBLsD3panr6siOuyXR6dgCog7x0Q8e1a35_N6Pao_VSVlt1iJ81LcD2t3tMGCRQOBxFpd4yFVU--MT8xeXTwKtvus_-Dj_HO82npEaAaGJcJ26W_aCWIKSgz2xsj4OLx0ZeXo9ORXQ1Io7hfUF5fgUm78hXnrk-aKDd1QZ4tz5iycEIqtfNYggfTooB4A5Oeg7SlIJ_46KDFIBEmE
    **HEADER** Content-Type: application/json     
                       
    POST:/api/users/activate HTTP /1.1
    HOST:192.34.62.36
    Content-Type:application/json 
            
    RESPONSE 200  (OK)
                    
        HTTP /1.1 200 OK
        HOST: 192.34.62.36
        Connection: close
            
           {
             "meta": {
               "status_code": "200",
               "status": "OK",
               "message": "Decline message was sent to the user signup email"
             },
             "declination": {
               "id": 12,
               "agent_id": 1,
               "status": 3,
               "information": "Unable to process your KRA details",
               "user_id": 17
             }
           }
   
    RESPONSE ERROR 422 (Unprocessable Entity)
                
    HTTP /1.1 422 Unprocessable Entity
    HOST: 192.34.62.36
    Connection: close
                  
        {
          "message": "422 Unprocessable Entity",
          "errors": [
            [
              "The information field is required."
            ]
          ],
          "status_code": 422
        }              