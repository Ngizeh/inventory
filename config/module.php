<?php


/*
|--------------------------------------------------------------------------
| Modules to Load
|--------------------------------------------------------------------------
|
| List all the modules that you want to load inside the `modules` array.
|
*/
return  [
    'modules' => [
        'Skeleton',
        'POS',
        'Inventory',
        'Knowledge',
        'Service',
        'Community',
        'Procurement',
        'Sales',
        'Hr',
        'Accounting',
        'Crm',
        'UserManagement',
        'Members',
        'Merchant'
    ]
];