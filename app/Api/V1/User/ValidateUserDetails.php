<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 4:53 PM
 */

namespace UzaPoint\Api\V1\User;
use Illuminate\Support\Facades\Validator;
use UzaPoint\Api\V1\Validator\ApiValidator;


trait ValidateUserDetails
{

    public function validateUserDetails($data){

        //Start the validation process
        $validator = Validator::make($data, [

            'document_one_title' => 'required',
            'document_one_name' => 'required',
            'document_one_description' => 'required',

            'document_two_title' => 'required',
            'document_two_name' => 'required',
            'document_two_description' => 'required',

        ]);

        $apiValidator = new ApiValidator;

        $apiValidator->validate($validator);
    }

}