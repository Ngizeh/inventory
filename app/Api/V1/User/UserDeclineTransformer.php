<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 12:37 PM
 */

namespace UzaPoint\Api\V1\User;


use UzaPoint\Api\V1\Transformers\Transformer;
use UzaPoint\Api\V1\Business\BusinessTransformer;
use UzaPoint\Repositories\BusinessRepository;
use UzaPoint\UserBusiness;

class UserDeclineTransformer extends Transformer
{
    public function transform($decline)
    {

        return [
            'meta'=>[
                'status_code' => '200',
                'status'     => 'OK',
                'message' => 'Decline message was sent to the user signup email'
            ],
            'declination' => [
                'id'    => $decline->id,
                'agent_id' => $decline->agent_id,
                'status' => $decline->status,
                'information' => $decline->information,
                'user_id' => $decline->user_id
            ]
        ];
    }
}
