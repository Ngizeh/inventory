<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 12:37 PM
 */

namespace UzaPoint\Api\V1\User;


use UzaPoint\Api\V1\Transformers\Transformer;
use UzaPoint\Api\V1\Business\BusinessTransformer;
use UzaPoint\Repositories\BusinessRepository;
use UzaPoint\UserBusiness;

class UserActivationTransformer extends Transformer
{
    public function transform($activation)
    {

        return [
            'meta'=>[
                'status_code' => '200',
                'status'     => 'OK',
                'message' => 'Activation code was sent to the user signup email'
            ],
            'activation' => [
                'id'    => $activation->id,
                'agent_id' => $activation->agent_id,
                'status' => $activation->status,
                'user_id' => $activation->user_id
            ]
        ];
    }
}
