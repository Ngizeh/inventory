<?php

namespace UzaPoint\Api\V1\User;
use UzaPoint\AgentRequest;
use UzaPoint\Http\Controllers\Controller;
use Illuminate\Http\Request;
use UzaPoint\Repositories\AgentRequestRepository;
use UzaPoint\Repositories\UserDetailsRepository;
use UzaPoint\Repositories\UserRepository;

class UserController extends Controller
{
    use ValidateUserDetails, ValidateUserActivation, ValidateUserDecline;

    public function addMoreDetails(Request $request, UserDetailsRepository $userDetailsRepository){

        $this->validateUserDetails($request->all());

        if($userDetailsRepository->store($request)){

            return $this->response->created();
        }

        return $this->response->errorInternal('An Error Occurred');
    }

    public function activateUser(Request $request, UserRepository $userRepository, UserActivationTransformer $userActivationTransformer){

        $this->validateUserActivation($request->all());

        if($activation = $userRepository->activateUser($request->user_id, $request->agent_id, new AgentRequestRepository(new AgentRequest()))){

            return $userActivationTransformer->transform($activation);
        }
        return $this->response->errorInternal('An Error Occurred');

    }

    public function declineUserRequest(Request $request, UserRepository $userRepository, UserDeclineTransformer $userDeclineTransformer){

        $this->validateUserDecline($request->all());

        if($decline = $userRepository->declineUser($request->user_id, $request->agent_id, new AgentRequestRepository(new AgentRequest()), $request->information)){

            return $userDeclineTransformer->transform($decline);
        }

        return $this->response->errorInternal('An Error Occurred');
    }

    public function get(UserRepository $userRepository, UserTransformer $userTransformer){

        return  $userTransformer->transform($userRepository->getUser());

    }

}