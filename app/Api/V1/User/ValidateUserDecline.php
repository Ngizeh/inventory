<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 4:53 PM
 */

namespace UzaPoint\Api\V1\User;
use Illuminate\Support\Facades\Validator;
use UzaPoint\Api\V1\Validator\ApiValidator;


trait ValidateUserDecline
{

    public function validateUserDecline($data){

        //Start the validation process
        $validator = Validator::make($data, [

            'information' => 'required',
            'user_id'     => 'required|numeric',
            'agent_id'    => 'required|numeric'

        ]);

        $apiValidator = new ApiValidator;

        $apiValidator->validate($validator);
    }

}