<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 12:37 PM
 */

namespace UzaPoint\Api\V1\User;


use UzaPoint\Api\V1\Roles\UserRolesTransformer;
use UzaPoint\Api\V1\Transformers\Transformer;

class UserTransformer extends Transformer
{
    public function transform($user)
    {
        $user_roles_transformer = new UserRolesTransformer();

        return [
            'meta'=>[
                'status_code' => '200',
                'status'     => 'OK',
            ],
            'user' => [
                'id'    => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'roles' => $user_roles_transformer->transformCollection($user->roles()->get()->toArray())
            ]
        ];
    }
}
