<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 4:53 PM
 */

namespace UzaPoint\Api\V1\Account;
use Illuminate\Support\Facades\Validator;
use UzaPoint\Api\V1\Validator\ApiValidator;


trait ValidateActivationDetails
{

    public function validateActivationDetails($data){

        //Start the validation process
        $validator = Validator::make($data, [

            'email' => 'required',
            'code'     => 'required|max:5',
        ]);

        $apiValidator = new ApiValidator;

        $apiValidator->validate($validator);
    }

}