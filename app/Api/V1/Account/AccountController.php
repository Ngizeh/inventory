<?php

namespace UzaPoint\Api\V1\Account;
use Illuminate\Http\Request;
use UzaPoint\ActivationCode;
use UzaPoint\Api\V1\User\ValidateUserDetails;
use UzaPoint\Http\Controllers\Controller;
use UzaPoint\Repositories\AccountRepository;
use UzaPoint\Repositories\ActivationCodeRepository;
use UzaPoint\Repositories\UserRepository;
use UzaPoint\User;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/18/16
 * Time: 2:48 PM
 */
class AccountController extends Controller
{
    use ValidateActivationDetails;

    public function activate(Request $request, AccountRepository $accountRepository){

        $this->validateActivationDetails($request->all());

        if($accountRepository->activate($request->email, $request->code, new UserRepository(new User()), new ActivationCodeRepository(new ActivationCode()))){

            return response()->json([
                                    'status_code' => 200,
                                    'status'      => 'ok',
                                    'message' => 'Account was activated successfully, registration link sent to user sign up email'
                                    ]);
        }
        return $this->response->error('Activation Details Not Found, Try Again', 404);
    }
}