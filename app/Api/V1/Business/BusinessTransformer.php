<?php

namespace UzaPoint\Api\V1\Business;
use UzaPoint\Api\V1\Transformers\Transformer;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 1:05 PM
 */
class BusinessTransformer extends Transformer
{

    public function transform($business)
    {
        return [
                'business_name' => $business['business_name'],
                'location'      => $business['location'],
                'user_id'       => $business['user_id'],
        ];
    }
}