<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 12:54 PM
 */

namespace UzaPoint\Api\V1\Auth;
use Illuminate\Support\Facades\Validator;
use UzaPoint\Api\V1\Validator\ApiValidator;


trait SignUpValidator
{
    /**
     * Validate a new signup request
     * @param $userData
     * @param $signup_fields
     * @return mixed
     * @throws \Dingo\Api\Exception\ValidationHttpException
     */
    public function validateSignup($data){

        //Start the validation process
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone_number' => 'required|max:12',
            'business_name' => 'required|max:100',
            'location'      => 'required|max:100'
        ]);

        $apiValidator = new ApiValidator;

        $apiValidator->validate($validator);
    }

}