<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 1:41 PM
 */

namespace UzaPoint\Api\V1\Auth;
use Illuminate\Support\Facades\Validator;
use UzaPoint\Api\V1\Validator\ApiValidator;


trait LoginValidator
{
    /**
     * Validate a new signup request
     * @param $userData
     * @param $signup_fields
     * @return mixed
     * @throws \Dingo\Api\Exception\ValidationHttpException
     */
    public function validateLogin($data){

        //Start the validation process
        $validator = Validator::make($data, [
            'email' => 'required|email|max:255',
            'password' => 'required'
        ]);

        $apiValidator = new ApiValidator;

        $apiValidator->validate($validator);
    }

}