<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 12:37 PM
 */

namespace UzaPoint\Api\V1\Auth;


use UzaPoint\Api\V1\Transformers\Transformer;
use UzaPoint\Api\V1\Business\BusinessTransformer;
use UzaPoint\Repositories\BusinessRepository;
use UzaPoint\UserBusiness;

class RegistrationTransformer extends Transformer
{
    public function transform($user)
    {
        $business_transformer = new BusinessTransformer();
        $business_repository = new BusinessRepository(new UserBusiness());

        return [
            'meta'=>[
                'status_code' => '200',
                'status'     => 'OK',
                'message' => 'Thanks for signing up, UzaPoint Agents will get back to you shortly'
            ],
            'user' => [
                'user_id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
            ],
            'businesses' => $business_transformer->transformCollection($business_repository->getUserBusinesses($user->id)->toArray())
        ];
    }
}