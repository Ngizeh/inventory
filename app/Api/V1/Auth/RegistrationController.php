<?php

namespace UzaPoint\Api\V1\Auth;
use Dingo\Api\Routing\Helpers;
use UzaPoint\Api\V1\Auth\SignUpValidator;
use UzaPoint\Repositories\RegistrationRepository;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 10:11 AM
 */
class RegistrationController
{
    /**
     * The dingo API package routing helper
     */
    use Helpers;
    use SignUpValidator;

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(Request $request, RegistrationRepository $registrationRepository, RegistrationTransformer $registrationTransformer)
    {
        $this->validateSignup($request->all());

        if($user = $registrationRepository->create($request->all())){

            return $registrationTransformer->transform($user);
        }

        return $this->response->error('An error occurred while creating the user', 500);
    }

}