<?php

namespace UzaPoint\Api\V1\POS;
use Illuminate\Http\Request;
use UzaPoint\ActivationCode;
use UzaPoint\Api\V1\User\ValidateUserDetails;
use UzaPoint\Http\Controllers\Controller;
use UzaPoint\Inventory;
use UzaPoint\Repositories\AccountRepository;
use UzaPoint\Repositories\ActivationCodeRepository;
use UzaPoint\Repositories\UserRepository;
use UzaPoint\SaleTemp;
use UzaPoint\User;
use Illuminate\Support\Facades\Input;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/18/16
 * Time: 2:48 PM
 */
class PosController extends Controller
{

    public function index(){


        return response()->json(Inventory::all());

    }

    public function store()
    {
        $SaleTemps = new SaleTemp;
        $SaleTemps->inventory_id = Input::get('item_id');
        $SaleTemps->cost_price = Input::get('cost_price');
        $SaleTemps->selling_price = Input::get('selling_price');
        $SaleTemps->quantity = 1;
        $SaleTemps->total_cost = Input::get('cost_price');
        $SaleTemps->total_selling = Input::get('selling_price');
        $SaleTemps->save();
        return $SaleTemps;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getSaleTemp()
    {
        return response()->json(SaleTemp::with('inventory')->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $SaleTemps = SaleTemp::find($id);


        $SaleTemps->quantity = Input::get('quantity');
        $SaleTemps->total_cost = Input::get('total_cost');
        $SaleTemps->total_selling = Input::get('total_selling');
        $SaleTemps->save();
        return $SaleTemps;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        SaleTemp::destroy($id);
    }
}