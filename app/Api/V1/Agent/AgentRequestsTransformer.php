<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 12:37 PM
 */

namespace UzaPoint\Api\V1\Agent;


use UzaPoint\Api\V1\Transformers\Transformer;
use UzaPoint\Api\V1\Business\BusinessTransformer;
use UzaPoint\Repositories\BusinessRepository;
use UzaPoint\UserBusiness;

class AgentRequestsTransformer extends Transformer
{
    public function transform($request)
    {
        $business_transformer = new BusinessTransformer();
        $business_repository = new BusinessRepository(new UserBusiness());

        return [
                'id'    => $request['id'],
                'agent_id' => $request['agent_id'],
                'status' => $request['status'],
                'user_id' => $request['user_id'],
                'businesses' => $business_transformer->transformCollection($business_repository->getUserBusinesses($request['user_id'])->toArray())
        ];
    }
}
