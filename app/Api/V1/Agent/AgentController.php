<?php

namespace UzaPoint\Api\V1\Agent;


use UzaPoint\Repositories\AgentRepository;
use UzaPoint\Repositories\AgentRequestRepository;
use UzaPoint\Repositories\UserRepository;
use UzaPoint\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AgentController extends Controller
{

    use ValidateAgent;

    public function store(AgentRepository $agentRepository, Request $request, UserRepository $userRepository){

        $this->validateAgent($request->all());

        $agent = $agentRepository->store($request);

        if($userRepository->store($agent->name, $agent->email, $request->password)){

            return $this->response->created();
        }

        return $this->response->errorInternal('An Error Occurred');
    }

    public function getRequests($agent_id, AgentRequestsTransformer $agentRequestsTransformer, AgentRequestRepository $agentRequestRepository){

        $requests = $agentRequestRepository->getAgentRequests($agent_id)->toArray();

        return $this->respond(['data' => [$agentRequestsTransformer->transformCollection($requests)],
                                'meta'=>[
                                        'status_code' => '200',
                                        'status'     => 'OK',
                                        ]
                             ]);
    }
}
