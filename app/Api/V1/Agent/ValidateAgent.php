<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 4:53 PM
 */

namespace UzaPoint\Api\V1\Agent;
use Illuminate\Support\Facades\Validator;
use UzaPoint\Api\V1\Validator\ApiValidator;


trait ValidateAgent
{

    public function validateAgent($data){

        //Start the validation process
        $validator = Validator::make($data, [
            'name' => 'required',
            'id_number' => 'required',
            'postal_address' => 'required',
            'phone_number'   => 'required',
            'location'      => 'required',
            'email'         => 'required|email|unique:agents|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        $apiValidator = new ApiValidator;

        $apiValidator->validate($validator);
    }

}