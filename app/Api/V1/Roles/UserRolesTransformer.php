<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 12:37 PM
 */

namespace UzaPoint\Api\V1\Roles;


use UzaPoint\Api\V1\Transformers\Transformer;


class UserRolesTransformer extends Transformer
{
    public function transform($role)
    {
        return [
                'id'    => $role['id'],
                'name' => $role['name'],
                'description' => $role['description'],
        ];
    }
}
