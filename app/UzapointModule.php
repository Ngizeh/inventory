<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class UzapointModule extends Model
{
    /**
     * The database View Used by this model
     * @var string
     */
    protected $table = 'uzapoint_modules';

    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'status'
    ];

    /**
     * UzaPointModule User Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){

        return $this->belongsTo(User::class);
    }

    public function isEnabled(){

        if($this->status == 1){
            return true;
        }
        return false;
    }
}
