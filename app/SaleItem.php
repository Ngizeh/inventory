<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class SaleItem extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'sale_items';

    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'sale_id',
        'inventory_id',
        'cost_price',
        'selling_price',
        'quantity',
        'total_cost',
        'total_selling'
    ];

    public function inventory(){

        return $this->belongsTo(Inventory::class);
    }
}
