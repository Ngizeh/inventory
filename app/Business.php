<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $table = 'businesses';

    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'business_name',
        'location'
    ];

    /**
     * Business UserBusiness Inventory
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_businesses(){

        return $this->hasMany(UserBusiness::class);
    }

    /**
     * Business BusinessInventoryCategory relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function business_inventory_categories(){

        return $this->hasMany(BusinessInventoryCategory::class);
    }




}
