<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'inventories';

    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [
        'product_name',
        'product_id',
        'product_quantity',
        'purchase_price',
        'selling_price'
    ];

    /**
     * Inventory BusinessInventoryCategory relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(){

        return $this->hasMany(CategoryProduct::class);
    }

}
