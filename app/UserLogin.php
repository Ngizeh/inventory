<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class UserLogin extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'user_logins';

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [

        'status'
    ];

    /**
     * UserLogin User relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){

        return $this->belongsTo(User::class);
    }
}
