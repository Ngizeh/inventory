<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    /**
     * The table used by this model
     * @var string
     *
     */
    protected $table = 'agents';

    /**
     * All the fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'name',
        'id_number',
        'postal_address',
        'phone_number',
        'location',
        'email'
    ];

    /**
     * Agent AgentRequest Relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function request(){

        return $this->hasMany(AgentRequest::class);
    }
}
