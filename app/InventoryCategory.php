<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class InventoryCategory extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'inventory_categories';

    /**
     * The fields that can be mass assigned
     * @var array
     */
    protected $fillable = [
        'category_name',
        'category_description',
    ];

    /**
     * InventoryCategory BusinessInventoryCategory relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function business_inventory_categories(){

        return $this->hasMany(BusinessInventoryCategory::class);
    }

    /**
     * Inventory Category CategoryProduct relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function category(){
        return $this->hasMany(CategoryProduct::class, 'inventory_category_id');
    }
}
