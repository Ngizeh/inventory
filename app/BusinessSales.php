<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class BusinessSales extends Model
{
    /**
     * The table used by this repository
     * @var string
     */
    protected $table = 'business_sales';

    /**
     * The fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'business_id',
        'sale_id'
    ];

    public function sales(){

        return $this->belongsTo(Sale::class);

    }

    public function business(){

        return $this->belongsTo(Business::class);
    }

}
