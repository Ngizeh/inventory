<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    /**
     * Table used by this model
     * @var string
     */
    protected $table = 'category_products';

    protected $fillable = [

        'inventory_id',
        'inventory_category_id'
    ];

    /**
     * CategoryProduct Inventory relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inventory(){

        return $this->belongsTo(Inventory::class);
    }

    public function category(){
        return $this->belongsTo(InventoryCategory::class, 'inventory_category_id');
    }
}
