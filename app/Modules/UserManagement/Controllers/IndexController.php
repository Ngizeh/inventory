<?php namespace UzaPoint\Modules\UserManagement\Controllers;


use Illuminate\Support\Facades\Auth;
use UzaPoint\Http\Controllers\Controller;

/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the Skeleton.
 */
class IndexController extends Controller
{
	public function index()
	{
        if(Auth::check()){
            if(Auth::user()->hasRole('admin')){

                return redirect()->route('modules.index');
            }else{
                return view('UserManagement::index');
            }
        }else {
            return view('welcome');
        }
	}

	public function users(){

	    return view('UserManagement::users.index');
    }
}

