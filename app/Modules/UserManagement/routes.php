<?php
/*
|--------------------------------------------------------------------------
| Skeleton Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Skeleton module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'user-management'], function () {
    Route::get('/', ['as' => 'user-management.index', 'uses' => 'IndexController@index']);
    Route::get('manage', ['as' => 'users.manage', 'uses' => 'IndexController@users']);
});