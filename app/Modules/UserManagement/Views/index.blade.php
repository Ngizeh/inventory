@extends('layouts.app')

@section('content')

    <p>Add a new User</p>

    <form>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label">User Name</label>
            <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" placeholder="Enter member user name"></p>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label">Email</label>
            <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Enter member email"></p>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label">Password</label>
            <div class="col-sm-10">
                <p class="form-control-static"><input type="password" class="form-control" id="inputPassword" placeholder="Enter Password"></p>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 form-control-label">Repeat Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" placeholder="Repeat Password">
            </div>
        </div>
    </form>

    <button type="submit" class="btn btn-rounded">Add User</button>

@endsection