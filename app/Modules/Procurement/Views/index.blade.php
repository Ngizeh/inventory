@extends('layouts.app')

@section('content')

        <section class="tabs-section">
            <div class="tabs-section-nav tabs-section-nav-left">
                <ul class="nav" role="tablist">

                    <li class="nav-item">
                        <a class="nav-link active" href="#tabs-2-tab-2" role="tab" data-toggle="tab">
                            <span class="nav-link-in">Orders</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-2-tab-4" role="tab" data-toggle="tab">
                            <span class="nav-link-in">Purchases</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-2-tab-5" role="tab" data-toggle="tab">
                            <span class="nav-link-in">Create an Order</span>
                        </a>
                    </li>
                </ul>
            </div><!--.tabs-section-nav-->

            <div class="tab-content no-styled profile-tabs">
                <div role="tabpanel" class="tab-pane active" id="tabs-2-tab-2">
                    <section class="box-typical box-typical-padding">
                        <table class="table">
                            <thead>
                            <tr>
                                <td>
                                    Order Number
                                </td>
                                <td>
                                    Item Name
                                </td>
                                <td>
                                    Order Description
                                </td>
                                <td>
                                    From
                                </td>
                                <td>
                                    Quantity
                                </td>
                                <td>
                                   Order Units
                                </td>
                                <td>
                                    Reply
                                </td>
                                <td>
                                    Delete
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    1223121
                                </td>
                                <td>
                                    T-shirts
                                </td>
                                <td>
                                    Red Tshirts for Primary School Kids
                                </td>
                                <td>
                                    Mr. Randadam Madarandaram
                                </td>
                                <td>
                                    300
                                </td>
                                <td>
                                    Item
                                </td>
                                <td>
                                    <a>Reply</a>
                                </td>
                                <td>
                                    <a>Delete</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    1223121
                                </td>
                                <td>
                                    T-shirts
                                </td>
                                <td>
                                    Red Tshirts for Primary School Kids
                                </td>
                                <td>
                                    Mr. Randadam Madarandaram
                                </td>
                                <td>
                                    300
                                </td>
                                <td>
                                    Item
                                </td>
                                <td>
                                    <a>Reply</a>
                                </td>
                                <td>
                                    <a>Delete</a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    1223121
                                </td>
                                <td>
                                    T-shirts
                                </td>
                                <td>
                                    Red Tshirts for Primary School Kids
                                </td>
                                <td>
                                    Mr. Randadam Madarandaram
                                </td>
                                <td>
                                    300
                                </td>
                                <td>
                                    Item
                                </td>
                                <td>
                                    <a>Reply</a>
                                </td>
                                <td>
                                    <a>Delete</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </section>
                </div><!--.tab-pane-->
                <div role="tabpanel" class="tab-pane" id="tabs-2-tab-4">
                    <section class="box-typical profile-settings">
                        <section class="box-typical-section">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        Order Number
                                    </td>
                                    <td>
                                        Item Name
                                    </td>
                                    <td>
                                        Order Description
                                    </td>
                                    <td>
                                        To
                                    </td>
                                    <td>
                                        Quantity
                                    </td>
                                    <td>
                                        Order Units
                                    </td>
                                    <td>
                                        Edit
                                    </td>
                                    <td>
                                        Delete
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        1223121
                                    </td>
                                    <td>
                                        T-shirts
                                    </td>
                                    <td>
                                        Red Tshirts for Primary School Kids
                                    </td>
                                    <td>
                                        Mr. Randadam Madarandaram
                                    </td>
                                    <td>
                                        300
                                    </td>
                                    <td>
                                        Item
                                    </td>
                                    <td>
                                        <a>Edit</a>
                                    </td>
                                    <td>
                                        <a>Delete</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        1223121
                                    </td>
                                    <td>
                                        T-shirts
                                    </td>
                                    <td>
                                        Red Tshirts for Primary School Kids
                                    </td>
                                    <td>
                                        Mr. Randadam Madarandaram
                                    </td>
                                    <td>
                                        300
                                    </td>
                                    <td>
                                        Item
                                    </td>
                                    <td>
                                        <a>Edit</a>
                                    </td>
                                    <td>
                                        <a>Delete</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        1223121
                                    </td>
                                    <td>
                                        T-shirts
                                    </td>
                                    <td>
                                        Red Tshirts for Primary School Kids
                                    </td>
                                    <td>
                                        Mr. Randadam Madarandaram
                                    </td>
                                    <td>
                                        300
                                    </td>
                                    <td>
                                        Item
                                    </td>
                                    <td>
                                        <a>Edit</a>
                                    </td>
                                    <td>
                                        <a>Delete</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </section>
                        <section class="box-typical-section profile-settings-btns">
                            <button type="submit" class="btn btn-rounded">Save Changes</button>
                            <button type="button" class="btn btn-rounded btn-grey">Cancel</button>
                        </section>
                    </section>
                </div><!--.tab-pane-->
                <div role="tabpanel" class="tab-pane" id="tabs-2-tab-5">
                    <section class="box-typical profile-settings">
                        <section class="box-typical-section">
                            <header class="box-typical-header-sm">Info</header>
                            <div class="form-group row">
                                <div class="col-xl-2">
                                    <label class="form-label">
                                        Item Name
                                    </label>
                                </div>
                                <div class="col-xl-4">
                                    <input class="form-control" type="text" value="Tshirts"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xl-2">
                                    <label class="form-label">
                                        Order Description
                                    </label>
                                </div>
                                <div class="col-xl-4">
                                    <input class="form-control" type="text" value="All Tshirts should be red in colour"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xl-2">
                                    <label class="form-label">
                                       To
                                    </label>
                                </div>
                                <div class="col-xl-6">
                                    <input class="form-control" type="text" value="Ramdadam Daramdaramda"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xl-2">
                                    <label class="form-label">
                                        Quantity
                                    </label>
                                </div>
                                <div class="col-xl-6">
                                    <input class="form-control" type="text" value="400"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xl-2">
                                    Order Units
                                </div>
                                <div class="col-xl-4">
                                    <input class="form-control" type="text" value="Item"/>
                                </div>
                            </div>
                        </section>
                        <section class="box-typical-section profile-settings-btns">
                            <button type="submit" class="btn btn-rounded">Save Changes</button>
                            <button type="button" class="btn btn-rounded btn-grey">Cancel</button>
                        </section>
                    </section>
                </div><!--.tab-pane-->

            </div><!--.tab-content-->
        </section><!--.tabs-section-->


@endsection