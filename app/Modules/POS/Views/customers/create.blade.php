@extends('layouts.app')

@section('content')

            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">New Customer</div>
                    <div class="panel-body">
                        {!! Html::ul($errors->all()) !!}

                        {!! Form::open(array('url' => 'pos/customers', 'files' => true)) !!}

                        <div class="form-group">
                            {!! Form::label('name', 'Name',' *') !!}
                            {!! Form::text('name', '', array('class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::text('email', '', array('class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('phone_number', 'Phone Number') !!}
                            {!! Form::text('phone_number', '', array('class' => 'form-control')) !!}
                        </div>


                        <div class="form-group">
                            {!! Form::label('account', 'Account') !!}
                            {!! Form::text('account', '',  array('class' => 'form-control')) !!}
                        </div>

                        {{--<div class="form-group">--}}
                            {{--{!! Form::label('city', 'City') !!}--}}
                            {{--{!! Form::text('city', '',  array('class' => 'form-control')) !!}--}}
                        {{--</div>--}}


                        {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
@endsection