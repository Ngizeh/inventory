@extends('layouts.app')

@section('content')


            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">List Customers</div>

                    <div class="panel-body">
                        <a class="btn btn-small btn-success" href="{{ URL::to('pos/customers/create') }}">New Customer</a>
                        <hr />
                        @if (Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        @endif

                        <table class="table table-striped table-bordered">
                            <thead style="background-color: #00a1f3;color: #ffffff">
                            <tr>
                                <td>Customer ID</td>
                                <td>Customer Name</td>
                                <td>Customer Email</td>
                                <td>Customer Phone</td>
                                <td>Avatar</td>
                            </tr>
                            </thead>
                            <tbody>
                            {{--@foreach($customer as $value)--}}
                                {{--<tr>--}}
                                    {{--<td>{{ $value->id }}</td>--}}
                                    {{--<td>{{ $value->name }}</td>--}}
                                    {{--<td>{{ $value->email }}</td>--}}
                                    {{--<td>{{ $value->phone_number }}</td>--}}
                                    {{--<td>--}}

                                        {{--<a class="btn btn-small btn-info" href="{{ URL::to('customers/' . $value->id . '/edit') }}">{{trans('customer.edit')}}</a>--}}
                                        {{--{!! Form::open(array('url' => 'customers/' . $value->id, 'class' => 'pull-right')) !!}--}}
                                        {{--{!! Form::hidden('_method', 'DELETE') !!}--}}
                                        {{--{!! Form::submit(trans('customer.delete'), array('class' => 'btn btn-warning')) !!}--}}
                                        {{--{!! Form::close() !!}--}}
                                    {{--</td>--}}
                                    {{--<td>{!! Html::image(url() . '/images/customers/' . $value->avatar, 'a picture', array('class' => 'thumb')) !!}</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


@endsection