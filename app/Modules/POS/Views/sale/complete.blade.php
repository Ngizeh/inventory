@extends('layouts.app')
@section('content')
{!! Html::script('js/angular.min.js', array('type' => 'text/javascript')) !!}
{!! Html::script('js/app.js', array('type' => 'text/javascript')) !!}
<style>
table td {
    border-top: none !important;
}
</style>
<div class="container-fluid">
   <div class="row">
        <div class="col-md-12" style="text-align:center">
            UzaPoint Point of Sale
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            Customer: {{$sales->customer->name  }}<br />
            Sale ID: SALE{{$saleItems}}<br />
            Employee: {{$sales->user->name}}<br />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
           <table class="table">
                <tr>
                    <td>Item</td>
                    <td>Price</td>
                    <td>Quantity</td>
                    <td>Total</td>
                </tr>
                @foreach($itemssale as $value)
                <tr>
                    <td>{{$value->inventory->product_name}}</td>
                    <td>{{$value->selling_price}}</td>
                    <td>{{$value->quantity}}</td>
                    <td>{{$value->total_selling}}</td>
                </tr>
                @endforeach
            </table>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            Payment Type: {{$sales->payment_type}}

            <p class="col-md-offset-9">Total Amount: {{ $itemssale->sum('total_selling') }}</p>
        </div>
    </div>
    <hr class="hidden-print"/>
    <div class="row">
        <div class="col-md-8">
            &nbsp;
        </div>
        <div class="col-md-2">
            <button type="button" onclick="printInvoice()" class="btn btn-info pull-right hidden-print">Print Sale</button>
        </div>
        <div class="col-md-2">
            <a href="{{route('pos.index') }}" type="button" class="btn btn-info pull-right hidden-print">New Sale</a>
        </div>
    </div>
</div>
<script>
function printInvoice() {
    window.print();
}
</script>
@endsection