@extends('layouts.app')

@section('content')
    {!! Html::script('js/angular.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('js/sale.js', array('type' => 'text/javascript')) !!}

    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="btn-group pull-left">--}}
                {{--<button type="button" class="btn btn-info">Select Business</button>--}}
                {{--<button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<span class="sr-only">Toggle Dropdown</span>--}}
                {{--</button>--}}
                {{--<div class="dropdown-menu">--}}
                    {{--@foreach($businesses as $business)--}}
                        {{--<a class="dropdown-item" href="{{ route('pos.index', [$business->business->id]) }}">{{ $business->business->business_name }} | {{ $business->business->location }}</a>--}}
                    {{--@endforeach--}}
                    {{--<a class="dropdown-item" href="#">Thika Branch</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <span class="glyphicon glyphicon-hand-left"></span>
                <a href="{{('/')}}">Home</a>
                 &nbsp; ||&nbsp;
                <a href="{{ route('pos.customers.create') }}">New Customer</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-default-radius">
                    <div class="panel-heading ">
                        <span class="glyphicon glyphicon-inbox" aria-hidden="true"></span>&nbsp;
                        <strong>Point Of Sale / P.O.S </strong>
                    </div>

                    <div class="panel-body panel-bottom-border">

                        @if (Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                        @endif
                        {!! Html::ul($errors->all()) !!}

                        <div class="row" ng-controller="SearchItemCtrl">

                            <div class="col-md-4">
                                <div class="panel panel-default ">
                                    <div class="panel-body panel-right-sales_content">
                                        <label>Search Item <input ng-model="searchKeyword" class="form-control"></label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-hover table-item">
                                                    <tr class="col-md-6" ng-repeat="item in items  | filter: searchKeyword | limitTo:10">

                                                        <td class="col-md-6">
                                                            <div class="card">
                                                                {{--<img class="card-img-top" src="..." alt="Omo image">--}}
                                                                <div class="card-block">
                                                                    <h5 class="card-title"><strong>[[ item.product_name  ]]</strong></h5>
                                                                    <p class="card-text">[[ item.product_quantity ]] kg</p>
                                                                    <button class="btn btn-success btn-xs " type="button" ng-click="addSaleTemp(item, newsaletemp)">
                                                                        <span class="glyphicon glyphicon-share-alt icon-size" aria-hidden="true"></span>
                                                                    </button>
                                                                </div>
                                                            </div>

                                                        </td>

                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                        @if($business_no == 0)
                                            {!! Form::open(array('url' => 'pos/sales/'.$default_business->business->id, 'class' => 'form-horizontal')) !!}

                                        @else

                                        {!! Form::open(array('url' => 'pos/sales/'.$biz->id, 'class' => 'form-horizontal')) !!}

                                        @endif
                                        <div class="form-group">
                                            <label for="employee" class="col-sm-3 control-label">Employee</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="employee" value="{{ Auth::user()->name }}" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">

                                    {!! Form::open(array('url' => 'sales', 'class' => 'form-horizontal')) !!}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="invoice" class="col-sm-3 control-label">Invoice:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="invoice" value="@if ($sale) {{$sale->id + 1}} @else 1 @endif" readonly/>
                                            </div>
                                        </div>

                                        <br>
                                        <br>
                                        <div class="form-group">
                                            <label for="customer_id" class="col-sm-3 control-label">Customer:</label>
                                            <div class="col-sm-9">
                                                <select name="customer_id" class="form-control">
                                                    @foreach($customers as $customer)
                                                    <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="payment_type" class="col-sm-4 control-label">Payment Type:</label>
                                            <div class="col-sm-8">
                                                <select name="payment_type" class="form-control ">
                                                    <option value="1" class="select-payment">
                                                        Mpesa
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>
                            <div class="row">
                                <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Item ID</th>
                                        <th>Item Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th>&nbsp;Delete</th>
                                    </tr>
                                    <tr ng-repeat="newsaletemp in saletemp">
                                        <td>[[newsaletemp.inventory_id]]</td><td>[[newsaletemp.inventory.product_name]]</td><td>[[newsaletemp.inventory.selling_price | currency]]</td><td><input type="text" style="text-align:center" autocomplete="off" name="quantity" ng-change="updateSaleTemp(newsaletemp)" ng-model="newsaletemp.quantity" size="2"></td><td>[[newsaletemp.inventory.selling_price * newsaletemp.quantity | currency]]</td><td><button class="btn btn-danger btn-xs" type="button" ng-click="removeSaleTemp(newsaletemp.id)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>
                                    </tr>
                                    </tr>
                                </table>
                                </div>
                            </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="add_payment" class="col-sm-4 control-label">Add Payment:</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Kshs</div>
                                                    <input type="text" class="form-control" id="add_payment" ng-model="add_payment"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div>&nbsp;</div>
                                        <div class="form-group">
                                            <label for="comments" class="col-sm-4 control-label">Comments:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="comments" id="comments" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="supplier_id" class="col-sm-4 control-label amount-label">Total:</label>
                                            <div class="col-sm-8">
                                                <p class="form-control-static amount-paid"><b>[[ sum(saletemp) | currency]]</b></p>
                                            </div>
                                        </div>

                                        <br>
                                        <br>
                                        <div class="form-group">
                                            <label for="amount_due" class="col-sm-4 control-label amount-label">Amount Due:</label>
                                            <div class="col-sm-8">
                                                <p class="form-control-static amount-due"><strong>[[ add_payment - sum(saletemp) | currency]]</strong></p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-success btn-block btn-lg"><strong>Complete Sale</strong>
                                                &nbsp;<span class="glyphicon glyphicon-ok-circle glyphicon-color" aria-hidden="true"></span></button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                {!! Form::close() !!}



                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection