<?php

/*
|--------------------------------------------------------------------------
| Skeleton Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Skeleton module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'pos'], function () {
//    Route::get('/', ['as' => 'pos.index', 'uses' => 'IndexController@index']);
    Route::get('/business/{business_id?}', ['as' => 'pos.index', 'uses' => 'IndexController@index']);
    Route::get('customers', ['as' => 'pos.customers', 'uses' => 'IndexController@customers']);
    Route::get('customers/create', ['as' => 'pos.customers.create', 'uses' => 'IndexController@createCustomer']);
});



