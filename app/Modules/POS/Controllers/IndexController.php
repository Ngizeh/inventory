<?php namespace UzaPoint\Modules\POS\Controllers;


use Illuminate\Support\Facades\Auth;
use UzaPoint\Http\Controllers\Controller;
use UzaPoint\Repositories\BusinessRepository;
use UzaPoint\Sale;
use UzaPoint\Customer;

/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the Skeleton.
 */
class IndexController extends Controller
{

	public function index(BusinessRepository $businessRepository, $business_id = null)
	{
        if(Auth::check()){
            if(Auth::user()->hasRole('admin')){

                return redirect()->route('modules.index');
            }else{

                $sale = Sale::orderBy('id', 'desc')->first();
                $customers = Customer::all();

                $businesses = $businessRepository->getUserBusinesses(Auth::user()->id);

                if($business_id == null){
                    $default_business = $businessRepository->getFirstBusiness(Auth::user()->id);

                    $business_id = $default_business->business_id;

//                    dd($business_id);
                    $business_no = 0;

                }else{

//                    dd($business_id);

                    $biz = $businessRepository->getBusiness($business_id);

                    $business_no = 1;


                }


                return view('POS::index', compact('sale', 'customers', 'businesses', 'biz', 'default_business', 'business_no', 'business_id'));
            }
        }else {
            return view('welcome');
        }
	}

	public function customers(){

	    return view('POS::customers.index');
    }

    public function createCustomer(){

        return view('POS::customers.create');
    }

}

