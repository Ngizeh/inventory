<?php 

/*
|--------------------------------------------------------------------------
| Skeleton Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Skeleton module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'skeleton', 'namespace' => 'UzaPoint\Modules\Skeleton\Controllers'], function () {
	Route::get('/', ['as' => 'skeleton.index', 'uses' => 'IndexController@index']);
	Route::get('model-test', ['as' => 'module-one.modelTest', 'uses' => 'IndexController@modelTest']);
});