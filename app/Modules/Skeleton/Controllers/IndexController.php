<?php namespace UzaPoint\Modules\Skeleton\Controllers;


use Illuminate\Support\Facades\Auth;
use UzaPoint\Http\Controllers\Controller;
use UzaPoint\Modules\Skeleton\Models\TestModel;


/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the Skeleton.
 */
class IndexController extends Controller
{
	function __construct( TestModel $testModel )
	{
		$this->testModel = $testModel;
	}

	public function index()
	{
		// Skeleton is the module name and dummy is the blade file
		// you can specify Skeleton::someFolder.file if your file exists
		// inside a folder. Also the blade will use the same syntax i.e.
		// ModuleName::viewName

        if(Auth::check()){
            if(Auth::user()->hasRole('admin')){

                return redirect()->route('modules.index');
            }else{
                return redirect()->route('app.index');
            }
        }else {

            return view('welcome');
        }
	}

	public function modelTest()
	{
		return $this->testModel->getAny();
	}
}

