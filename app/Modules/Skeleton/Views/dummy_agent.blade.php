@extends('layouts.app')

@section('content')


	@if($requests->count())
	<p>Your Requests</p>

	<table class="table">
		<thead>
		<tr>
			<td>
				Customer Name
			</td>
			<td>
				Email
			</td>
			<td>
				Status
			</td>
		</tr>
		</thead>

		<tbody>
		@foreach($requests as $request)
		<tr>
			<td>
				{{ $request->user->name }}
			</td>
			<td>
				{{ $request->user->email }}
			</td>
			<td>
				@if($request->status == 0)
					<button class="btn btn-info">Initiated</button>
				@endif

				@if($request->status == 1)
						<span class="alert alert-info">Request Verified</span>
				@endif

					@if($request->status == 2)
						<span class="alert alert-info">Request Approved</span>
					@endif

					@if($request->status == 3)
						<span class="alert alert-warning">Request Declined</span>
					@endif
			</td>
			@if($request->status  == 0)
			<td>
				<a href="{{ route('create.customer_details', [$request->user->id]) }}">Upload More Data</a>
			</td>
			@endif


			@if($request->status  == 1)

				<td>
					<form action="{{ route('activate.user', [$request->user->id, $request->agent_id]) }}" method="post">
						{{ csrf_field() }}
						<button class="btn" style="color: turquoise;" type="submit">Send Activation Code</button>
					</form>
				</td>

				<td>

                    <a class="btn btn-warning" href="{{ route('get.decline', [$request->user->id, $request->agent_id]) }}">Decline Request</a>

				</td>

			@endif

			@if($request->status  == 2)

				<td>
					<form action="{{ route('activate.user', [$request->user->id, $request->agent_id]) }}" method="post">
						{{ csrf_field() }}
						<button class="btn" type="submit" style="color: turquoise;">Resend Activation Code</button>
					</form>
				</td>

			@endif

			@if($request->status  == 3)

				<td>
					<form action="{{ route('activate.user', [$request->user->id, $request->agent_id]) }}" method="post">
						{{ csrf_field() }}
						<button class="btn btn-info" type="submit">Activate</button>
					</form>
				</td>

			@endif

		</tr>
		@endforeach
		</tbody>
	</table>

	@else
		<h3>You have no requests at this moment</h3>
	@endif

@endsection