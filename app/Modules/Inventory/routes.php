<?php

/*
|--------------------------------------------------------------------------
| Skeleton Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Skeleton module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'inventory'], function () {
    Route::get('/business/{business_id?}', ['as' => 'inventory.index', 'uses' => 'IndexController@index']);
    Route::get('create/{business_id?}', ['as' => 'inventory.create', 'uses' => 'IndexController@create']);

    Route::get('/update', ['as' => 'inventory.update', 'uses' => 'IndexController@update']);

    Route::get('/categories/{business_id?}', ['as' => 'inventory.category', 'uses' => 'IndexController@getCategory']);

    Route::get('/datatables/get',     ['as'  => 'inventory.datatables',  'uses' => 'IndexController@getIndex']);

    Route::get('/datatables/{business_id?}',     ['as'  => 'inventory.data',  'uses' => 'IndexController@anyData']);

    Route::post('/ids/post', ['as' => 'inventoryIds', 'uses' => 'IndexController@ids']);

    Route::post('/{business_id}', ['as' => 'saveInventory', 'uses' => 'IndexController@store']);



});

Route::group(['prefix' => 'categories'], function () {

    Route::get('/inventory/{inventory_id}', ['as' => 'getInventoryCategories', 'uses' => 'IndexController@getInventoryCategories']);

    Route::get('/datatables/{inventory_id}',     ['as'  => 'getInventoryCategoryData',  'uses' => 'IndexController@inventoryCategoryData']);



});




