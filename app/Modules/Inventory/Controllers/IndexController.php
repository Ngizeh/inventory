<?php namespace UzaPoint\Modules\Inventory\Controllers;


use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use UzaPoint\BusinessInventory;
use UzaPoint\BusinessInventoryCategory;
use UzaPoint\Http\Controllers\Controller;
use UzaPoint\Inventory;
use UzaPoint\InventoryCategory;
use UzaPoint\Repositories\BusinessInventoryRepository;
use UzaPoint\Repositories\BusinessRepository;
use UzaPoint\Repositories\CategoryProductRepository;
use UzaPoint\Repositories\InventoryRepository;
use Yajra\Datatables\Facades\Datatables;
use UzaPoint\Http\Requests\SaveInventoryRequest;

/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the Skeleton.
 */
class IndexController extends Controller
{

    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        return view('Inventory::index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData($business_id = null, BusinessInventoryRepository $businessInventoryRepository, BusinessRepository $businessRepository)
    {

        debug($business_id);

        if($business_id == null){

           $business_code = $businessRepository->getFirstBusiness(Auth::user()->id)->business_id;


            $inventories = $businessInventoryRepository->getBusinessInventory($business_code);

        }else{

            $inventories = $businessInventoryRepository->getBusinessInventory($business_id);

        }

//        debug($inventories);

        return Datatables::of($inventories)


            ->addColumn('checkbox', function ($inventory) {

                return '<input type="checkbox" name="'. $inventory->id .'" id="'. $inventory->id .'" />';
            })
            ->addColumn('category', function ($inventory) {

                return '<a href="'. route("getInventoryCategories", [$inventory->id]) .'" type="checkbox"/>Categories</a>';
            })
            ->addColumn('actions', function ($inventory) {
                return '<div class="btn-group">
                          <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                          </button>
                          <div class="dropdown-menu">
                                       <a class="dropdown-item" href="#">Export to PDF</a>
                                       <a class="dropdown-item" href="#">Export to Excel</a>
                                       <a class="dropdown-item" href="#">Delete Products</a>
                          </div>    
                        </div>';
            })
            ->make(true);
    }

	public function index(BusinessRepository $businessRepository, $business_id = null)
	{

        if(Auth::check()){
            if(Auth::user()->hasRole('admin')){

                return redirect()->route('modules.index');
            }else{

                $businesses = $businessRepository->getUserBusinesses(Auth::user()->id);

                if($business_id == null){
                    $default_business = $businessRepository->getFirstBusiness(Auth::user()->id);

                    $business_id = $default_business->business_id;

//                    dd($business_id);
                    $business_no = 0;

                }else{

//                    dd($business_id);

                    $biz = $businessRepository->getBusiness($business_id);

                    $business_no = 1;


                }


                return view('Inventory::index', compact('businesses', 'biz', 'default_business', 'business_no', 'business_id'));
            }
        }else {
            return view('welcome');
        }
	}

	public function create($business_id = null, BusinessRepository $businessRepository){

        if($business_id == null){

            $business_id = $businessRepository->getFirstBusiness(Auth::user()->id)->business_id;
        }


        $categoryIds = BusinessInventoryCategory::where('business_id', $business_id)->pluck('inventory_category_id')->toArray();

        $categories = InventoryCategory::whereIn('id', $categoryIds)->get();

        $businesses = $businessRepository->getNotDefaultBusinesses(Auth::user()->id, $business_id);

        return view('Inventory::create', compact('business_id', 'businesses', 'categories'));
    }

    public function update(Request $request){
        dd($request->all());
    }

    public function getCategory($business_id = null, BusinessRepository $businessRepository){

        if($business_id == null){

            $business_id = $businessRepository->getFirstBusiness(Auth::user()->id)->id;
        }

        $businesses = $businessRepository->getNotDefaultBusinesses(Auth::user()->id, $business_id);

        return view('Inventory::category.create', compact('business_id', 'businesses'));
    }

    public function getInventoryCategories($inventory_id){

        return view('Inventory::category.index_inventory', compact('inventory_id'));

    }

    public function inventoryCategoryData($inventory_id, CategoryProductRepository $categoryProductRepository){

        $categories = $categoryProductRepository->getCategoryInventory($inventory_id);

        return Datatables::of($categories)


            ->addColumn('checkbox', function ($category) {

                return '<input type="checkbox" name="'. $category->id .'" id="'. $category->id .'" />';
            })
//            ->addColumn('category', function ($inventory) {
//
//                return '<a href="'. route("getInventoryCategories", [$inventory->id]) .'" type="checkbox"/>Categories</a>';
//            })
            ->addColumn('actions', function ($category) {
                return '<div class="btn-group">
                          <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                          </button>
                          <div class="dropdown-menu">
                                       <a class="dropdown-item" href="#">Export to PDF</a>
                                       <a class="dropdown-item" href="#">Export to Excel</a>
                                       <a class="dropdown-item" href="#">Delete Products</a>
                          </div>    
                        </div>';
            })
            ->make(true);
    }

    public function ids(Request $request, InventoryRepository $inventoryRepository){

        $ids = $request->id;

        if($ids == null){

           Session::flash('flash_message_error', 'No items selected to perform this action');

           return redirect()->back();
        }


        Session::flash('flash_message', 'Items were deleted successfully');

        $inventoryRepository->deleteInventory($ids);

        return redirect()->back();
    }

    public function store($business_id, SaveInventoryRequest $saveInventoryRequest){

        $inventory = Inventory::create([
            'product_name' => $saveInventoryRequest->product_name,
            'product_id'   => $saveInventoryRequest->product_id,
            'product_quantity' => $saveInventoryRequest->product_quantity,
            'purchase_price' => $saveInventoryRequest->purchase_price,
            'selling_price'     => $saveInventoryRequest->selling_price
        ]);


        BusinessInventory::create([

            'business_id' => $business_id,
            'inventory_id' => $inventory->id

        ]);


        Session::flash('flash_message', 'Inventory Item was added successfully');

        return redirect()->back();

    }

}

