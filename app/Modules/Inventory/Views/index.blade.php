@extends('layouts.app')
{{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.6/jq-2.2.3/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.css"/>--}}


@section('content')


    <div class="row">
        <div class="col-md-12">
        <div class="btn-group pull-left">
            <button type="button" class="btn btn-info">Select Business</button>
            <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu">
                @foreach($businesses as $business)
                <a class="dropdown-item" href="{{ route('inventory.index', [$business->business->id]) }}">{{ $business->business->business_name }} | {{ $business->business->location }}</a>
                @endforeach
                {{--<a class="dropdown-item" href="#">Thika Branch</a>--}}
            </div>
        </div>
        </div>
    </div>

    <br>
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">--}}
                    {{--<p class="text-center">Business: Thika Branch</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div  id="export_actions">--}}

        {{--</div>--}}
    {{--</div>--}}


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    @if($business_no == 0)
                                    <label class="text-center">Business: {{ $default_business->business->business_name }} | {{ $default_business->business->location }} </label>

                                    @endif
                                    @if($business_no == 1)
                                        <label class="text-center">Business: {{ $biz->business_name }} | {{ $biz->location }} </label>
                                    @endif
                                </div>
                                <div class="col-md-offset-6 col-md-3 ">
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-info">Actions</button>
                                        <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <div class="dropdown-menu">
                                            @if($business_no == 0)

                                            <a class="dropdown-item" href="{{ route('inventory.category', [$default_business->business->id]) }}">Add Inventory Category</a>
                                            <a class="dropdown-item" href="{{ route('inventory.create', [$default_business->business->id])  }}">Add Inventory Item</a>

                                            @else

                                                <a class="dropdown-item" href="{{ route('inventory.category', [$biz->id]) }}">Add Inventory Category</a>
                                                <a class="dropdown-item" href="{{ route('inventory.create', [$biz->id])  }}">Add Inventory Item</a>


                                            @endif
                                            {{--<a class="dropdown-item" href="#">Export to PDF</a>--}}
                                            {{--<a class="dropdown-item" href="#">Export to Excel</a>--}}
                                            {{--<a class="dropdown-item" href="#">Delete Products</a>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    <div class="row" style="padding-top: 1%;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered datatable display cell-border order-column stripe select" id="inventory-table">
                                <thead class="dt-head-center">
                                <tr>
                                    <th><input name="select_all" value="1" type="checkbox"></th>
                                    <th style="background-color: darkslateblue;color: #ffffff">Product Name</th>
                                    <th  style="background-color: darkslateblue;color: #ffffff">Product ID</th>
                                    <th  style="background-color: darkslateblue;color: #ffffff">Quantity</th>
                                    <th  style="background-color: darkslateblue;color: #ffffff">Purchase Price</th>
                                    <th  style="background-color: darkslateblue;color: #ffffff">Selling Price</th>
                                    <th  style="background-color: darkslateblue;color: #ffffff">Categories</th>
                                    <th  style="background-color: darkslateblue;color: #ffffff">Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th class="no-search"></th>
                                    <th>Product Name</th>
                                    <th>Product ID</th>
                                    <th>Quantity</th>
                                    <th>Purchase Price</th>
                                    <th>Selling Price</th>
                                    <th class="no-search">Categories</th>
                                    <th class="no-search">Actions</th>
                                </tr>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <form name="frm-example" id="frm-example" method="post" action="{{ route('inventoryIds') }}">

        {{ csrf_field() }}

        {{--<p class="form-group">--}}
            {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
        {{--</p>--}}

        {{--<p class="form-group">--}}
            {{--<b>Data submitted to the server:</b>--}}
        {{--</p><pre id="example-console"></pre>--}}
        {{--<p></p>--}}

    </form>
@endsection
{{--<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.6/jq-2.2.3/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.js"></script>--}}
@push('scripts')
<script>


    //
    // Updates "Select all" control in a data table
    //
    function updateDataTableSelectAllCtrl(table){
        var $table             = table.table().node();
        var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
        var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
        var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);


        // If none of the checkboxes are checked
        if($chkbox_checked.length === 0){
            chkbox_select_all.checked = false;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = false;
            }

            // If all of the checkboxes are checked
        } else if ($chkbox_checked.length === $chkbox_all.length){
            chkbox_select_all.checked = true;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = false;
            }

            // If some of the checkboxes are checked
        } else {
            chkbox_select_all.checked = true;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = true;
            }
        }
    }

    $(function() {
        var rows_selected = [];

        // Array holding selected row IDs
        var table = $('#inventory-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('inventory.data', [$business_id]) !!}',
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'width': '1%',
                'className': 'dt-body-center',
            }],
            'order': [[1, 'asc']],
            'rowCallback': function(row, data, dataIndex){
                // Get row ID
                var rowId = data['id'];
                // If row ID is in the list of selected row IDs
                if($.inArray(rowId, rows_selected) !== -1){
                    $(row).find('input[type="checkbox"]').prop('checked', true);
                    $(row).addClass('selected');
                }
            },
            columns: [
                { data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                { data: 'product_name', name: 'product_name' },
                { data: 'product_id', name: 'product_id' },
                { data: 'product_quantity', name: 'product_quantity' },
                { data: 'purchase_price', name: 'purchase_price' },
                { data: 'selling_price', name: 'selling_price' },
                { data: 'category', name: 'category' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false},
            ],
        });

//        var buttons = new $.fn.dataTable.Buttons(table, {
//            buttons: [
//                'copyHtml5',
//                'excelHtml5',
//                'csvHtml5',
//                'pdfHtml5'
//            ]
//        }).container().appendTo($('#export_actions'));

        // Setup - add a text input to each footer cell
        $('#inventory-table tfoot th:not(.no-search)').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );


        // Apply the search
        table.columns().every( function () {
            var that = this;

            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                            .search( this.value )
                            .draw();
                }
            } );
        } );

        // Handle click on checkbox
        $('#inventory-table tbody').on('click', 'input[type="checkbox"]', function(e){
            var $row = $(this).closest('tr');

            // Get row data
            var data = table.row($row).data();

            // Get row ID
            var rowId = data['id'];

            // Determine whether row ID is in the list of selected row IDs
            var index = $.inArray(rowId, rows_selected);

            // If checkbox is checked and row ID is not in list of selected row IDs
            if(this.checked && index === -1){
                rows_selected.push(rowId);

                // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            } else if (!this.checked && index !== -1){
                rows_selected.splice(index, 1);
            }

            if(this.checked){
                $row.addClass('selected');
            } else {
                $row.removeClass('selected');
            }

            // Update state of "Select all" control
            updateDataTableSelectAllCtrl(table);

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });

//        // Handle click on table cells with checkboxes
//        $('#inventory-table').on('click', 'tbody td, thead th:first-child', function(e){
//            $(this).parent().find('input[type="checkbox"]').trigger('click');
//        });

        // Handle click on "Select all" control
        $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
            if(this.checked){
                $('#inventory-table tbody input[type="checkbox"]:not(:checked)').trigger('click');
            } else {
                $('#inventory-table tbody input[type="checkbox"]:checked').trigger('click');
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });

        // Handle table draw event
        table.on('draw', function(){
            // Update state of "Select all" control
            updateDataTableSelectAllCtrl(table);
        });

        // Handle form submission event
        $('#frm-example').on('submit', function(e){

            e.preventDefault();
            var form = this;

            // Iterate over all selected checkboxes
            $.each(rows_selected, function(index, rowId){
                // Create a hidden element

                console.log(rowId);

                $(form).append(
                        $('<input>')
                                .attr('type', 'hidden')
                                .attr('name', 'id[]')
                                .val(rowId)
                );


            });

            form.submit();


        });
    });
</script>
@endpush