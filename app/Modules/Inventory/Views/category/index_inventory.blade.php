@extends('layouts.app')


@section('content')

    <div class="row" style="padding-top: 1%;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered datatable display cell-border order-column stripe select" id="category-table">
                                <thead class="dt-head-center">
                                <tr>
                                    <th><input name="select_all" value="1" type="checkbox"></th>
                                    <th>Category Name</th>
                                    <th>Category Description</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th class="no-search"></th>
                                    <th>Category Name</th>
                                    <th>Category Description</th>
                                    <th class="no-search">Actions</th>
                                </tr>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@push('scripts')
<script>


    //
    // Updates "Select all" control in a data table
    //
    function updateDataTableSelectAllCtrl(table){
        var $table             = table.table().node();
        var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
        var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
        var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);


        // If none of the checkboxes are checked
        if($chkbox_checked.length === 0){
            chkbox_select_all.checked = false;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = false;
            }

            // If all of the checkboxes are checked
        } else if ($chkbox_checked.length === $chkbox_all.length){
            chkbox_select_all.checked = true;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = false;
            }

            // If some of the checkboxes are checked
        } else {
            chkbox_select_all.checked = true;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = true;
            }
        }
    }

    $(function() {
        var rows_selected = [];

        // Array holding selected row IDs
        var table = $('#category-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('getInventoryCategoryData', [$inventory_id]) !!}',
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'width': '1%',
                'className': 'dt-body-center',
            }],
            'order': [[1, 'asc']],
            'rowCallback': function(row, data, dataIndex){
                // Get row ID
                var rowId = data['id'];
                // If row ID is in the list of selected row IDs
                if($.inArray(rowId, rows_selected) !== -1){
                    $(row).find('input[type="checkbox"]').prop('checked', true);
                    $(row).addClass('selected');
                }
            },
            columns: [
                { data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                { data: 'category_name', name: 'category_name' },
                { data: 'category_description', name: 'category_description' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false},
            ],
        });

//        var buttons = new $.fn.dataTable.Buttons(table, {
//            buttons: [
//                'copyHtml5',
//                'excelHtml5',
//                'csvHtml5',
//                'pdfHtml5'
//            ]
//        }).container().appendTo($('#export_actions'));

        // Setup - add a text input to each footer cell
        $('#category-table tfoot th:not(.no-search)').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );


        // Apply the search
        table.columns().every( function () {
            var that = this;

            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                            .search( this.value )
                            .draw();
                }
            } );
        } );

        // Handle click on checkbox
        $('#category-table tbody').on('click', 'input[type="checkbox"]', function(e){
            var $row = $(this).closest('tr');

            // Get row data
            var data = table.row($row).data();

            // Get row ID
            var rowId = data['id'];

            // Determine whether row ID is in the list of selected row IDs
            var index = $.inArray(rowId, rows_selected);

            // If checkbox is checked and row ID is not in list of selected row IDs
            if(this.checked && index === -1){
                rows_selected.push(rowId);

                // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            } else if (!this.checked && index !== -1){
                rows_selected.splice(index, 1);
            }

            if(this.checked){
                $row.addClass('selected');
            } else {
                $row.removeClass('selected');
            }

            // Update state of "Select all" control
            updateDataTableSelectAllCtrl(table);

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });

//        // Handle click on table cells with checkboxes
//        $('#category-table').on('click', 'tbody td, thead th:first-child', function(e){
//            $(this).parent().find('input[type="checkbox"]').trigger('click');
//        });

        // Handle click on "Select all" control
        $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
            if(this.checked){
                $('#category-table tbody input[type="checkbox"]:not(:checked)').trigger('click');
            } else {
                $('#category-table tbody input[type="checkbox"]:checked').trigger('click');
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });

        // Handle table draw event
        table.on('draw', function(){
            // Update state of "Select all" control
            updateDataTableSelectAllCtrl(table);
        });

        // Handle form submission event
        $('#frm-example').on('submit', function(e){

            e.preventDefault();
            var form = this;

            // Iterate over all selected checkboxes
            $.each(rows_selected, function(index, rowId){
                // Create a hidden element
                $(form).append(
                        $('<input>')
                                .attr('type', 'hidden')
                                .attr('name', 'id[]')
                                .val(rowId)
                );
            });

        });
    });
</script>
@endpush
