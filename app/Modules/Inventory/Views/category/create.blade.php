@extends('layouts.app')
@section('content')
    <section class="box-typical box-panel">
        <header class="box-typical-header">
            <div class="tbl-row">
                <div class="tbl-cell tbl-cell-title">
                    <h3>Add Category Here</h3>
                </div>
            </div>
        </header>
        <div class="box-typical-body">
            <form id="example-form" method="post" action="{{ route('saveCategory', [$business_id] )}}" class="form-wizard" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div>
                    <section>
                        {{--<div class="form-group">--}}
                            {{--<label for="category_name">Include in Other Business</label>--}}

                            {{--<select class="businesses form-control" multiple>--}}
                                {{--@foreach($businesses as $business)--}}
                                {{--<option>--}}
                                    {{--{{ $business->business->business_name  }}--}}
                                    {{--{{ $business->business->location  }}--}}
                                {{--</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}

                        {{--</div>--}}
                        <div class="form-group">
                            <label for="category_name">Category Name</label>
                            <input type="text" class="form-control" id="category_name" name="category_name"  placeholder="Enter a category name" required>
                            <small  class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="category_description">Category Description</label>
                            <input type="text" id="category_description" class="form-control" name="category_description" id="product_id" placeholder="Enter a category description">
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label class="btn btn-default btn-file">--}}
                                {{--Upload Category Image <input type="file" style="display: none;" name="category_image">--}}
                            {{--</label>--}}
                        {{--</div>--}}


                        <div class="form-group">
                            <button type="submit" class="btn btn-rounded btn-inline btn-info-outline">Save Category</button>
                        </div>

                    </section>

                </div>
            </form>
        </div><!--.box-typical-body-->
    </section>

@endsection

@push('scripts')
@include('Inventory::scripts')
<script type="text/javascript">
    $('.businesses').select2();
</script>
@endpush