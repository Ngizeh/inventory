@extends('layouts.app')
@push('css-scripts')
<link href="{{asset('css/multiple_input_css/multiple_input.css')}}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/selectize.bootstrap3.css') }}" type="text/css" rel="stylesheet">
@endpush
@section('content')
    <section class="box-typical box-panel">
        <header class="box-typical-header">
            <div class="tbl-row">
                <div class="tbl-cell tbl-cell-title">
                    <h3>Add Inventory Here</h3>
                </div>
            </div>
        </header>
        <div class="box-typical-body">
            <form id="example-form" method="post" action="{{ route('saveInventory', [$business_id]) }}" class="form-wizard">

                {{ csrf_field() }}
                <div>
                    <section>
                        {{--<div class="form-group">--}}
                            {{--<label for="category_name">Select other Business</label>--}}

                            {{--<select class="businesses form-control" multiple>--}}
                                {{--@foreach($businesses as $business)--}}
                                    {{--<option>--}}
                                        {{--{{ $business->business->business_name  }}--}}
                                        {{--{{ $business->business->location  }}--}}
                                    {{--</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label for="category_name">Select Inventory Category</label>

                            <select name="category_name" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="product_name">Product Name</label>
                            <input type="text" class="form-control" id="product_name" name="product_name"  placeholder="Enter a product name" required>
                            <small  class="form-text text-muted"></small>
                        </div>

                        <div class="form-group">
                            <label for="product_description">Product Description</label>
                            <textarea class="form-control" name="product_description" rows="5" id="product_description"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="product_id">Product ID</label>
                            <input type="text" class="form-control" name="product_id" id="product_id" placeholder="Enter a Product ID">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Quantity</label>
                            <input type="number" class="form-control" name="product_quantity" id="quantity" placeholder="Enter a Quantity">
                        </div>

                        <div class="form-group">
                            <label for="purchase_price">Purchase Price</label>
                            <input type="number" class="form-control" name="purchase_price" id="purchase_price" placeholder="Enter a Purchase Price">
                        </div>

                        <div class="form-group">
                            <label for="selling_price">Selling Price</label>
                            <input type="number" class="form-control" name="selling_price" id="selling_price" placeholder="Enter a Selling Price">
                        </div>

                        <div class="form-group">
                            <label for="alert_quantity">Alert Quantity</label>
                            <input type="number" name="alert_quantity" class="form-control" id="alert_quantity" min="1">
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label for="product_colors">Product Colors</label>--}}

                            {{--<input type="text" id="product_colors" class="form-control" name="product_colors" required>--}}

                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label for="product_colors">Product Sizes</label>--}}
                            {{--<input type="text" id="product_sizes" class="form-control" name="product_sizes" required>--}}

                        {{--</div>--}}


                        <button type="submit" class="btn btn-rounded btn-inline btn-info-outline">Save Inventory Item</button>

                    </section>

                </div>
            </form>
        </div><!--.box-typical-body-->
    </section>
@endsection
@push('scripts')
@include('Inventory::scripts')
<script src="{{asset('js/selectize.min.js')}}"></script>

<script type="text/javascript">
    $('.businesses').select2();
    $('.categories').select2();

    $('#product_colors').selectize({
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });

    $('#product_sizes').selectize({
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });

</script>
@endpush