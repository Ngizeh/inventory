<?php namespace UzaPoint\Modules\Accounting\Controllers;


use Illuminate\Support\Facades\Auth;
use UzaPoint\Http\Controllers\Controller;

/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the Skeleton.
 */
class IndexController extends Controller
{
	public function index()
	{
        if(Auth::check()){
            if(Auth::user()->hasRole('admin')){

                return redirect()->route('modules.index');
            }else{
                return view('Accounting::index');
            }
        }else {
            return view('welcome');
        }
	}

	public function manage(){

	    return view('Accounting::manage');
    }

    public function expenses(){

        return view('Accounting::expenses');
    }
}

