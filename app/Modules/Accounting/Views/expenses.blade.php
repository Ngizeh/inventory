@extends('layouts.app')

@section('content')

    <p>Manage Expenses</p>

    <table class="table">
        <thead>
        <tr>
            <td>
                Expense Name
            </td>
            <td>
                Expense Amount (Kshs)
            </td>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>
                Electricity
            </td>
            <td>
                2,000
            </td>
        </tr>
        <tr>
            <td>
                Water
            </td>
            <td>
                4,000
            </td>
        </tr>
        <tr>
            <td>
                Salary
            </td>
            <td>
                2,000
            </td>
        </tr>
        </tbody>
    </table>



@endsection