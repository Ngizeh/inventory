@extends('layouts.app')

@section('content')

    <p>Manage Accounts</p>

    <table class="table">
        <thead>
        <tr>
            <td>
                Debit (Kshs)
            </td>
            <td>
                Credit (Kshs)
            </td>
            <td>
                Branch
            </td>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>
                1,000,000
            </td>
            <td>
                2,000,000
            </td>
            <td>
                Juja
            </td>
        </tr>
        <tr>
            <td>
                3,000,000
            </td>
            <td>
                4,000,000
            </td>
            <td>
                South B
            </td>
        </tr>
        <tr>
            <td>
                7,000,000
            </td>
            <td>
                2,000,000
            </td>
            <td>
                Nairobi
            </td>
        </tr>
        <tr>
            <td>
                1,000,000
            </td>
            <td>
                2,000,000
            </td>
            <td>
                Narok
            </td>
        </tr>
        <tr>
            <td>
                3,000,000
            </td>
            <td>
                4,000,000
            </td>
            <td>
                Juja
            </td>
        </tr>
        <tr>
            <td>
                7,000,000
            </td>
            <td>
                2,000,000
            </td>
            <td>
                Kiserian
            </td>
        </tr>
        <tr>
            <td>
                1,000,000
            </td>
            <td>
                2,000,000
            </td>
            <td>
                Nakuru
            </td>
        </tr>
        <tr>
            <td>
                3,000,000
            </td>
            <td>
                4,000,000
            </td>
            <td>
                Maziwa
            </td>
        </tr>
        <tr>
            <td>
                7,000,000
            </td>
            <td>
                2,000,000
            </td>
            <td>
                Mailel
            </td>
        </tr>

        </tbody>
    </table>






@endsection