<?php
/*
|--------------------------------------------------------------------------
| Skeleton Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Skeleton module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'accounting'], function () {
    Route::get('/', ['as' => 'accounting.index', 'uses' => 'IndexController@index']);
    Route::get('/manage', ['as' => 'accounting.manage', 'uses' => 'IndexController@manage']);
    Route::get('/expenses', ['as' => 'expenses.manage', 'uses' => 'IndexController@expenses']);
});