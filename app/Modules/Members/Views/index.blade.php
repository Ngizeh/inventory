@extends('layouts.app')

@section('content')


    <p>Add Member</p>

    <form>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label">Member Name</label>
            <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" placeholder="Enter member name"></p>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label">Member Phone</label>
            <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Enter member phone number"></p>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label">Member City</label>
            <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Enter Member city"></p>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 form-control-label">Member Contribution</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" placeholder="Enter Member contribution">
            </div>
        </div>
        <div class="form-group row">
            <label for="exampleSelect" class="col-sm-2 form-control-label">Description</label>
            <div class="col-sm-10">
                <textarea rows="4" class="form-control" placeholder="Enter a simple description about yourself"></textarea>
            </div>
        </div>
    </form>

    <button type="submit" class="btn btn-rounded">Add Member</button>



@endsection