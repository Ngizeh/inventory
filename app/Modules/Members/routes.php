<?php
/*
|--------------------------------------------------------------------------
| Skeleton Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Skeleton module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'members'], function () {
    Route::get('/', ['as' => 'members.index', 'uses' => 'IndexController@index']);
    Route::get('subscriptions', ['as' => 'members.subscriptions', 'uses' => 'IndexController@subscriptions']);
});