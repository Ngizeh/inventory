<?php namespace UzaPoint\Modules\Community\Controllers;


use Illuminate\Support\Facades\Auth;
use UzaPoint\Http\Controllers\Controller;

/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the Skeleton.
 */
class IndexController extends Controller
{
	public function index()
	{
        if(Auth::check()){
            if(Auth::user()->hasRole('admin')){

                return redirect()->route('modules.index');
            }else{
                return view('Community::index');
            }
        }else {
            return view('welcome');
        }
	}
}

