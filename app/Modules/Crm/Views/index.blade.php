@extends('layouts.app')

@section('content')

    <p>Customer Emails</p>

    <table class="table">
        <thead style="background-color: #00a1f3;color: #ffffff;">
        <tr>
            <td>
                Customer
            </td>
            <td>
                Action
            </td>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>
                Jemo
            </td>
            <td>
                <a>Read</a>
            </td>

        </tr>
        <tr>
            <td>
                Mwas
            </td>
            <td>
                <a>Read</a>
            </td>
        </tr>
        <tr>
            <td>
                Renn
            </td>
            <td>
                <a>Read</a>
            </td>

        </tr>
        <tr>
            <td>
                Jemo
            </td>
            <td>
                <a>Read</a>
            </td>
        </tr>
        <tr>
            <td>
                Mwas
            </td>
            <td>
                <a>Read</a>
            </td>

        </tr>
        <tr>
            <td>
                Renn
            </td>
            <td>
                <a>Read</a>
            </td>

        </tr>
        </tbody>
    </table>

@endsection