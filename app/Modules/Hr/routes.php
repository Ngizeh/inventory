<?php
/*
|--------------------------------------------------------------------------
| Skeleton Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Skeleton module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'hr'], function () {
    Route::get('/', ['as' => 'hr.index', 'uses' => 'IndexController@index']);
});