<?php namespace UzaPoint\Modules\Sales\Controllers;


use Illuminate\Support\Facades\Auth;
use UzaPoint\Http\Controllers\Controller;
use UzaPoint\Repositories\BusinessInventoryRepository;
use UzaPoint\Repositories\BusinessRepository;
use Yajra\Datatables\Facades\Datatables;


/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the Skeleton.
 */
class IndexController extends Controller
{
	public function index(BusinessRepository $businessRepository, $business_id = null)
	{
        if(Auth::check()){
            if(Auth::user()->hasRole('admin')){

                return redirect()->route('modules.index');
            }else{

                $businesses = $businessRepository->getUserBusinesses(Auth::user()->id);

                if($business_id == null){
                    $default_business = $businessRepository->getFirstBusiness(Auth::user()->id);

                    $business_id = $default_business->business_id;

//                    dd($business_id);
                    $business_no = 0;

                }else{

//                    dd($business_id);

                    $biz = $businessRepository->getBusiness($business_id);

                    $business_no = 1;


                }

                return view('Sales::index', compact('businesses', 'biz', 'default_business', 'business_no', 'business_id'));
            }
        }else {
            return view('welcome');
        }
	}

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData($business_id = null, BusinessInventoryRepository $businessInventoryRepository, BusinessRepository $businessRepository)
    {

        debug($business_id);

        if($business_id == null){

            $business_code = $businessRepository->getFirstBusiness(Auth::user()->id)->business_id;


            $sales = $businessInventoryRepository->getBusinessSales($business_code);

        }else{

            $sales = $businessInventoryRepository->getBusinessSales($business_id);

        }

       debug($sales);

        return Datatables::of($sales)


            ->addColumn('checkbox', function ($sale) {

                return '<input type="checkbox" name="'. $sale->id .'" id="'. $sale->id .'" />';
            })
            ->addColumn('sale_items', function ($sale) {

                return '<a href="'. route("getSaleItems", [$sale->id]) .'"/>Sale Items</a>';
            })
//            ->addColumn('actions', function ($inventory) {
//                return '<div class="btn-group">
//                          <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//                            Actions
//                          </button>
//                          <div class="dropdown-menu">
//                                       <a class="dropdown-item" href="#">Export to PDF</a>
//                                       <a class="dropdown-item" href="#">Export to Excel</a>
//                                       <a class="dropdown-item" href="#">Delete Products</a>
//                          </div>
//                        </div>';
//            })
            ->make(true);
    }

    public function getSaleItems($sale_id){


        return view('Sales::items.show', compact('sale_id'));

    }

    public function getSaleItemsData($sale_id, BusinessInventoryRepository $businessInventoryRepository, BusinessRepository $businessRepository){

        $sales = $businessInventoryRepository->getSaleItems($sale_id);

        debug($sales);

        return Datatables::of($sales)


            ->addColumn('checkbox', function ($sale) {

                return '<input type="checkbox" name="'. $sale->id .'" id="'. $sale->id .'" />';
            })
            ->addColumn('sale_items', function ($sale) {

                return '<a href="'. route("getSaleItems", [$sale->id]) .'"/>Sale Items</a>';
            })
//            ->addColumn('actions', function ($inventory) {
//                return '<div class="btn-group">
//                          <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//                            Actions
//                          </button>
//                          <div class="dropdown-menu">
//                                       <a class="dropdown-item" href="#">Export to PDF</a>
//                                       <a class="dropdown-item" href="#">Export to Excel</a>
//                                       <a class="dropdown-item" href="#">Delete Products</a>
//                          </div>
//                        </div>';
//            })
            ->make(true);


    }
}

