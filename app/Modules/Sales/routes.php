<?php
/*
|--------------------------------------------------------------------------
| Skeleton Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Skeleton module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'sales'], function () {
    Route::get('/', ['as' => 'sales.index', 'uses' => 'IndexController@index']);

    Route::get('/datatables/{business_id?}',     ['as'  => 'sales.data',  'uses' => 'IndexController@anyData']);

    Route::get('/items/{sale_id}', ['as' => 'getSaleItems', 'uses' => 'IndexController@getSaleItems']);

    Route::get('/datatables/items/{sale_id}', ['as' => 'sales.items', 'uses' => 'IndexController@getSaleItemsData']);


});