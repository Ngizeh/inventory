<?php
/*
|--------------------------------------------------------------------------
| Skeleton Module Routes
|--------------------------------------------------------------------------
|
| All the routes related to the Skeleton module have to go in here. Make sure
| to change the namespace in case you decide to change the
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'merchant'], function () {
    Route::get('/', ['as' => 'merchant.index', 'uses' => 'IndexController@index']);
});