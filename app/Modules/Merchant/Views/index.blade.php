@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <table class="table">
                <thead style="background-color: #00a1f3;color: #ffffff">
                    <td><strong>Name</strong></td>
                    <td><strong>Email</strong></td>
                    <td><strong>Phone</strong></td>
                    <td><strong>User Details</strong></td>
                </thead>
                <tbody>
                <tr>
                    <td>John</td>
                    <td>john@uza.com</td>
                    <td>0712345435</td>
                   <td><a href="#">user details</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection