<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class UserBusiness extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'users_businesses';

    /**
     * All the fields that can be mass assigned
     * @var array
     */
    protected $fillable = [
        'business_id'
    ];


    /**
     * UserBusiness User Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * UserBusiness Business Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function business(){
        return $this->belongsTo(Business::class);
    }
}
