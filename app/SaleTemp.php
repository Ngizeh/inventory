<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class SaleTemp extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'sales_temp';

    protected $fillable = [

        'inventory_id',
        'cost_price',
        'selling_price',
        'quantity',
        'total_cost',
        'total_selling'

    ];

    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }
}
