<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'sales';

    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'customer_id',
        'user_id',
        'payment_type',
        'comments'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
