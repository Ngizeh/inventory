<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/24/16
 * Time: 4:59 PM
 */

namespace UzaPoint\Repositories;
use UzaPoint\BusinessInventory;
use UzaPoint\BusinessSales;
use UzaPoint\Inventory;
use UzaPoint\Sale;
use UzaPoint\SaleItem;


class BusinessInventoryRepository
{
    /**
     * The model used by this table
     * @var
     */
    protected $model;

    public function __construct(BusinessInventory $businessInventory)
    {
       $this->model = $businessInventory;
    }

    public function getBusinessInventory($business_id){


        $inventoryIds =  $this->model->where('business_id', $business_id)->pluck('inventory_id');

        debug($inventoryIds);


        return Inventory::whereIn('id', $inventoryIds)->latest()->get();

    }

    public function getBusinessSales($business_id){


        $saleIds =  BusinessSales::where('business_id', $business_id)->pluck('sale_id');


        return Sale::with('user', 'customer')->whereIn('id', $saleIds)->latest()->get();

    }

    public function getSaleItems($sale_id){


        return SaleItem::where('sale_id', $sale_id)->with('inventory')->latest()->get();

    }

}