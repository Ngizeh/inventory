<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 7:04 AM
 */

namespace UzaPoint\Repositories;
use UzaPoint\AgentRequest;
use UzaPoint\Mailers\AppMailer;


class AccountRepository
{
    /**
     * The request model
     * @var
     */
    protected $request_model;

    /**
     * AccountRepository constructor.
     * @param AgentRequest $agentRequest
     */
    public function __construct(AgentRequest $agentRequest)
    {
        $this->request_model = $agentRequest;
    }

    public function activate($email, $code, UserRepository $userRepository, ActivationCodeRepository $activationCodeRepository){

        if($userRepository->findByEmail($email) != null){

            $user = $userRepository->findByEmail($email);

            if($activationCodeRepository->checkIfExists($code, $user->id) != null){
                $user->activation_codes()->update([
                    'code' => ''
                ]);

                $mailer  = new AppMailer();

                $mailer->sendRegistrationLink($user->email, $user->name, $user->token);

                return true;
            }else{

                return false;
            }
        }
        return false;
    }

}