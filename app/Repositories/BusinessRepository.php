<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 1:10 PM
 */

namespace UzaPoint\Repositories;
use UzaPoint\Business;
use UzaPoint\UserBusiness;


class BusinessRepository
{
    /**
     * The model used by this repository
     * @var
     */
    protected $model;

    /**
     * BusinessRepository constructor.
     * @param UserBusiness $userBusiness
     */
    public function __construct(UserBusiness $userBusiness)
    {
       $this->model = $userBusiness;
    }

    /**
     * Get all the businesses for a particular user
     * @param $user_id
     * @return mixed
     */
    public function getUserBusinesses($user_id){

        return $this->model->with('business')->where('user_id', $user_id)->get();
    }

    /**
     * Get the first business of a user
     * @param $user_id
     * @return mixed
     */
    public function getFirstBusiness($user_id){

        return $this->model->with('business')->where('user_id', $user_id)->first();
    }

    public function getNotDefaultBusinesses($user_id, $business_id){

        return $this->model->with('business')->where('user_id', $user_id)->where('id', '!=', $business_id)->get();

    }

    public function getBusiness($business_id){


        return Business::findOrFail($business_id);

    }

}