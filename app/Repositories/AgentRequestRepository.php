<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/16
 * Time: 5:15 PM
 */

namespace UzaPoint\Repositories;
use Illuminate\Support\Facades\Auth;
use UzaPoint\AgentRequest;
use UzaPoint\Mailers\AppMailer;
use UzaPoint\User;


class AgentRequestRepository
{

    /**
     * The model for this repository
     * @var
     */
    protected $model;

    /**
     * AgentRequestRepository constructor.
     * @param AgentRequest $agentRequest
     */
    public function __construct(AgentRequest $agentRequest)
    {
        $this->model = $agentRequest;
    }

    public function store($user, $business, AgentRepository $agentRepository){

        $agents = $agentRepository->findAgents($business->location);

        foreach ($agents as $agent){

            $this->model->create([

                'user_id' => $user->id,
                'agent_id' =>  $agent->id,
                'status'   => 0

            ]);
        }

    }

    public function getAgentRequests($agent_id){

       return $this->model->where('agent_id', $agent_id)->with('user')->get();

    }

    public function setApproved($user_id){

        $this->model->where('user_id', $user_id)
                    ->update([

                    'status' => 1

                    ]);
    }

    public function setActivationCodeSent($user_id, $agent_id, ActivationCodeRepository $activationCodeRepository){

        $this->model->where('user_id', $user_id)
            ->where('agent_id', $agent_id)
            ->update([
                'status' => 2
            ]);

        $activationCodeRepository->store($user_id, new UserRepository(new User()));

        return $this->getAgentRequest($user_id, $agent_id);
    }


    public function setDeclined($user_id, $agent_id, $information, UserRepository $userRepository){

        $user = $userRepository->findById($user_id);

        $this->model->where('user_id', $user_id)
            ->where('agent_id', $agent_id)
            ->update([
                'status' => 3,
                'information' => $information
            ]);

        $mailer = new AppMailer();

        $mailer->declineRequestEmail($user->email, $user->name, $information);

        return $this->getAgentRequest($user_id, $agent_id);
    }

    public function getAgentRequest($user_id, $agent_id){

        return $this->model->where('user_id', $user_id)
            ->where('agent_id', $agent_id)
            ->first();
    }

}