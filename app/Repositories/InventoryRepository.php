<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/26/16
 * Time: 11:14 PM
 */

namespace UzaPoint\Repositories;
use UzaPoint\Inventory;


class InventoryRepository
{
    /**
     * The model used by this repository
     * @var
     */
    protected $model;

    public function __construct(Inventory $inventory)
    {
        $this->model = $inventory;
    }

    public function deleteInventory(Array $ids){

        foreach ($ids as $id){

            $inventory = $this->model->where('id', $id)
                        ->first();

            $inventory->delete();

        }

    }

}