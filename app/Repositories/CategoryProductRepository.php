<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/26/16
 * Time: 7:12 PM
 */

namespace UzaPoint\Repositories;

use UzaPoint\CategoryProduct;
use UzaPoint\InventoryCategory;


class CategoryProductRepository
{
    /**
     * The model used by this repository
     * @var
     */
    protected $model;

    /**
     * CategoryProductRepository constructor.
     * @param CategoryProduct $categoryProduct
     */
    public function __construct(CategoryProduct $categoryProduct)
    {
        $this->model = $categoryProduct;
    }

    public function getCategoryInventory($inventory_id){

        $categoryIds = $this->model->where('inventory_id', $inventory_id)
                    ->pluck('inventory_category_id')
                    ->toArray();

        return InventoryCategory::whereIn('id', $categoryIds)->get();
    }
}