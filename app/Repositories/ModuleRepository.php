<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/9/16
 * Time: 2:37 AM
 */

namespace UzaPoint\Repositories;

use Illuminate\Support\Facades\Auth;
use UzaPoint\UzapointModule;


class ModuleRepository
{
    /**
     * The model used by this repository
     * @var
     */
    protected $model;

    /**
     * ModuleRepository constructor.
     * @param UzapointModule $uzapointModule
     */
    public function __construct(UzapointModule $uzapointModule)
    {
        $this->model = $uzapointModule;
    }


    /**
     * Get all the modules in the application
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(){

        return $this->model->all();


    }

    /**
     * Get a single module from the database
     * @param $module_id
     * @return mixed
     */
    public function show($module_id){

        return $this->model->findOrFail($module_id);
    }

    /**
     * Update a particular model to the database
     * @param $module_id
     * @param $request
     */
    public function update($module_id, $request){

        $module = $this->show($module_id);
        $module->update([
            'name' => $request->name,
            'description' => $request->description,
            'status'      => $request->status
        ]);
    }

    /**
     * Persist a new module to the database
     * @param $request
     */
    public function store($request){

        Auth::user()->modules()->create($request);
    }

   public function isSkeletonEnabled(){

       return $this->model->where('name', '=', 'Skeleton')->first()->status;

   }
}