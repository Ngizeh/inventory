<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 4:51 AM
 */

namespace UzaPoint\Repositories;
use UzaPoint\ActivationCode;
use UzaPoint\Mailers\AppMailer;


class ActivationCodeRepository
{
    /**
     * The model used by this repository
     * @var
     */
    protected $model;

    /**
     * ActivationCodeRepository constructor.
     * @param ActivationCode $activationCode
     */
    public function __construct(ActivationCode $activationCode)
    {
        $this->model = $activationCode;
    }

    public function store($user_id, UserRepository $userRepository){

        $user = $userRepository->findById($user_id);

        $activation_code = $user->activation_codes()->create([

        ]);

        $mailer = new AppMailer();

        $mailer->sendActivationCode($user->email, $activation_code->code);
    }

    public function checkIfExists($code, $user_id){

        if($this->model->where('code', $code)->where('user_id', $user_id)->exists()){

            return $this->model->where('code', $code)->where('user_id', $user_id)->first();
        }

        return null;

    }

}