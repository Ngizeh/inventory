<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/18/16
 * Time: 11:29 AM
 */

namespace UzaPoint\Repositories;
use UzaPoint\User;
use UzaPoint\UserDetails;



class UserDetailsRepository
{
    /**
     * The model used by this repository
     * @var
     */
    protected $model;

    /**
     * UserDetailsRepository constructor.
     * @param UserDetails $userDetails
     */
    public function __construct(UserDetails $userDetails)
    {
        $this->model = $userDetails;
    }

    /**
     * Store User Details to the database
     * @param $request
     */
    public function store($request){

       return $request->
                user()
                ->details()
                ->create([
                    'document_one_title' => $request->document_one_title,
                    'document_one_name' => $request->document_one_name,
                    'document_one_description' => $request->document_one_description,

                    'document_two_title' => $request->document_two_title,
                    'document_two_name' => $request->document_two_name,
                    'document_two_description' => $request->document_two_description,
                ]);
    }

}