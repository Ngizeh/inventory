<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 12:15 PM
 */

namespace UzaPoint\Repositories;
use UzaPoint\Business;
use UzaPoint\Repositories\RegisterRepository;
use UzaPoint\User;
use UzaPoint\Web\Authorization\Repositories\RolesRepository;
use UzaPoint\Web\Authorization\Models\Role;
use UzaPoint\AgentRequest;
use UzaPoint\Agent;


class RegistrationRepository
{

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
//            'password' => bcrypt($data['password']),
        ]);

        $business = $this->setBusiness($user, $data);

        $this->addRole($user, new RolesRepository(new Role()), 'company');

        $request_repo = new AgentRequestRepository(new AgentRequest());

        $request_repo->store($user, $business, new AgentRepository(new Agent()));

        return $user;
    }

    public function setBusiness($user, $data){

       $business = Business::create([
            'business_name' => $data['business_name'],
            'location'      => $data['location']
        ]);

        return $user->businesses()->create([

            'business_id' => $business->id

        ]);
    }

    /**
     * Add a role for the user
     * @param $user
     * @param RolesRepository $rolesRepository
     */
    public function addRole($user, RolesRepository $rolesRepository, $name){

        $company = $rolesRepository->getRoleByName($name);

        $user->attachRole($company);
    }

}