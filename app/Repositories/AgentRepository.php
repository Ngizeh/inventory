<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/16
 * Time: 3:52 PM
 */

namespace UzaPoint\Repositories;
use UzaPoint\Agent;


class AgentRepository
{

    /**
     * The model used by this repository
     * @var
     */
    protected $model;

    /**
     * AgentRepository constructor.
     * @param Agent $agent
     */
    public function __construct(Agent $agent)
    {
        $this->model = $agent;
    }

    /**
     * Persist a new Agent to the database
     * @param $request
     */
    public function store($request){

        return $this->model->create([

            'name' => $request->name,
            'id_number' => $request->id_number,
            'postal_address' => $request->postal_address,
            'phone_number'   => $request->phone_number,
            'location'       => $request->location,
            'email'          => $request->email

        ]);
    }

    public function getAgents(){

        return $this->model->latest()->get();
    }

    public function findAgents($location){

        $agents = $this->model->where('location', 'LIKE', "%$location%")->get();

       if($agents->count()){

           return $this->model->where('location', 'LIKE', "%$location%")->get(['id']);

       }else{

           return $this->model->where('id', 1)->get(['id']);
       }
    }

    public function findAgentIdFromEmail($email){

        return $this->model->where('email', $email)->first()->id;

    }

}