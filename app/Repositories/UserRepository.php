<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/16
 * Time: 4:19 PM
 */

namespace UzaPoint\Repositories;
use Illuminate\Support\Facades\Session;
use UzaPoint\ActivationCode;
use UzaPoint\User;
use UzaPoint\Web\Authorization\Models\Role;
use UzaPoint\Web\Authorization\Repositories\RolesRepository;
use UzaPoint\Http\Requests\DeclineRequest;
use Illuminate\Support\Facades\Auth;


class UserRepository
{

    /**
     * The model used by this repository
     * @var
     */
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function store($name, $email, $password){

        $user = $this->model->create([
            'name'  => $name,
            'email' => $email,
            'password' => bcrypt($password)
        ]);

        $this->addRole($user, new RolesRepository(new Role()), 'agent');

        return $user;
    }


    /**
     * Add a role for the user
     * @param $user
     * @param RolesRepository $rolesRepository
     */
    public function addRole($user, RolesRepository $rolesRepository, $name){
        $agent = $rolesRepository->getRoleByName($name);
        $user->attachRole($agent);
    }

    public function activateUser($user_id, $agent_id, AgentRequestRepository $agentRequestRepository){

        return $agentRequestRepository->setActivationCodeSent($user_id, $agent_id, new ActivationCodeRepository(new ActivationCode()) );
    }

    public function declineUser($user_id, $agent_id, AgentRequestRepository $agentRequestRepository, $information){
       return $agentRequestRepository->setDeclined($user_id, $agent_id, $information, new UserRepository(new User()));
    }

    public function findById($user_id){

        return $this->model->findOrFail($user_id);
    }

    public function findByEmail($email){

        if($this->model->where('email', $email)->exists()){

            return $this->model->where('email', $email)->first();
        }

        return null;
    }

    public function checkIfTokenExists($code, $email){

        if($this->model->where('token', $code)->where('email', $email)->exists()){

            return true;
        }

        return false;
    }

    public function secureAccount($email, $request){

        if($this->findByEmail($email)){

            $user = $this->findByEmail($email);

            $user->update([
                'password' => bcrypt($request->password)
            ]);
        }
    }

    public function getUser(){
        return Auth::user();
    }
}