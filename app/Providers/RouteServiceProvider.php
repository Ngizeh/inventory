<?php

namespace UzaPoint\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'UzaPoint\Http\Controllers';

    protected $namespace_pos = 'UzaPoint\Modules\POS\Controllers';

    protected $namespace_inventory = 'UzaPoint\Modules\Inventory\Controllers';

    protected $namespace_knowledge = 'UzaPoint\Modules\Knowledge\Controllers';

    protected $namespace_service = 'UzaPoint\Modules\Service\Controllers';

    protected $namespace_community = 'UzaPoint\Modules\Community\Controllers';

    protected $namespace_procurement = 'UzaPoint\Modules\Procurement\Controllers';

    protected $namespace_sales = 'UzaPoint\Modules\Sales\Controllers';

    protected $namespace_hr = 'UzaPoint\Modules\Hr\Controllers';

    protected $namespace_accounting = 'UzaPoint\Modules\Accounting\Controllers';

    protected $namespace_crm = 'UzaPoint\Modules\Crm\Controllers';

    protected $namespace_user_management = 'UzaPoint\Modules\UserManagement\Controllers';

    protected $namespace_members = 'UzaPoint\Modules\Members\Controllers';

    protected $namespace_merchant = 'UzaPoint\Modules\Merchant\Controllers';



    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
        $this->mapApiRoutes();
        $this->mapPOSRoutes();
        $this->mapInventoryRoutes();
        $this->mapKnowledgeRoutes();
        $this->mapServiceRoutes();
        $this->mapCommunityRoutes();
        $this->mapProcurementRoutes();
        $this->mapSalesRoutes();
        $this->mapHrRoutes();
        $this->mapAccountingRoutes();
        $this->mapCrmRoutes();
        $this->mapUserManagementRoutes();
        $this->mapMembersRoutes();
        $this->mapMerchantRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }


    protected function mapPOSRoutes(){

        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_pos,
        ], function ($router) {
            require base_path('app/Modules/POS/routes.php');
        });
    }

    protected function mapInventoryRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_inventory,
        ], function ($router) {
            require base_path('app/Modules/Inventory/routes.php');
        });
    }

    protected function mapKnowledgeRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_knowledge,
        ], function ($router) {
            require base_path('app/Modules/Knowledge/routes.php');
        });
    }

    protected function mapServiceRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_service,
        ], function ($router) {
            require base_path('app/Modules/Service/routes.php');
        });
    }

    protected function mapCommunityRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_community,
        ], function ($router) {
            require base_path('app/Modules/Community/routes.php');
        });
    }

    protected function mapProcurementRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_procurement,
        ], function ($router) {
            require base_path('app/Modules/Procurement/routes.php');
        });
    }

    protected function mapSalesRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_sales,
        ], function ($router) {
            require base_path('app/Modules/Sales/routes.php');
        });
    }

    protected function mapHrRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_hr,
        ], function ($router) {
            require base_path('app/Modules/Hr/routes.php');
        });
    }

    protected function mapAccountingRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_accounting,
        ], function ($router) {
            require base_path('app/Modules/Accounting/routes.php');
        });
    }

    protected function mapCrmRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_crm,
        ], function ($router) {
            require base_path('app/Modules/Crm/routes.php');
        });
    }

    protected function mapUserManagementRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_user_management,
        ], function ($router) {
            require base_path('app/Modules/UserManagement/routes.php');
        });
    }

    protected function mapMembersRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_members,
        ], function ($router) {
            require base_path('app/Modules/Members/routes.php');
        });
    }

    protected function mapMerchantRoutes(){
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace_merchant,
        ], function ($router) {
            require base_path('app/Modules/Merchant/routes.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }
}
