<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class ActivationCode extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'activation_codes';

    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'user_id',
        'code'

    ];

    /**
     * User ActivationCode relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){

        return $this->belongsTo(User::class);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($activation_code){

            $activation_code->code = str_random(5);

        });
    }
}
