<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class BusinessInventory extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'business_inventories';

    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'business_id',
        'inventory_id'
    ];

    /**
     * BusinessInventory Business relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function business(){

        return $this->belongsTo(Business::class);
    }

    /**
     * BusinessInventory Inventory relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inventory(){

        return $this->belongsTo(Inventory::class);
    }

}
