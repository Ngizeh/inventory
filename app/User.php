<?php

namespace UzaPoint;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustPermissionTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    /**
     * User UzaPointModule Relationship
     * @return Eloquent\Relations\HasMany
     */
    public function modules(){

        return $this->hasMany(UzapointModule::class);
    }

    /**
     * User UserBusiness Relationship
     * @return Eloquent\Relations\HasMany
     */
    public function businesses(){

        return $this->hasMany(UserBusiness::class);
    }

    /**
     * User AgnetRequest Relationship
     * @return Eloquent\Relations\HasOne
     */
    public function request(){

        return $this->hasOne(AgentRequest::class);
    }

    /**
     * User ActivationCode Relationship
     * @return Eloquent\Relations\HasMany
     */
    public function activation_codes(){

        return $this->hasMany(ActivationCode::class);
    }

    /**
     * User UserDetails relationship
     * @return Eloquent\Relations\HasOne
     */
    public function details(){

        return $this->hasOne(UserDetails::class);
    }

    /**
     * User UserLogin relationship
     * @return Eloquent\Relations\HasOne
     */
    public function login(){

        return $this->hasOne(UserLogin::class);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($user){

            $user->token = str_random(30);

        });
    }

    public function sale(){

        return $this->hasMany(Sale::class);

    }


}
