<?php

namespace UzaPoint\Web\Authorization\Models;

use Illuminate\Database\Eloquent\Model;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission


{
    /**
     * The table used by this model
     * @var string
     *
     * */
      protected  $table = 'permissions';

    /**
     * Fields that can be mass assined
     *  main attributes
     */

    protected $fillable = [

        'name', 'display_name','description'
    ];
}

