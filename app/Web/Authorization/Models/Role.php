<?php

namespace UzaPoint\Web\Authorization\Models;

use Illuminate\Database\Eloquent\Model;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    /**
     * The table used by this model
     * @var string
     *
     */
    protected  $table = 'roles';

    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'name', 'display_name','description'
    ];
}
