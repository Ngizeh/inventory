<?php

/**
 * Handles the Input and Output Flow for the
 */

namespace UzaPoint\Web\Authorization\Controllers;

use Illuminate\Support\Facades\Session;
use UzaPoint\Web\Authorization\Repositories\PermissionsRepository;
use UzaPoint\Http\Controllers\Controller;
use UzaPoint\Web\Authorization\Validation\SavePermissionRequest;

class PermissionsController extends Controller
{
    /**
     * Delegate the storage of a new Permission to the database
     * @param SavePermissionRequest $savePermissionRequest
     * @param PermissionsRepository $permissionsRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SavePermissionRequest $savePermissionRequest, PermissionsRepository $permissionsRepository){

        $permissionsRepository->store($savePermissionRequest);

        Session::flash('flash_message', 'New Permissions was successfully added');

        return redirect()->back();
    }

    /**
     * Get the template for adding a new Permission
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){

        return view('authorization.permissions.create');
    }
}
