<?php

/**
 * Handles the Roles input and Output Flow
 */

namespace UzaPoint\Web\Authorization\Controllers;

use UzaPoint\Web\Authorization\Repositories\RolesRepository;
use UzaPoint\Http\Controllers\Controller;
use UzaPoint\Web\Authorization\Validation\SaveRoleRequest;
use Illuminate\Support\Facades\Session;

class RolesController extends Controller
{
    /**
     * Delegate the process of adding a new role
     * @param SaveRoleRequest $saveRoleRequest
     * @param RolesRepository $rolesRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SaveRoleRequest $saveRoleRequest, RolesRepository $rolesRepository){

        $rolesRepository->store($saveRoleRequest->all());

        Session::flash('flash_message', 'New Role was added to the database successfully');

        return redirect()->back();
    }

    /**
     * Get the template for adding a new Role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){

        return view('authorization.roles.create');
    }
}
