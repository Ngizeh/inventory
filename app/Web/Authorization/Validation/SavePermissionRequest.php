<?php

namespace UzaPoint\Web\Authorization\Validation;

use Illuminate\Foundation\Http\FormRequest;

class SavePermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|min:3|max:100',
            'display_name' => 'required|min:3|max:100',
            'description'  => 'required|min:5|max:1000'
        ];
    }
}
