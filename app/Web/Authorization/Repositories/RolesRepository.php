<?php
/**
 * Handles the business logic for the Roles Table
 */

namespace UzaPoint\Web\Authorization\Repositories;

use UzaPoint\Web\Authorization\Models\Role;

class RolesRepository
{
    /**
     * The model used by this repository
     * @var
     */
    protected $model;

    /**
     * PermissionsRepository constructor.
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    /**
     * Store a new permission to the database
     * @param $request
     */
    public function store($request){

        $this->model->create($request);
    }

    /**
     * Get a permission by name
     * @param $name
     * @return mixed
     */
    public function getRoleByName($name){

       return $this->model->where('name', '=', $name)->first();
    }
}