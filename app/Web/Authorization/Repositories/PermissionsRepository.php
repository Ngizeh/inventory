<?php
/**
 * Handles the business logic for the Permission model
 */
namespace UzaPoint\Web\Authorization\Repositories;

use UzaPoint\Web\Authorization\Models\Permission;

class PermissionsRepository
{
    /**
     * The model used by this Repository
     * @var Permission
     */
    protected $model;

    /**
     * PermissionsRepository constructor.
     * @param Permission $permission
     */
    public function __construct(Permission $permission)
    {
        $this->model = $permission;
    }

    /**
     * Persist a new Permission to the database
     * @param array $array
     */
    public function store($addPermissionRequest){

      $this->model->create([

          'name' => $addPermissionRequest->name,
          'display_name' => $addPermissionRequest->display_name,
          'description'  => $addPermissionRequest->description

      ]);
    }

}