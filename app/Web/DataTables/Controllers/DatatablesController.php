<?php

namespace UzaPoint\Web\DataTables\Controllers;

use UzaPoint\BranchInventory;
use UzaPoint\Http\Controllers\Controller;
use UzaPoint\User;
use Yajra\Datatables\Facades\Datatables;

class DatatablesController extends Controller
{
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        return view('home.retailer.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {

        $inventories =  BranchInventory::all();

        return Datatables::of($inventories)
            ->addColumn('action', function ($inventory) {
                return '<a href="#edit-'.$inventory->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }
}
