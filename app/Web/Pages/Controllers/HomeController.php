<?php

namespace UzaPoint\Web\Pages\Controllers;

use UzaPoint\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use UzaPoint\Repositories\AgentRepository;
use UzaPoint\Repositories\AgentRequestRepository;
use UzaPoint\Repositories\ModuleRepository;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Handle The page viewing between authenticated and un authenticated users
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function rootIndex(){

        if(Auth::check()){

            return redirect('/home');
        }
        return view('welcome');
    }

    /**
     * Show the application dashboard.
     * @param ModuleRepository $moduleRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ModuleRepository $moduleRepository, AgentRequestRepository $agentRequestRepository, AgentRepository $agentRepository)
    {
        if(Auth::user()->hasRole('admin')){

            return redirect()->route('modules.index');

        }elseif (Auth::user()->hasRole('retailer')){

            if($moduleRepository->isSkeletonEnabled() == 1){
                return view('Skeleton::dummy');
            }else{

                return view('errors.skeleton');
            }
        }elseif (Auth::user()->hasRole('company')){

            if($moduleRepository->isSkeletonEnabled() == 1){
                return view('Skeleton::dummy');
            }else{

                return view('errors.skeleton');
            }

        }elseif (Auth::user()->hasRole('agent')){
            if($moduleRepository->isSkeletonEnabled() == 1){

                $agent_id = $agentRepository->findAgentIdFromEmail(Auth::user()->email);

                $requests = $agentRequestRepository->getAgentRequests($agent_id);

                return view('Skeleton::dummy_agent', compact('requests'));
            }else{

                return view('errors.skeleton');
            }
        }elseif (Auth::user()->hasRole('technical')){
            if($moduleRepository->isSkeletonEnabled() == 1){

                return view('home.admin.technical_support.index');
            }else{

                return view('errors.skeleton');
            }
        }elseif (Auth::user()->hasRole('manager')){
            if($moduleRepository->isSkeletonEnabled() == 1){

                return view('home.admin.general_manager.index');
            }else{

                return view('errors.skeleton');
            }
        }elseif (Auth::user()->hasRole('distributor_admin')){
            if($moduleRepository->isSkeletonEnabled() == 1){

                return view('home.distributors.distributor_admin.index');
            }else{

                return view('errors.skeleton');
            }
        }elseif (Auth::user()->hasRole('distributor_manager')){
            if($moduleRepository->isSkeletonEnabled() == 1){

                return view('home.distributors.distributor_manager.index');
            }else{

                return view('errors.skeleton');
            }
        }elseif (Auth::user()->hasRole('distributor_sales_agent')){
            if($moduleRepository->isSkeletonEnabled() == 1){

                return view('home.distributors.distributor_sales_agent.index');
            }else{

                return view('errors.skeleton');
            }
        }elseif (Auth::user()->hasRole('sub_agent')){
            if($moduleRepository->isSkeletonEnabled() == 1){

                return view('home.sub-agent.index');
            }else{

                return view('errors.skeleton');
            }
        }

    }
}
