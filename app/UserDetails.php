<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'user_details';

    /**
     * Fields that can be mass assigned
     * @var array
     */
    protected $fillable = [

        'document_one_title',
        'document_one_name',
        'document_one_description',
        'document_two_title',
        'document_two_name',
        'document_two_description'

    ];

    /**
     * UserDetails User relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){

        return $this->belongsTo(User::class);
    }
}
