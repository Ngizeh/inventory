<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/3/16
 * Time: 12:05 PM
 */

namespace UzaPoint\Mailers;

use Illuminate\Support\Facades\Mail;
use UzaPoint\User;


class AppMailer {

    /**
     * Email sender
     * @var
     */
    protected $from = 'karokijames40@gmail.com';

    /**
     * Email receiver
     * @var
     */
    protected $to;

    /**
     * Email view
     * @var
     */
    protected $view;

    /**
     * Email subject
     * @var
     */
    protected $subject;

    /**
     * Email data
     * @var
     */
    protected $data;


    /**
     * Send Invitation Emails to users
     * @param $email
     * @param $admin
     * @param $request_code
     * @param $team_name
     */
    public function sendActivationCode($email, $code){

        $this->to = $email;
        $this->view = 'emails.activation_code_email';
        $this->subject = 'UzaPoint Activation Code';
        $this->data = compact('email', 'code');
        $this->deliver();
    }

    public function declineRequestEmail($email, $name, $information){

        $this->to = $email;
        $this->view = 'emails.decline_request_email';
        $this->subject = 'UzaPoint Decline Request';
        $this->data = compact('email', 'information', 'name');
        $this->deliver();
    }

    public function sendRegistrationLink($email, $name, $code){

        $this->to = $email;
        $this->view = 'emails.registration_link_email';
        $this->subject = 'UzaPoint Registration';
        $this->data = compact('email', 'name', 'code');
        $this->deliver();
    }

    /**
     * Handle sending of actual email
     */
    public function deliver(){

        Mail::send($this->view, $this->data, function($message){

            $message->from($this->from, 'UzaPoint Admin')
                ->to($this->to)
                ->subject($this->subject);
        });
    }
} 