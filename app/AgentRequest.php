<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class AgentRequest extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'agent_requests';

    /**
     * The table used by this model
     * @var array
     */
    protected $fillable = [

        'user_id',
        'agent_id',
        'status',
        'information'
    ];


    /**
     * Agent AgentRequest Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent(){

        return $this->belongsTo(Agent::class);
    }

    /**
     * User AgentRequest Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){

        return $this->belongsTo(User::class);
    }
}
