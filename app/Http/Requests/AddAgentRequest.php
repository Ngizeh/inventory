<?php

namespace UzaPoint\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddAgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => 'required',
            'id_number' => 'required',
            'postal_address' => 'required',
            'phone_number'   => 'required',
            'location'      => 'required',
            'email'         => 'required|email',
            'password' => 'required|min:6|confirmed',
        ];
    }
}
