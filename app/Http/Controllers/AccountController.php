<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use UzaPoint\ActivationCode;
use UzaPoint\Http\Requests;
use UzaPoint\Http\Requests\ActivateAccountRequest;
use UzaPoint\Repositories\AccountRepository;
use UzaPoint\Repositories\ActivationCodeRepository;
use UzaPoint\Repositories\UserRepository;
use UzaPoint\User;
use UzaPoint\Http\Requests\SecureAccountRequest;

class AccountController extends Controller
{
    public function getActivate(){

        return view('account.activate');
    }

    public function activate(ActivateAccountRequest $activateAccountRequest, AccountRepository $accountRepository){

        if($accountRepository->activate($activateAccountRequest->email, $activateAccountRequest->code, new UserRepository(new User()), new ActivationCodeRepository(new ActivationCode()))){

            Session::flash('flash_message', 'Your account was activated successfully, Kindly check your email to continue');

            return redirect()->back();
        }

        Session::flash('flash_message_error', 'There was a problem activating your account, check your activation details and try again');

        return redirect()->back();
    }

    public function complete($code, $email, UserRepository $userRepository){

        if($userRepository->checkIfTokenExists($code, $email)){

            return view('auth.passwords.secure', compact('email'));

        }

        return view('errors.404');
    }

    public function secure($email, SecureAccountRequest $accountRequest, UserRepository $userRepository){

        $userRepository->secureAccount($email, $accountRequest);

        $user = $userRepository->findByEmail($email);

        Auth::login($user);

        return redirect('/home');

    }
}
