<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use UzaPoint\Http\Requests;
use UzaPoint\InventoryCategory;
use UzaPoint\Http\Requests\SaveCategoryRequest;
use UzaPoint\BusinessInventoryCategory;

class CategoryController extends Controller
{
    public function store($business_id, SaveCategoryRequest $saveCategoryRequest){

        $category = InventoryCategory::create([

            'category_name' => $saveCategoryRequest->category_name,
            'category_description' => $saveCategoryRequest->category_description,

        ]);

         BusinessInventoryCategory::create([

            'business_id' => $business_id,
            'inventory_category_id' => $category->id

        ]);

        Session::flash('flash_message', 'The category was added successfully');

        return redirect()->back();

    }
}
