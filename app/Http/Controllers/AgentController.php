<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use UzaPoint\Http\Requests;
use UzaPoint\Repositories\AgentRepository;
use UzaPoint\Http\Requests\AddAgentRequest;
use UzaPoint\Repositories\UserRepository;

class AgentController extends Controller
{
    public function index(AgentRepository $agentRepository){

        $agents = $agentRepository->getAgents();

        return view('agents.index', compact('agents'));
    }

    public function create(){

        return view('agents.create');
    }

    public function store(AgentRepository $agentRepository, AddAgentRequest $addAgentRequest, UserRepository $userRepository){

        $agent = $agentRepository->store($addAgentRequest);

        $userRepository->store($agent->name, $agent->email, $addAgentRequest->password);

        Session::flash('flash_message', 'The Agent was added successfully');

        return redirect()->back();

    }
}
