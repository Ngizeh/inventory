<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use UzaPoint\Customer;
use UzaPoint\Http\Requests;
use UzaPoint\Http\Requests\AddCustomerRequest;

class CustomersController extends Controller
{
    public function index(){

        return view('customer.index');
    }

    public function store(AddCustomerRequest $addCustomerRequest){


        Customer::create([

            'name' => $addCustomerRequest->name,
            'email' => $addCustomerRequest->email,
            'phone_number' => $addCustomerRequest->phone_number,
            'account'    => $addCustomerRequest->account

        ]);

        Session::flash('flash_message', 'The customer was added successfully');

        return redirect()->back();
    }
}
