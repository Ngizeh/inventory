<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use UzaPoint\Http\Requests;

class OauthController extends Controller
{
    /**
     * Display Oauth items
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){

        return view('auth.oauth.index');
    }
}
