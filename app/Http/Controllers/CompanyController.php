<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use UzaPoint\Http\Requests;

class CompanyController extends Controller
{
    public function index(){

        return view('companies.index');
    }

    public function modules(){

        return view('companies.modules');
    }
}
