<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use UzaPoint\Http\Requests;

class RetailerController extends Controller
{

    public function index(){

        return view('retailers.index');
    }

    public function create(){

        return view('retailers.create');
    }
}
