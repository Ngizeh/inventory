<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use UzaPoint\Http\Requests;

class DistributorController extends Controller
{

    public function index(){

        return view('distributors.index');
    }

    public function create(){

        return view('distributors.create');
    }

    public function manageCustomers(){

        return view('home.distributors.distributor_admin.customers.manage');
    }

    public function manageProfile(){

        return view('home.distributors.distributor_admin.profile.manage');
    }

    public function manageBranch(){

        return view('home.distributors.distributor_admin.branch.manage');
    }

    public function manageProducts(){

        return view('home.distributors.distributor_admin.products.manage');
    }

    public function managePortal(){

        return view('home.distributors.distributor_admin.portal.manage');
    }

    public function manageSales(){

        return view('home.distributors.distributor_admin.sales.manage');
    }

    public function manageSku(){

        return view('home.distributors.distributor_admin.sku.manage');
    }

    public function manageInvoicing(){

        return view('home.distributors.distributor_admin.invoicing.manage');
    }
}
