<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use UzaPoint\AgentRequest;
use UzaPoint\Http\Requests;
use UzaPoint\Repositories\AgentRequestRepository;
use UzaPoint\Repositories\UserRepository;
use UzaPoint\Http\Requests\DeclineRequest;

class UserController extends Controller
{
    public function addMoreDetails($user_id){


        return view('home.customer.more_details', compact('user_id'));
    }

    public function storeMoreDetails($user_id, AgentRequestRepository $agentRequestRepository){

        $agentRequestRepository->setApproved($user_id);

        Session::flash('flash_message', 'The Customer Request was approved successfully');

        return redirect('/home');

    }

    public function activateUser($user_id, $agent_id, UserRepository $userRepository){

       $userRepository->activateUser($user_id, $agent_id, new AgentRequestRepository(new AgentRequest()));

        Session::flash('flash_message', "The activation code was sent to the user's email");

        return redirect()->back();

    }

    public function declineUserRequest($user_id, $agent_id, UserRepository $userRepository, DeclineRequest $declineRequest){

        $userRepository->declineUser($user_id, $agent_id, new AgentRequestRepository(new AgentRequest()), $declineRequest->information);

        Session::flash('flash_message', "The Customer Request was declined successfully");

        return redirect()->back();

    }

    public function getDecline($user_id, $agent_id){

        return view('agents.decline', compact('user_id', 'agent_id'));

    }
}
