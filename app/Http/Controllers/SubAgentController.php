<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use UzaPoint\Http\Requests;

class SubAgentController extends Controller
{
    public function index(){

        return view('sub_agents.index');
}

    public function create(){

        return view('sub_agents.create');
    }
}
