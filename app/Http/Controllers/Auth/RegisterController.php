<?php

namespace UzaPoint\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use UzaPoint\Agent;
use UzaPoint\AgentRequest;
use UzaPoint\Business;
use UzaPoint\Repositories\AgentRepository;
use UzaPoint\Repositories\AgentRequestRepository;
use UzaPoint\User;
use UzaPoint\Web\Authorization\Models\Role;
use Validator;
use UzaPoint\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use UzaPoint\Web\Authorization\Repositories\RolesRepository;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone_number' => 'required|max:12',
            'business_name' => 'required|max:100',
            'location'      => 'required|max:100'
//            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $this->create($request->all());

        Session::flash('flash_message', 'Your request has been received, Our agent will get back to you shortly');

//        return redirect($this->redirectPath());

        return view('auth.login');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
//            'password' => bcrypt($data['password']),
        ]);

        $business = $this->setBusiness($user, $data);

        $user->login()->create([
            'status' => 0
        ]);

        $this->addRole($user, new RolesRepository(new Role()), 'company');

        $request_repo = new AgentRequestRepository(new AgentRequest());

        $request_repo->store($user, $business, new AgentRepository(new Agent()));

        return $user;
    }

    public function setBusiness($user, $data){

        $business = Business::create([
            'business_name' => $data['business_name'],
            'location'      => $data['location']
        ]);

        return $user->businesses()->create([
            'business_id' => $business->id
        ]);
    }

    /**
     * Add a role for the user
     * @param $user
     * @param RolesRepository $rolesRepository
     */
    public function addRole($user, RolesRepository $rolesRepository, $name){

        $company = $rolesRepository->getRoleByName($name);

        $user->attachRole($company);
    }
}
