<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use PhpParser\Node\Expr\AssignOp\Mod;
use UzaPoint\Http\Requests;
use UzaPoint\Http\Requests\RegisterModuleRequest;
use UzaPoint\Repositories\ModuleRepository;
use Yajra\Datatables\Facades\Datatables;

class ModuleController extends Controller
{

    /**
     * Queries all the modules in the database
     * @param ModuleRepository $moduleRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){



        return view('modules.index');
    }

    public function modulesData(ModuleRepository $moduleRepository){

        $modules = $moduleRepository->index();


        return Datatables::of($modules)


            ->addColumn('checkbox', function ($module) {

                return '<input type="checkbox" name="'. $module->id .'" id="'. $module->id .'" />';
            })
            ->addColumn('edit_module', function ($module) {

                return '<a href="'. route("modules.edit", [$module->id]) .'"/>Edit Module</a>';
            })
//            ->addColumn('actions', function ($inventory) {
//                return '<div class="btn-group">
//                          <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//                            Actions
//                          </button>
//                          <div class="dropdown-menu">
//                                       <a class="dropdown-item" href="#">Export to PDF</a>
//                                       <a class="dropdown-item" href="#">Export to Excel</a>
//                                       <a class="dropdown-item" href="#">Delete Products</a>
//                          </div>
//                        </div>';
//            })
            ->make(true);
    }

    /**
     * Get the Template for adding a module to the database
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){

        return view('modules.create');
    }

    /**
     * Get the template for editing a module
     * @param $module_id
     * @param ModuleRepository $moduleRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($module_id, ModuleRepository $moduleRepository){

        $module = $moduleRepository->show($module_id);

        return view('modules.edit', compact('module'));

    }

    /**
     * Handle updating a module
     * @param $module_id
     * @param RegisterModuleRequest $registerModuleRequest
     * @param ModuleRepository $moduleRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($module_id, RegisterModuleRequest $registerModuleRequest, ModuleRepository $moduleRepository){

        $moduleRepository->update($module_id, $registerModuleRequest);

        Session::flash('flash_message', 'Module was updated successfully');

        return redirect()->back();
    }

    /**
     * Delegate the process of persisting a new module to the database
     * @param RegisterModuleRequest $registerModuleRequest
     * @param ModuleRepository $moduleRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RegisterModuleRequest $registerModuleRequest, ModuleRepository $moduleRepository){

        $moduleRepository->store($registerModuleRequest->all());

        Session::flash('flash_message', 'New module was registered successfully');

        return redirect()->back();
    }
}
