<?php

namespace UzaPoint\Http\Controllers;

use Illuminate\Http\Request;

use UzaPoint\BusinessSales;
use UzaPoint\Http\Requests;
use UzaPoint\Sale;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use UzaPoint\SaleTemp;
use UzaPoint\Inventory;
use UzaPoint\SaleItem;
use Illuminate\Support\Facades\Session;
use UzaPoint\Http\Requests\SaleRequest;

class SalesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SaleRequest $request, $business_id)
    {

        $sales = new Sale;
//        $sales->customer_id = Input::get('customer_id');
//        $sales->user_id = Auth::user()->id;
//        $sales->payment_type = Input::get('payment_type');
//        $sales->comments = Input::get('comments');
        $sales = $sales->create([

            'customer_id' => Input::get('customer_id'),
            'user_id'     => Auth::user()->id,
            'payment_type' => Input::get('payment_type'),
            'comments'      => Input::get('comments')

        ]);


        BusinessSales::create([

            'business_id' => $business_id,
            'sale_id'     => $sales->id

        ]);
        // process sale items
        $saleItems = SaleTemp::all();

        $saleItemsData = new SaleItem;
        $saleItemsData->sale_id = $sales->id;

        foreach ($saleItems as $value) {

            $saleItemsData = new SaleItem;
            $saleItemsData->sale_id = $sales->id;
            $saleItemsData->inventory_id = $value->inventory_id;
            $saleItemsData->cost_price = $value->cost_price;
            $saleItemsData->selling_price = $value->selling_price;
            $saleItemsData->quantity = $value->quantity;
            $saleItemsData->total_cost = $value->total_cost;
            $saleItemsData->total_selling = $value->total_selling;
            $saleItemsData->save();
            //process inventory
//            $items = Inventory::find($value->item_id);
//
//                $inventories = new Inventory;
//                $inventories->item_id = $value->item_id;
//                $inventories->user_id = Auth::user()->id;
//                $inventories->in_out_qty = -($value->quantity);
//                $inventories->remarks = 'SALE'.$sales->id;
//                $inventories->save();
//                //process item quantity
//                $items->quantity = $items->quantity - $value->quantity;
//                $items->save();
        }
        $saleItems = $saleItemsData->sale_id;
        //delete all data on SaleTemp model
        SaleTemp::truncate();
        $itemssale = SaleItem::where('sale_id', $saleItemsData->sale_id)->with('inventory')->get();


        Session::flash('message', 'You have successfully added sales');
        //return Redirect::to('receivings');
        return view('POS::sale.complete', compact('sales', 'saleItems', 'itemssale'));


    }

}
