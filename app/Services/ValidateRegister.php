<?php

namespace UzaPoint\Services;
use Illuminate\Support\Facades\Validator;


/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/17/16
 * Time: 12:17 PM
 */
trait ValidateRegister
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone_number' => 'required|max:12',
            'business_name' => 'required|max:100',
            'location'      => 'required|max:100'
//            'password' => 'required|min:6|confirmed',
        ]);
    }

}