<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class BusinessInventoryCategory extends Model
{
    protected $table = 'business_inventory_categories';

    protected $fillable = [

        'business_id',
        'inventory_category_id'
    ];

    /**
     * BusinessInventoryCategory InventoryCategory relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inventory_category(){
        return $this->belongsTo(InventoryCategory::class);
    }

    /**
     * BusinessInventoryCategory Business relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function business(){
        return  $this->belongsTo(Business::class);
    }
}
