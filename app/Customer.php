<?php

namespace UzaPoint;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The table used by this model
     * @var string
     */
    protected $table = 'customers';

    /**
     * The fields that can be mass assigned
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'account'
    ];

    public function sale(){

        return $this->hasMany(Sale::class);
    }


}
